# Audit Message Validator

Library for importing Audit Message specifications and validating Audit logs in DICOM PS 3.15 format.

## Build

Requires maven and JDK 8 to be build.

```shell
mvn clean install
```

The lib will requires to be integrated in a Seam project, it runs with Java 7.
