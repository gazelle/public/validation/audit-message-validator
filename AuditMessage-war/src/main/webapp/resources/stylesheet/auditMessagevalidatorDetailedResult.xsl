<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~   Copyright 2014-2023 IHE International
  ~
  ~    Licensed under the Apache License, Version 2.0 (the "License");
  ~    you may not use this file except in compliance with the License.
  ~    You may obtain a copy of the License at
  ~
  ~        http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~    Unless required by applicable law or agreed to in writing, software
  ~    distributed under the License is distributed on an "AS IS" BASIS,
  ~    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~    See the License for the specific language governing permissions and
  ~    limitations under the License.
  -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output encoding="UTF-8" indent="yes" method="html" omit-xml-declaration="yes"/>
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Feb 02, 2012</xd:p>
            <xd:p><xd:b>Author:</xd:b> Abderrazek Boufahja, IHE Development, Kereval</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:param name="viewdown">true</xsl:param>
    <xsl:param name="constraintPath">https://gazelle.ihe.net/CDAGenerator/constraints/</xsl:param>
    <xsl:param name="auditMessageLink">https://gazelle.ihe.net/gss</xsl:param>

    <xsl:template match="/">
        <html>
            <head>
                <title>External Validation Report</title>
                <link href="https://gazelle.ihe.net/xsl/resultStyle.css" rel="stylesheet" type="text/css" media="screen"/>
            </head>
            <body>
                <script type="text/javascript">
                    function hideOrViewValidationDetailsMB() {
                       var detaileddiv = document.getElementById('resultdetailedann');
                       if (detaileddiv != null){
                           var onn = document.getElementById('resultdetailedann_switch_on');
                           if (onn != null){
                               if (onn.style.display == 'inline-block') onn.style.display = 'none';
                               else if (onn.style.display == 'none') onn.style.display = 'inline-block';
                           }
                           var off = document.getElementById('resultdetailedann_switch_off');
                           if (off != null){
                               if (off.style.display == 'inline-block') off.style.display = 'none';
                               else if (off.style.display == 'none') off.style.display = 'inline-block';
                           }
                           var body = document.getElementById('resultdetailedann_body');
                           if (body != null){
                               if (body.style.display == 'block') body.style.display = 'none';
                               else if (body.style.display == 'none') body.style.display = 'block';
                           }
                       }
                    }

                    function hideOrUnhideMB(elem){
                        var elemToHide = document.getElementById(elem.name + '_p');
                        if (elemToHide != null){
                                if (elem.checked){
                                    elemToHide.style.display = 'none';
                                }
                                else{
                                    elemToHide.style.display = 'block';
                                }
                           }
                    }

                    function detectDownloadResultButton(){
                        var elemDown = document.getElementById('resultForm:downloadMBResultB');
                        if (elemDown != null) return true;
                        return false;
                    }

                    function extractDownloadResultButton(parentNoeud){
                        var elemDown = document.getElementById('resultForm:downloadMBResultB').cloneNode(true);
                        if (elemDown != null) {
                            var downloadspan = document.getElementById('downloadspan');
                            downloadspan.appendChild(elemDown);
                        }
                    }

                </script>
                <style type="text/css">
                    .moore {
                        color: #005500;
                        background-color: #99bbee;
                   }
                </style>
                <h2>External Validation Report</h2>
                <br/>
                <div class="rich-panel styleResultBackground">
                    <div class="rich-panel-header">General Informations</div>
                    <div class="rich-panel-body">
                        <table border="0">
                            <tr>
                                <td><b>Validation Date</b></td>
                                <td><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationDate"/> - <xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTime"/></td>
                            </tr>
                            <tr>
                                <td><b>Validation Service</b></td>
                                <td>
                                <a>
                                	<xsl:attribute name="href">
                                        <xsl:value-of select="$auditMessageLink"/>/audit-messages/view.seam?name=<xsl:value-of
                                            select="substring(detailedResult/ValidationResultsOverview/ValidationServiceName, 36)"/>
                                	</xsl:attribute>
                                	<xsl:attribute name="target">_blank</xsl:attribute>
                                	<xsl:value-of select="substring(detailedResult/ValidationResultsOverview/ValidationServiceName, 36)"/>
                                </a>
                                	<xsl:if test="count(detailedResult/ValidationResultsOverview/ValidationServiceVersion) = 1">
	                                	(Version : <xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationServiceVersion"/>)
	                                </xsl:if>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Validation Test Status</b></td>
                                <td>
                                    <xsl:if test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'PASSED')">
                                        <div class="PASSED"><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTestResult"/></div>
                                    </xsl:if>
                                    <xsl:if test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'FAILED')">
                                        <div class="FAILED"><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTestResult"/></div>
                                    </xsl:if>
                                    <xsl:if test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'ABORTED')">
                                        <div class="ABORTED"><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTestResult"/></div>
                                    </xsl:if>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <br/>

                <div class="rich-panel styleResultBackground">
                    <div class="rich-panel-header ">Result overview</div>
                    <div class="rich-panel-body ">
                        <span>
                            <div>
                                <table border="0">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <b>
                                                    <a href="#wellformed">XML</a>
                                                </b>
                                            </td>
                                            <td>
                                                <div>
                                                    <xsl:attribute name="class"><xsl:value-of select="detailedResult/DocumentWellFormed/Result"/></xsl:attribute>
                                                    <xsl:value-of select="detailedResult/DocumentWellFormed/Result"/>
                                                </div>
                                            </td>
                                        </tr>
                                        <xsl:if test="count(detailedResult/DocumentValidXSD) = 1">
                                        <tr>
                                            <td><b><a href="#xsd">XSD</a></b></td>
                                            <td>
                                                <div>
                                                    <xsl:attribute name="class"><xsl:value-of select="detailedResult/DocumentValidXSD/Result"/></xsl:attribute>
                                                    <xsl:value-of select="detailedResult/DocumentValidXSD/Result"/>
                                                </div>
                                            </td>
                                        </tr>
                                        </xsl:if>
                                        <tr>
                                            <td>
                                                <b>
                                                    <a href="#mbv">ModelBased Validation</a>
                                                </b>
                                            </td>
                                            <td>
                                                <div>
                                                    <xsl:attribute name="class"><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTestResult"/></xsl:attribute>
                                                    <xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTestResult"/>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </span>
                    </div>
                </div>
                <br/>

                <div class="rich-panel styleResultBackground">
                    <div class="rich-panel-header "><a name="wellformed">XML Validation Report</a></div>
                    <div class="rich-panel-body ">
                        <span>
                            <div>
                                <xsl:choose>
                                    <xsl:when test="detailedResult/DocumentWellFormed/Result = 'PASSED'">
                                        <p class="PASSED">The XML document is well-formed</p>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <p class="FAILED">The XML document is not well-formed</p>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </div>
                        </span>
                    </div>
                </div>
                <br/>

                <xsl:if test="count(detailedResult/DocumentValidXSD) = 1">
                <div class="rich-panel styleResultBackground">
                    <div class="rich-panel-header"><a name="xsd">XSD Validation detailed Results</a></div>
                    <div class="rich-panel-body">
                        <i>The document you have validated is supposed to be an XML document. The validator has checked if it is well-formed and has validated it against one ore several XSD schemas, results of those validations are gathered in this part.</i>
                        <xsl:if test="count(detailedResult/DocumentValidXSD) = 1">
                            <xsl:choose>
                                <xsl:when test="(detailedResult/DocumentValidXSD/Result='PASSED') or (detailedResult/DocumentValidXSD/nbOfErrors=0 and count(detailedResult/DocumentValidXSD/Result)=0)">
                                    <p class="PASSED">The XML document is valid regarding the schema</p>
                                </xsl:when>
                                <xsl:otherwise>
                                    <p class="FAILED">The XML document is not valid regarding the schema because of the following reasons: </p>
                                    <xsl:if test="count(detailedResult/DocumentValidXSD/*) &gt; 3">
                                        <ul>
                                            <xsl:for-each select="detailedResult/DocumentValidXSD/*">
                                                <xsl:if test="contains(current(), 'error')">
                                                    <li><xsl:value-of select="current()"/></li>
                                                </xsl:if>
                                            </xsl:for-each>
                                        </ul>
                                    </xsl:if>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </div>
                </div>
                <br/>
                </xsl:if>
                <br/>
                <xsl:if test="count(detailedResult/MDAValidation) = 1">
                <div class="rich-stglpanel" id="resultdetailedann">
                    <div class="rich-stglpanel-header" onclick="hideOrViewValidationDetailsMB();">
                        <p id="mbv" style="cursor: pointer;">Model Based Validation details
                            <span class="rich-stglpanel-marker">
                                <span id="resultdetailedann_switch_on" class="rich-stglpnl-marker" style="display: inline-block;"><xsl:text>[-]</xsl:text></span>
                                <span id="resultdetailedann_switch_off" class="rich-stglpnl-marker" style="display: none;"><xsl:text>[+]</xsl:text></span>
                            </span>
                        </p>
                    </div>
                    <div class="rich-stglpanel-body styleResultBackground" style="display: block;" id="resultdetailedann_body">
                        <table class="styleResultBackground">
                            <tbody>
                                <tr>
                                    <td>
                                        <b>Result</b>
                                    </td>
                                    <td>
                                        <xsl:if test="count(detailedResult/MDAValidation/Error) = 0">
                                            <div class="PASSED">PASSED</div>
                                        </xsl:if>
                                        <xsl:if test="count(detailedResult/MDAValidation/Error) &gt; 0">
                                            <div class="FAILED">FAILED</div>
                                        </xsl:if>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100px" valign="top">
                                        <b>Summary</b>
                                    </td>
                                    <td>
                                        <xsl:value-of select="count(detailedResult/MDAValidation/Error) + count(detailedResult/MDAValidation/Warning) + count(detailedResult/MDAValidation/Info) + count(detailedResult/MDAValidation/Note)"/> checks <br/>
                                        <xsl:value-of select="count(detailedResult/MDAValidation/Error)"/> errors <br/>
                                        <xsl:value-of select="count(detailedResult/MDAValidation/Warning)"/> warning <br/>
                                        <xsl:value-of select="count(detailedResult/MDAValidation/Info)"/> infos <br/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <b>HIDE : </b>
                        <input type="checkbox" onclick="hideOrUnhideMB(this)" name="Errors">Errors</input>
                        <input type="checkbox" onclick="hideOrUnhideMB(this)" name="Warnings">Warnings</input>
                        <input type="checkbox" onclick="hideOrUnhideMB(this)" name="Infos">Infos</input>
                        <input type="checkbox" onclick="hideOrUnhideMB(this)" name="Reports">Reports</input>
                        <xsl:if test="count(detailedResult/MDAValidation/Error) &gt; 0">
                            <div id="Errors_p">
                            <p><b>Errors</b></p>
                            <xsl:for-each select="detailedResult/MDAValidation/Error">
                                <xsl:call-template name="viewnotification">
                                    <xsl:with-param name="kind">errortwo</xsl:with-param>
                                </xsl:call-template>
                            </xsl:for-each>
                            </div>
                        </xsl:if>
                        <xsl:if test="count(detailedResult/MDAValidation/Warning) &gt; 0">
                            <div id="Warnings_p">
                            <p><b>Warnings</b></p>
                            <xsl:for-each select="detailedResult/MDAValidation/Warning">
                                <xsl:call-template name="viewnotification">
                                    <xsl:with-param name="kind">warning</xsl:with-param>
                                </xsl:call-template>
                            </xsl:for-each>
                            </div>
                        </xsl:if>
			             <xsl:if test="count(detailedResult/MDAValidation/Info) &gt; 0">
			                 <div id="Infos_p">
                            <p><b>Infos</b></p>
                            <xsl:for-each select="detailedResult/MDAValidation/Info">
                                <xsl:call-template name="viewnotification">
                                    <xsl:with-param name="kind">note</xsl:with-param>
                                </xsl:call-template>
                            </xsl:for-each>
			                 </div>
                        </xsl:if>
                        <xsl:if test="count(detailedResult/MDAValidation/Note) &gt; 0">
                            <div id="Reports_p">
                            <p><b>Reports</b></p>
                            <xsl:for-each select="detailedResult/MDAValidation/Note">
                                <xsl:if test="position() &lt; 101">
                                    <xsl:call-template name="viewnotification">
                                        <xsl:with-param name="kind">report</xsl:with-param>
                                    </xsl:call-template>
                                </xsl:if>
                            </xsl:for-each>
                            <xsl:if test="count(detailedResult/MDAValidation/Note) &gt; 100">
                                <table class="Report" width="98%">
                                    <tr>
                                        <td valign="top" width="100"><b>..........</b></td>
                                    </tr>
                                </table>
                                <br/>
                                <table class="moore" width="98%">
                                    <tr>
                                        <td valign="top" width="100"><b>All errors and warnings are shown above. If you want to view the complete report including all positive checks, please download the 'Model Based Validation Result'.</b>
                                            <br/>
                                            <span id="downloadspan"><script type="text/javascript">extractDownloadResultButton()</script></span>
                                        </td>
                                    </tr>
                                </table>
                            </xsl:if>
                            </div>
                        </xsl:if>

                    </div>
                </div>
                    </xsl:if>
            </body>
        </html>
    </xsl:template>

    <xsl:template name="viewnotification">
        <xsl:param name="kind"/>
        <table width="98%">
            <xsl:attribute name="class"><xsl:value-of select="$kind"/></xsl:attribute>
            <xsl:if test="count(Test) &gt; 0">
            <tr>
                <td valign="top" width="100"><b>Test</b></td>
                <td><xsl:value-of select="Test"/></td>
            </tr>
            </xsl:if>
            <xsl:if test="count(Location) &gt; 0">
            <tr>
                <td valign="top"><b>Location</b></td>
                <td><xsl:value-of select="Location"/> <xsl:if test="$viewdown = 'true'"><img src="/EVSClient/img/icons64/down.gif" style="vertical-align: middle;" width="15px" onclick="gotoo(this)"/></xsl:if></td>
            </tr>
            </xsl:if>

            <tr>
                <td valign="top"><b>Description</b></td>
                <td>
                    <xsl:value-of select="Description"/>
                    <xsl:if test="Identifiant and ($constraintPath != '#')">
                        <a>
                            <xsl:attribute name="href"><xsl:value-of select="$constraintPath"/><xsl:value-of select="Identifiant"/>.html</xsl:attribute>
                            <xsl:attribute name="target">_blank</xsl:attribute>
                            more...
                        </a>
                    </xsl:if>
                </td>
            </tr>
        </table>
        <br/>
    </xsl:template>
</xsl:stylesheet>