/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.xdstar.audit.validator.test;


import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileReadWrite {

	public static void printDoc(String doc, String name) throws IOException {
        FileWriter fw = null;
        IOException ioe = null;
        try {
            fw = new FileWriter(new File(name));
            fw.append(doc);
        } catch (IOException e) {
            ioe = e;
        } finally {
            IOUtils.closeQuietly(fw);
        }
        if (ioe != null) {
            throw ioe;
        }
    }

	public static String readDoc(String name){
		BufferedReader scanner = null;
		StringBuilder res = new StringBuilder();
		try {
			scanner = new BufferedReader(new FileReader(name));
			String line = scanner.readLine();
			while (line != null) {
				res.append(line + "\n");
				line = scanner.readLine();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		} 
		finally {
			try{
				scanner.close();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		return res.toString();
	}

}
