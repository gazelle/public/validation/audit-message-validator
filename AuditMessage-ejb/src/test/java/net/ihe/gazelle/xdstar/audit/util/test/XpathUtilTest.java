/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.xdstar.audit.util.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;

import javax.xml.namespace.NamespaceContext;

import net.ihe.gazelle.audit.message.utils.XpathUtils;
import net.ihe.gazelle.xdstar.audit.validator.test.FileReadWrite;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Node;

public class XpathUtilTest {
	
	private NamespaceContext xmlns = new NamespaceContext() {
		
		@Override
		public Iterator<String> getPrefixes(String namespaceURI) {
			// TODO Auto-generated method stub
			return null;
		}
		
		@Override
		public String getPrefix(String namespaceURI) {
			if ("http://www.w3.org/2003/05/soap-envelope".equals(namespaceURI)) {
				return "s";
			} 
			return null;
		}
		
		@Override
		public String getNamespaceURI(String prefix) {
			if ("s".equals(prefix)) {
				return "http://www.w3.org/2003/05/soap-envelope";
			}
			return null;
		}
	};

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEvaluateByString() throws Exception {
		String doc = FileReadWrite.readDoc("src/test/resources/samples/utils/xds.xml");
		Boolean toto = XpathUtils.evaluateByString(doc, "/s:Envelope/s:Header", xmlns);
		System.out.println(toto);
		assertTrue(toto);
		assertFalse(XpathUtils.evaluateByString(doc, "/s:Envelope/s:Headers", xmlns));
	}

	@Test
	public void testEvaluateByNode() throws Exception {
		String doc = FileReadWrite.readDoc("src/test/resources/samples/utils/xds.xml");
		Node toto = XpathUtils.getNodeFromString(doc, "s:Envelope");
		assertTrue(XpathUtils.evaluateByNode(toto, "/s:Envelope/s:Header", xmlns));
		assertFalse(XpathUtils.evaluateByString(doc, "/s:Envelope/s:Headers", xmlns));
	}

	@Test
	public void testGetNodeFromString() throws Exception {
		String doc = FileReadWrite.readDoc("src/test/resources/samples/utils/xds.xml");
		Node toto = XpathUtils.getNodeFromString(doc, "s:Envelope");
		assertTrue(toto != null);
	}

}
