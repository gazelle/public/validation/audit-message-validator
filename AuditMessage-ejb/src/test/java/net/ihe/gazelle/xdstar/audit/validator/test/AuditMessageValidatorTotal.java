/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.xdstar.audit.validator.test;

import net.ihe.gazelle.audit.message.action.AuditMessageMarshaller;
import net.ihe.gazelle.audit.message.desc.AuditMessageSpecification;
import net.ihe.gazelle.audit.message.validator.AuditMessageValidator;
import net.ihe.gazelle.validation.Notification;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

public class AuditMessageValidatorTotal {
	
	public static void main(String[] args) {
		(new AuditMessageValidatorTotal()).testValidate();
	}

	public void testValidate() {
		try {
			String document = FileReadWrite.readDoc("src/test/resources/samples/event/1.xml");
			String amsdoc = FileReadWrite.readDoc("src/test/resources/ams/iti55init.xml");
            AuditMessageSpecification ams = AuditMessageMarshaller
                    .xmlToObject(new ByteArrayInputStream(amsdoc.getBytes()));
            List<Notification> ln = new ArrayList<Notification>();
			AuditMessageValidator.instance.validate(document, ams, ln);
			for (Notification notification : ln) {
				System.out.println(notification.getClass().getSimpleName() + "::" +  notification.getDescription());
			}
			System.out.println(ln.size());
		} catch (Exception e) {
		}

	}
	
	
	

}
