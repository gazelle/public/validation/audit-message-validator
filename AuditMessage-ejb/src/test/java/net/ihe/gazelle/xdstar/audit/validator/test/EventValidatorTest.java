/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.xdstar.audit.validator.test;

import net.ihe.gazelle.audit.message.desc.AuditMessageSpecification;
import net.ihe.gazelle.audit.message.desc.ElementDescription;
import net.ihe.gazelle.audit.message.desc.EventIdentificationDesc;
import net.ihe.gazelle.audit.message.desc.Optionality;
import net.ihe.gazelle.audit.message.validator.EventValidator;
import net.ihe.gazelle.validation.Notification;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class EventValidatorTest extends EventValidator{
	
	List<Notification> diagnostic;
	
	String document;
	
	AuditMessageSpecification ams;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() {
		diagnostic = new ArrayList<Notification>();
		ams = new AuditMessageSpecification();
		ams.setIsDicomCompatible(false);
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testValidate() {
		ams.setEvent(new EventIdentificationDesc());
		ams.getEvent().setEventID(new ElementDescription());
		ams.getEvent().getEventID().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/event/1.xml");
		this.validate(document, ams, diagnostic);
		assertTrue(diagnostic.size()>0);
	}

	@Test
	public void testValidateEventIDOpt() {
		ams.setEvent(new EventIdentificationDesc());
		ams.getEvent().setEventID(new ElementDescription());
		ams.getEvent().getEventID().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/event/1.xml");
		this.validateEventIDOpt(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "Event/EventID is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/2.xml");
		this.validateEventIDOpt(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "Event/EventID is mandatory", diagnostic));
	}

	@Test
	public void testValidateEventIDValue() throws Exception {
		ams.setEvent(new EventIdentificationDesc());
		ams.getEvent().setEventID(new ElementDescription());
		ams.getEvent().getEventID().setValue("EV('110112', 'DCM', 'Query')");
		document = FileReadWrite.readDoc("src/test/resources/samples/event/3.xml");
		this.validateEventIDValue(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "/AuditMessage/EventIdentification/EventID@code must be '110112' if Event/EventID present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/4.xml");
		this.validateEventIDValue(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "/AuditMessage/EventIdentification/EventID@code must be '110112' if Event/EventID present", diagnostic));
	}

	@Test
	public void testValidateEventIDCodeValue() throws Exception {
		document = FileReadWrite.readDoc("src/test/resources/samples/event/3.xml");
		this.validateEventIDCodeValue("110112", document, ams, diagnostic);
		assertTrue(this.containDescription("note", "/AuditMessage/EventIdentification/EventID@code must be '110112' if Event/EventID present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/4.xml");
		this.validateEventIDCodeValue("110112", document, ams, diagnostic);
		assertTrue(this.containDescription("error", "/AuditMessage/EventIdentification/EventID@code must be '110112' if Event/EventID present", diagnostic));
	}

	@Test
	public void testValidateEventIDCodeSystemNameValue() throws Exception {
		document = FileReadWrite.readDoc("src/test/resources/samples/event/3.xml");
		this.validateEventIDCodeSystemNameValue("DCM", document, ams, diagnostic);
		assertTrue(this.containDescription("note", "/AuditMessage/EventIdentification/EventID@codeSystemName must be 'DCM' if Event/EventID present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/4.xml");
		this.validateEventIDCodeSystemNameValue("DCMM", document, ams, diagnostic);
		assertTrue(this.containDescription("error", "/AuditMessage/EventIdentification/EventID@codeSystemName must be 'DCMM' if Event/EventID present", diagnostic));
	}

	@Test
	public void testValidateEventIDDisplayNameValue() throws Exception {
		document = FileReadWrite.readDoc("src/test/resources/samples/event/3.xml");
		this.validateEventIDDisplayNameValue("Query", document, ams, diagnostic);
        assertTrue(this.containDescription("note", "/AuditMessage/EventIdentification/EventID@displayName should be 'Query' if Event/EventID present",
                diagnostic));
        diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/4.xml");
		this.validateEventIDDisplayNameValue("Queryy", document, ams, diagnostic);
        assertTrue(this.containDescription("warning",
                "/AuditMessage/EventIdentification/EventID@displayName should be 'Queryy' if Event/EventID present", diagnostic));
    }

	@Test
	public void testValidateEventActionCodeOpt() {
		ams.setEvent(new EventIdentificationDesc());
		ams.getEvent().setEventActionCode(new ElementDescription());
		ams.getEvent().getEventActionCode().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/event/5.xml");
		this.validateEventActionCodeOpt(document, ams, diagnostic);
//		for (Notification notification : diagnostic) {
//			System.out.println(notification.getClass().getSimpleName() + "::" +  notification.getDescription());
//		}
		assertTrue(this.containDescription("note", "Event/@EventActionCode is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/6.xml");
		this.validateEventActionCodeOpt(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "Event/@EventActionCode is mandatory", diagnostic));
	}

	@Test
	public void testValidateEventActionCodeValue() {
		ams.setEvent(new EventIdentificationDesc());
		ams.getEvent().setEventActionCode(new ElementDescription());
		ams.getEvent().getEventActionCode().setValue("E");
		document = FileReadWrite.readDoc("src/test/resources/samples/event/7.xml");
		this.validateEventActionCodeValue(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "/AuditMessage/EventIdentification/@EventActionCode must be 'E' if Event/EventActionCode present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/8.xml");
		this.validateEventActionCodeValue(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "/AuditMessage/EventIdentification/@EventActionCode must be 'E' if Event/EventActionCode present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/9.xml");
		this.validateEventActionCodeValue(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "/AuditMessage/EventIdentification/@EventActionCode must be 'E' if Event/EventActionCode present", diagnostic));
	}

	@Test
	public void testValidateEventActionCodeRegex() {
		ams.setEvent(new EventIdentificationDesc());
		ams.getEvent().setEventActionCode(new ElementDescription());
		ams.getEvent().getEventActionCode().setRegex("^[E-G]$");
		document = FileReadWrite.readDoc("src/test/resources/samples/event/7.xml");
		this.validateEventActionCodeRegex(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "/AuditMessage/EventIdentification/@EventActionCode must have this form : '^[E-G]$' if Event/EventActionCode present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/8.xml");
		this.validateEventActionCodeRegex(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "/AuditMessage/EventIdentification/@EventActionCode must have this form : '^[E-G]$' if Event/EventActionCode present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/9.xml");
		this.validateEventActionCodeRegex(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "/AuditMessage/EventIdentification/@EventActionCode must have this form : '^[E-G]$' if Event/EventActionCode present", diagnostic));
	}

	@Test
	public void testValidateEventDateTimeOpt() {
		ams.setEvent(new EventIdentificationDesc());
		ams.getEvent().setEventDateTime(new ElementDescription());
		ams.getEvent().getEventDateTime().setOptionality(Optionality.C);
		document = FileReadWrite.readDoc("src/test/resources/samples/event/10.xml");
		this.validateEventDateTimeOpt(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "Event/@EventDateTime has optionality 'C'", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/11.xml");
		this.validateEventDateTimeOpt(document, ams, diagnostic);
		assertTrue(this.containDescription("warning", "Event/@EventDateTime has optionality 'C'", diagnostic));

	}

	@Test
	public void testValidateEventDateTimeValue() {
		ams.setEvent(new EventIdentificationDesc());
		ams.getEvent().setEventDateTime(new ElementDescription());
		ams.getEvent().getEventDateTime().setValue("11");
		document = FileReadWrite.readDoc("src/test/resources/samples/event/12.xml");
		this.validateEventDateTimeValue(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "Event/@EventDateTime must be '11' if Event/EventActionCode present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/13.xml");
		this.validateEventDateTimeValue(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "Event/@EventDateTime must be '11' if Event/EventActionCode present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/14.xml");
		this.validateEventDateTimeValue(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "Event/@EventDateTime must be '11' if Event/EventActionCode present", diagnostic));
		
	}

	@Test
	public void testValidateEventDateTimeRegex() {
		ams.setEvent(new EventIdentificationDesc());
		ams.getEvent().setEventDateTime(new ElementDescription());
		ams.getEvent().getEventDateTime().setRegex("11\\s*");
		document = FileReadWrite.readDoc("src/test/resources/samples/event/12.xml");
		this.validateEventDateTimeRegex(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "Event/@EventDateTime must have this form : '11\\s*' if Event/@EventDateTime present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/14.xml");
		this.validateEventDateTimeRegex(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "Event/@EventDateTime must have this form : '11\\s*' if Event/@EventDateTime present", diagnostic));
	}

	@Test
	public void testValidateEventOutcomeIndicatorOpt() {
		ams.setEvent(new EventIdentificationDesc());
		ams.getEvent().setEventOutcomeIndicator(new ElementDescription());
		ams.getEvent().getEventOutcomeIndicator().setOptionality(Optionality.NA);
		document = FileReadWrite.readDoc("src/test/resources/samples/event/15.xml");
		this.validateEventOutcomeIndicatorOpt(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "Event/@EventOutcomeIndicator is prohibited", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/16.xml");
		this.validateEventOutcomeIndicatorOpt(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "Event/@EventOutcomeIndicator is prohibited", diagnostic));
	}

	@Test
	public void testValidateEventOutcomeIndicatorValue() {
		ams.setEvent(new EventIdentificationDesc());
		ams.getEvent().setEventOutcomeIndicator(new ElementDescription());
		ams.getEvent().getEventOutcomeIndicator().setValue("0");
		document = FileReadWrite.readDoc("src/test/resources/samples/event/15.xml");
		this.validateEventOutcomeIndicatorValue(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "Event/@EventOutcomeIndicator must be '0' if Event/@EventOutcomeIndicator present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/16.xml");
		this.validateEventOutcomeIndicatorValue(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "Event/@EventOutcomeIndicator must be '0' if Event/@EventOutcomeIndicator present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/17.xml");
		this.validateEventOutcomeIndicatorValue(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "Event/@EventOutcomeIndicator must be '0' if Event/@EventOutcomeIndicator present", diagnostic));
	}

	@Test
	public void testValidateEventOutcomeIndicatorRegex() {
		ams.setEvent(new EventIdentificationDesc());
		ams.getEvent().setEventOutcomeIndicator(new ElementDescription());
		ams.getEvent().getEventOutcomeIndicator().setRegex("^0{1}$");
		document = FileReadWrite.readDoc("src/test/resources/samples/event/15.xml");
		this.validateEventOutcomeIndicatorRegex(document, ams, diagnostic);
		assertTrue(this.containDescription("note", 
				"Event/@EventOutcomeIndicator must have this form : '^0{1}$' if Event/@EventOutcomeIndicator present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/17.xml");
		this.validateEventOutcomeIndicatorRegex(document, ams, diagnostic);
		assertTrue(this.containDescription("error", 
				"Event/@EventOutcomeIndicator must have this form : '^0{1}$' if Event/@EventOutcomeIndicator present", diagnostic));
	}

	@Test
	public void testValidateEventTypeCodeOpt() {
		ams.setEvent(new EventIdentificationDesc());
		ams.getEvent().setEventTypeCode(new ElementDescription());
		ams.getEvent().getEventTypeCode().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/event/18.xml");
		this.validateEventTypeCodeOpt(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "Event/EventTypeCode is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/19.xml");
		this.validateEventTypeCodeOpt(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "Event/EventTypeCode is mandatory", diagnostic));
	}

	@Test
	public void testValidateEventTypeCodeValueStringAuditMessageSpecificationListOfNotification() {
		ams.setEvent(new EventIdentificationDesc());
		ams.getEvent().setEventTypeCode(new ElementDescription());
		ams.getEvent().getEventTypeCode().setValue("EV('ITI-55','IHE Transactions','Cross Gateway Patient Discovery')");
		document = FileReadWrite.readDoc("src/test/resources/samples/event/20.xml");
		this.validateEventTypeCodeValue(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "Event/EventTypeCode@code must be 'ITI-55' if Event/EventTypeCode present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/21.xml");
		this.validateEventTypeCodeValue(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "Event/EventTypeCode@code must be 'ITI-55' if Event/EventTypeCode present", diagnostic));

	}

	
	@Test
	public void testValidateEventTypeCodeValueStringStringAuditMessageSpecificationListOfNotification() throws Exception {
		document = FileReadWrite.readDoc("src/test/resources/samples/event/20.xml");
		this.validateEventTypeCodeValue("ITI-55", document, ams, diagnostic);
		assertTrue(this.containDescription("note", "Event/EventTypeCode@code must be 'ITI-55' if Event/EventTypeCode present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/21.xml");
		this.validateEventTypeCodeValue("ITI-55", document, ams, diagnostic);
		assertTrue(this.containDescription("error", "Event/EventTypeCode@code must be 'ITI-55' if Event/EventTypeCode present", diagnostic));
	}

	@Test
	public void testValidateEventTypeCodeSystemNameValue() throws Exception {
		document = FileReadWrite.readDoc("src/test/resources/samples/event/20.xml");
		this.validateEventTypeCodeSystemNameValue("IHE Transactions", document, ams, diagnostic);
		assertTrue(this.containDescription("note", "Event/EventTypeCode@codeSystemName must be 'IHE Transactions' if Event/EventTypeCode present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/21.xml");
		this.validateEventTypeCodeSystemNameValue("IHE Transactionssss", document, ams, diagnostic);
		assertTrue(this.containDescription("error", 
				"Event/EventTypeCode@codeSystemName must be 'IHE Transactionssss' if Event/EventTypeCode present", diagnostic));
	}

	@Test
	public void testValidateEventTypeDisplayNameValue() throws Exception {
		document = FileReadWrite.readDoc("src/test/resources/samples/event/20.xml");
		this.validateEventTypeDisplayNameValue("Cross Gateway Patient Discovery", document, ams, diagnostic);
		assertTrue(this.containDescription("note",
                "Event/EventTypeCode@displayName should be 'Cross Gateway Patient Discovery' if Event/EventTypeCode present", diagnostic));
        diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/21.xml");
		this.validateEventTypeDisplayNameValue("Cross Gateway Patient Discoveriess", document, ams, diagnostic);
        assertTrue(this.containDescription("warning",
                "Event/EventTypeCode@displayName should be 'Cross Gateway Patient Discoveriess' if Event/EventTypeCode present", diagnostic));
    }
	
	@Test
	public void testValidateEventOutcomeDescriptionOpt() {
		ams.setIsDicomCompatible(true);
		ams.setEvent(new EventIdentificationDesc());
		ams.getEvent().setEventOutcomeDescription(new ElementDescription());
		ams.getEvent().getEventOutcomeDescription().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/event/eod_01.xml");
		this.validateEventOutcomeDescriptionOpt(document, ams, diagnostic);
//		for (Notification notification : diagnostic) {
//			System.out.println(notification.getClass().getSimpleName() + "::" +  notification.getDescription());
//		}
		assertTrue(this.containDescription("note", "Event/EventOutcomeDescription is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/eod_02.xml");
		this.validateEventOutcomeDescriptionOpt(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "Event/EventOutcomeDescription is mandatory", diagnostic));
	}

	@Test
	public void testValidateEventOutcomeDescriptionValue() {
		ams.setIsDicomCompatible(true);
		ams.setEvent(new EventIdentificationDesc());
		ams.getEvent().setEventOutcomeDescription(new ElementDescription());
		ams.getEvent().getEventOutcomeDescription().setValue("E");
		document = FileReadWrite.readDoc("src/test/resources/samples/event/eod_03.xml");
		this.validateEventOutcomeDescriptionValue(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "/AuditMessage/EventIdentification/EventOutcomeDescription must be 'E' if Event/EventOutcomeDescription present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/eod_04.xml");
		this.validateEventOutcomeDescriptionValue(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "/AuditMessage/EventIdentification/EventOutcomeDescription must be 'E' if Event/EventOutcomeDescription present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/eod_05.xml");
		this.validateEventOutcomeDescriptionValue(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "/AuditMessage/EventIdentification/EventOutcomeDescription must be 'E' if Event/EventOutcomeDescription present", diagnostic));
	}

	@Test
	public void testValidateEventOutcomeDescriptionRegex() {
		ams.setIsDicomCompatible(true);
		ams.setEvent(new EventIdentificationDesc());
		ams.getEvent().setEventOutcomeDescription(new ElementDescription());
		ams.getEvent().getEventOutcomeDescription().setRegex("^[E-G]$");
		document = FileReadWrite.readDoc("src/test/resources/samples/event/eod_06.xml");
		this.validateEventOutcomeDescriptionRegex(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "/AuditMessage/EventIdentification/EventOutcomeDescription must have this form : '^[E-G]$' if Event/EventOutcomeDescription present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/eod_07.xml");
		this.validateEventOutcomeDescriptionRegex(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "/AuditMessage/EventIdentification/EventOutcomeDescription must have this form : '^[E-G]$' if Event/EventOutcomeDescription present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/eod_08.xml");
		this.validateEventOutcomeDescriptionRegex(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "/AuditMessage/EventIdentification/EventOutcomeDescription must have this form : '^[E-G]$' if Event/EventOutcomeDescription present", diagnostic));
	}
	
	@Test
	public void testValidatePurposeOfUseOpt() {
		ams.setIsDicomCompatible(true);
		ams.setEvent(new EventIdentificationDesc());
		ams.getEvent().setPurposeOfUse(new ElementDescription());
		ams.getEvent().getPurposeOfUse().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/event/pou_01.xml");
		this.validatePurposeOfUseOpt(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "Event/PurposeOfUse is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/pou_02.xml");
		this.validatePurposeOfUseOpt(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "Event/PurposeOfUse is mandatory", diagnostic));
	}

	@Test
	public void testValidatePurposeOfUseValueStringAuditMessageSpecificationListOfNotification() {
		ams.setIsDicomCompatible(true);
		ams.setEvent(new EventIdentificationDesc());
		ams.getEvent().setPurposeOfUse(new ElementDescription());
		ams.getEvent().getPurposeOfUse().setValue("EV('ITI-55','IHE Transactions','Cross Gateway Patient Discovery')");
		document = FileReadWrite.readDoc("src/test/resources/samples/event/pou_03.xml");
		this.validatePurposeOfUseValue(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "Event/PurposeOfUse@csd-code must be 'ITI-55' if Event/PurposeOfUse present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/pou_04.xml");
		this.validatePurposeOfUseValue(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "Event/PurposeOfUse@csd-code must be 'ITI-55' if Event/PurposeOfUse present", diagnostic));

	}

	@Test
	public void testValidatePurposeOfUseSystemNameValue() throws Exception {
		ams.setIsDicomCompatible(true);
		document = FileReadWrite.readDoc("src/test/resources/samples/event/pou_07.xml");
		this.validatePurposeOfUseSystemNameValue("IHE Transactions", document, ams, diagnostic);
		assertTrue(this.containDescription("note", "Event/PurposeOfUse@codeSystemName must be 'IHE Transactions' if Event/PurposeOfUse present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/pou_08.xml");
		this.validatePurposeOfUseSystemNameValue("IHE Transactions", document, ams, diagnostic);
		assertTrue(this.containDescription("error", 
				"Event/PurposeOfUse@codeSystemName must be 'IHE Transactions' if Event/PurposeOfUse present", diagnostic));
	}

	@Test
	public void testValidatePurposeOfUseDisplayNameValue() throws Exception {
		ams.setIsDicomCompatible(true);
		document = FileReadWrite.readDoc("src/test/resources/samples/event/pou_09.xml");
		this.validatePurposeOfUseDisplayNameValue("Cross Gateway Patient Discovery", document, ams, diagnostic);
		assertTrue(this.containDescription("note",
                "Event/PurposeOfUse@originalText should be 'Cross Gateway Patient Discovery' if Event/PurposeOfUse present", diagnostic));
        diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/event/pou_10.xml");
		this.validatePurposeOfUseDisplayNameValue("Cross Gateway Patient Discovery", document, ams, diagnostic);
        assertTrue(this.containDescription("warning",
                "Event/PurposeOfUse@originalText should be 'Cross Gateway Patient Discovery' if Event/PurposeOfUse present", diagnostic));
    }
	
	
	
	private boolean containDescription(String kind, String desc, List<Notification> diagnostic){
		for (Notification notification : diagnostic) {
			if (notification.getClass().getSimpleName().equalsIgnoreCase(kind.toLowerCase())
					&& notification.getDescription().equals(desc)){
				return true;
			}
		}
		return false;
	}

}
