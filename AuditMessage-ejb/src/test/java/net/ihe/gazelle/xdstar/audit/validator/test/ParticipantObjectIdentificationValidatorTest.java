/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.xdstar.audit.validator.test;

import net.ihe.gazelle.audit.message.desc.AuditMessageSpecification;
import net.ihe.gazelle.audit.message.desc.ElementDescription;
import net.ihe.gazelle.audit.message.desc.Optionality;
import net.ihe.gazelle.audit.message.desc.ParticipantObjectIdentificationDesc;
import net.ihe.gazelle.audit.message.validator.ParticipantObjectIdentificationValidator;
import net.ihe.gazelle.validation.Notification;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class ParticipantObjectIdentificationValidatorTest extends ParticipantObjectIdentificationValidator{
	
	List<Notification> diagnostic;
	
	String document;
	
	AuditMessageSpecification ams;
	
	ParticipantObjectIdentificationDesc partid;
	
	String distinguisher = "@ParticipantObjectTypeCode='2' and @ParticipantObjectTypeCodeRole='24'";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		diagnostic = new ArrayList<Notification>();
		ams = new AuditMessageSpecification();
		partid = new ParticipantObjectIdentificationDesc();
		partid.setName("Query");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testValidate() {
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_01.xml");
		partid.setParticipantObjectTypeCode(new ElementDescription());
		partid.getParticipantObjectTypeCode().setOptionality(Optionality.M);
		this.validate(document, diagnostic, partid, distinguisher, false);
		assertTrue(diagnostic.size()>0);
	}

	@Test
	public void testValidateParticipantObjectTypeCodeOpt() {
		partid.setParticipantObjectTypeCode(new ElementDescription());
		partid.getParticipantObjectTypeCode().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_01.xml");
		this.validateParticipantObjectTypeCodeOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", "Query/ParticipantObjectTypeCode is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_02.xml");
		this.validateParticipantObjectTypeCodeOpt(document, diagnostic, partid, "@ParticipantObjectTypeCodeRole='24'");
		assertTrue(this.containDescription("error", "Query/ParticipantObjectTypeCode is mandatory", diagnostic));
	}

	@Test
	public void testValidateParticipantObjectTypeCodeValue() {
		partid.setParticipantObjectTypeCode(new ElementDescription());
		partid.getParticipantObjectTypeCode().setValue("2");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_01.xml");
		this.validateParticipantObjectTypeCodeValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/@ParticipantObjectTypeCode must be '2' if /Query/@ParticipantObjectTypeCode present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_02.xml");
		this.validateParticipantObjectTypeCodeValue(document, diagnostic, partid, "@ParticipantObjectTypeCodeRole='24'");
		assertTrue(this.containDescription("note", 
				"Query/@ParticipantObjectTypeCode must be '2' if /Query/@ParticipantObjectTypeCode present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_03.xml");
		this.validateParticipantObjectTypeCodeValue(document, diagnostic, partid, "@ParticipantObjectTypeCodeRole='24'");
		assertTrue(this.containDescription("error", 
				"Query/@ParticipantObjectTypeCode must be '2' if /Query/@ParticipantObjectTypeCode present", 
				diagnostic));
	}

	@Test
	public void testValidateParticipantObjectTypeCodeRegex() {
		partid.setParticipantObjectTypeCode(new ElementDescription());
		partid.getParticipantObjectTypeCode().setRegex("^2$");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_01.xml");
		this.validateParticipantObjectTypeCodeRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/@ParticipantObjectTypeCode must have this form '^2$' if Query/@ParticipantObjectTypeCode present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_02.xml");
		this.validateParticipantObjectTypeCodeRegex(document, diagnostic, partid, "@ParticipantObjectTypeCodeRole='24'");
		assertTrue(this.containDescription("note", 
				"Query/@ParticipantObjectTypeCode must have this form '^2$' if Query/@ParticipantObjectTypeCode present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_03.xml");
		this.validateParticipantObjectTypeCodeRegex(document, diagnostic, partid, "@ParticipantObjectTypeCodeRole='24'");
		assertTrue(this.containDescription("error", 
				"Query/@ParticipantObjectTypeCode must have this form '^2$' if Query/@ParticipantObjectTypeCode present", 
				diagnostic));
	}

	@Test
	public void testValidateParticipantObjectTypeCodeRoleOpt() {
		partid.setParticipantObjectTypeCodeRole(new ElementDescription());
		partid.getParticipantObjectTypeCodeRole().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_04.xml");
		this.validateParticipantObjectTypeCodeRoleOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", "Query/ParticipantObjectTypeCodeRole is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_05.xml");
		this.validateParticipantObjectTypeCodeRoleOpt(document, diagnostic, partid, "@ParticipantObjectTypeCode='2'");
		assertTrue(this.containDescription("error", "Query/ParticipantObjectTypeCodeRole is mandatory", diagnostic));
	}

	@Test
	public void testValidateParticipantObjectTypeCodeRoleValue() {
		partid.setParticipantObjectTypeCodeRole(new ElementDescription());
		partid.getParticipantObjectTypeCodeRole().setValue("24");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_04.xml");
		this.validateParticipantObjectTypeCodeRoleValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/@ParticipantObjectTypeCodeRole must be '24' if /Query/@ParticipantObjectTypeCodeRole present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_05.xml");
		this.validateParticipantObjectTypeCodeRoleValue(document, diagnostic, partid, "@ParticipantObjectTypeCode='2'");
		assertTrue(this.containDescription("note", 
				"Query/@ParticipantObjectTypeCodeRole must be '24' if /Query/@ParticipantObjectTypeCodeRole present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_06.xml");
		this.validateParticipantObjectTypeCodeRoleValue(document, diagnostic, partid, "@ParticipantObjectTypeCode='2'");
		assertTrue(this.containDescription("error", 
				"Query/@ParticipantObjectTypeCodeRole must be '24' if /Query/@ParticipantObjectTypeCodeRole present", 
				diagnostic));
	}

	@Test
	public void testValidateParticipantObjectTypeCodeRoleRegex() {
		partid.setParticipantObjectTypeCodeRole(new ElementDescription());
		partid.getParticipantObjectTypeCodeRole().setRegex("^24");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_04.xml");
		this.validateParticipantObjectTypeCodeRoleRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/@ParticipantObjectTypeCodeRole must have this form '^24' if Query/@ParticipantObjectTypeCodeRole present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_05.xml");
		this.validateParticipantObjectTypeCodeRoleRegex(document, diagnostic, partid, "@ParticipantObjectTypeCode='2'");
		assertTrue(this.containDescription("note", 
				"Query/@ParticipantObjectTypeCodeRole must have this form '^24' if Query/@ParticipantObjectTypeCodeRole present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_06.xml");
		this.validateParticipantObjectTypeCodeRoleRegex(document, diagnostic, partid, "@ParticipantObjectTypeCode='2'");
		assertTrue(this.containDescription("error", 
				"Query/@ParticipantObjectTypeCodeRole must have this form '^24' if Query/@ParticipantObjectTypeCodeRole present", 
				diagnostic));
	}

	@Test
	public void testValidateParticipantObjectDataLifeCycleOpt() {
		partid.setParticipantObjectDataLifeCycle(new ElementDescription());
		partid.getParticipantObjectDataLifeCycle().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_07.xml");
		this.validateParticipantObjectDataLifeCycleOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", "Query/@ParticipantObjectDataLifeCycle is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_08.xml");
		this.validateParticipantObjectDataLifeCycleOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", "Query/@ParticipantObjectDataLifeCycle is mandatory", diagnostic));
	}

	@Test
	public void testValidateParticipantObjectDataLifeCycleValue() {
		partid.setParticipantObjectDataLifeCycle(new ElementDescription());
		partid.getParticipantObjectDataLifeCycle().setValue("1");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_07.xml");
		this.validateParticipantObjectDataLifeCycleValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/@ParticipantObjectDataLifeCycle must be '1' if /Query/@ParticipantObjectDataLifeCycle present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_08.xml");
		this.validateParticipantObjectDataLifeCycleValue(document, diagnostic, partid, "@ParticipantObjectTypeCode='2'");
		assertTrue(this.containDescription("note", 
				"Query/@ParticipantObjectDataLifeCycle must be '1' if /Query/@ParticipantObjectDataLifeCycle present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_09.xml");
		this.validateParticipantObjectDataLifeCycleValue(document, diagnostic, partid, "@ParticipantObjectTypeCode='2'");
		assertTrue(this.containDescription("error", 
				"Query/@ParticipantObjectDataLifeCycle must be '1' if /Query/@ParticipantObjectDataLifeCycle present", 
				diagnostic));
	}

	@Test
	public void testValidateParticipantObjectDataLifeCycleRegex() {
		partid.setParticipantObjectDataLifeCycle(new ElementDescription());
		partid.getParticipantObjectDataLifeCycle().setRegex("1");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_07.xml");
		this.validateParticipantObjectDataLifeCycleRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/@ParticipantObjectDataLifeCycle must have this form '1' if Query/@ParticipantObjectDataLifeCycle present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_08.xml");
		this.validateParticipantObjectDataLifeCycleRegex(document, diagnostic, partid, "@ParticipantObjectTypeCode='2'");
		assertTrue(this.containDescription("note", 
				"Query/@ParticipantObjectDataLifeCycle must have this form '1' if Query/@ParticipantObjectDataLifeCycle present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_09.xml");
		this.validateParticipantObjectDataLifeCycleRegex(document, diagnostic, partid, "@ParticipantObjectTypeCode='2'");
		assertTrue(this.containDescription("error", 
				"Query/@ParticipantObjectDataLifeCycle must have this form '1' if Query/@ParticipantObjectDataLifeCycle present", 
				diagnostic));
	}

	@Test
	public void testValidateParticipantObjectIDTypeCodeOpt() {
		partid.setParticipantObjectIDTypeCode(new ElementDescription());
		partid.getParticipantObjectIDTypeCode().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_10.xml");
		this.validateParticipantObjectIDTypeCodeOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", "Query/ParticipantObjectIDTypeCode is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_11.xml");
		this.validateParticipantObjectIDTypeCodeOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", "Query/ParticipantObjectIDTypeCode is mandatory", diagnostic));
	}

	@Test
	public void testValidateParticipantObjectIDTypeCodeValue() {
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_12.xml");
		partid.setParticipantObjectIDTypeCode(new ElementDescription());
		partid.getParticipantObjectIDTypeCode().setValue("EV('ITI-55','IHE Transactions','Cross Gateway Patient Discovery')");
		this.validateParticipantObjectIDTypeCodeValue(document, diagnostic, partid, distinguisher, false);
		assertTrue(this.containDescription("note", 
				"Query/ParticipantObjectIDTypeCode@code must be 'ITI-55' if Query/ParticipantObjectIDTypeCode present", 
				diagnostic));
	}

	@Test
	public void testValidateParticipantObjectIDTypeCodeCodeValue() throws Exception {
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_12.xml");
		this.validateParticipantObjectIDTypeCodeCodeValue("ITI-55", document, diagnostic, partid, distinguisher, false);
		assertTrue(this.containDescription("note", 
				"Query/ParticipantObjectIDTypeCode@code must be 'ITI-55' if Query/ParticipantObjectIDTypeCode present", 
				diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_13.xml");
		this.validateParticipantObjectIDTypeCodeCodeValue("ITI-55", document, diagnostic, partid, distinguisher, false);
		assertTrue(this.containDescription("error", 
				"Query/ParticipantObjectIDTypeCode@code must be 'ITI-55' if Query/ParticipantObjectIDTypeCode present",
				diagnostic));

	}

	@Test
	public void testValidateParticipantObjectIDTypeCodeCodeSystemNameValue() throws Exception {
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_12.xml");
		this.validateParticipantObjectIDTypeCodeCodeSystemNameValue("IHE Transactions", document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/ParticipantObjectIDTypeCode@codeSystemName must be 'IHE Transactions' if Query/ParticipantObjectIDTypeCode present", 
				diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_13.xml");
		this.validateParticipantObjectIDTypeCodeCodeSystemNameValue("IHE Transactions", document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/ParticipantObjectIDTypeCode@codeSystemName must be 'IHE Transactions' if Query/ParticipantObjectIDTypeCode present",
				diagnostic));

	}

	@Test
	public void testValidateParticipantObjectIDTypeCodeDisplayNameValue() throws Exception {
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_12.xml");
		this.validateParticipantObjectIDTypeCodeDisplayNameValue("Cross Gateway Patient Discovery", document, diagnostic, partid, distinguisher, false);
		assertTrue(this.containDescription("note",
                "Query/ParticipantObjectIDTypeCode@displayName should be 'Cross Gateway Patient Discovery' if Query/ParticipantObjectIDTypeCode present",

                diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_13.xml");
		this.validateParticipantObjectIDTypeCodeDisplayNameValue("Cross Gateway Patient Discovery", document, diagnostic, partid, distinguisher,
                false);
        assertTrue(this.containDescription("warning",
                "Query/ParticipantObjectIDTypeCode@displayName should be 'Cross Gateway Patient Discovery' if Query/ParticipantObjectIDTypeCode " +
                        "present",
                diagnostic));

	}

	@Test
	public void testValidateParticipantObjectSensitivityOpt() {
		partid.setParticipantObjectSensitivity(new ElementDescription());
		partid.getParticipantObjectSensitivity().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_14.xml");
		this.validateParticipantObjectSensitivityOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", "Query/@ParticipantObjectSensitivity is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_15.xml");
		this.validateParticipantObjectSensitivityOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", "Query/@ParticipantObjectSensitivity is mandatory", diagnostic));
	}

	@Test
	public void testValidateParticipantObjectSensitivityValue() {
		partid.setParticipantObjectSensitivity(new ElementDescription());
		partid.getParticipantObjectSensitivity().setValue("test");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_14.xml");
		this.validateParticipantObjectSensitivityValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/@ParticipantObjectSensitivity must be 'test' if /Query/@ParticipantObjectSensitivity present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_15.xml");
		this.validateParticipantObjectSensitivityValue(document, diagnostic, partid, "@ParticipantObjectTypeCode='2'");
		assertTrue(this.containDescription("note", 
				"Query/@ParticipantObjectSensitivity must be 'test' if /Query/@ParticipantObjectSensitivity present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_16.xml");
		this.validateParticipantObjectSensitivityValue(document, diagnostic, partid, "@ParticipantObjectTypeCode='2'");
		assertTrue(this.containDescription("error", 
				"Query/@ParticipantObjectSensitivity must be 'test' if /Query/@ParticipantObjectSensitivity present", 
				diagnostic));
	}

	@Test
	public void testValidateParticipantObjectSensitivityRegex() {
		partid.setParticipantObjectSensitivity(new ElementDescription());
		partid.getParticipantObjectSensitivity().setRegex("test$");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_14.xml");
		this.validateParticipantObjectSensitivityRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/@ParticipantObjectSensitivity must have this form 'test$' if Query/@ParticipantObjectSensitivity present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_15.xml");
		this.validateParticipantObjectSensitivityRegex(document, diagnostic, partid, "@ParticipantObjectTypeCode='2'");
		assertTrue(this.containDescription("note", 
				"Query/@ParticipantObjectSensitivity must have this form 'test$' if Query/@ParticipantObjectSensitivity present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_16.xml");
		this.validateParticipantObjectSensitivityRegex(document, diagnostic, partid, "@ParticipantObjectTypeCode='2'");
		assertTrue(this.containDescription("error", 
				"Query/@ParticipantObjectSensitivity must have this form 'test$' if Query/@ParticipantObjectSensitivity present", 
				diagnostic));
	}

	@Test
	public void testValidateParticipantObjectIDOpt() {
		partid.setParticipantObjectID(new ElementDescription());
		partid.getParticipantObjectID().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_17.xml");
		this.validateParticipantObjectIDOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", "Query/ParticipantObjectID is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_18.xml");
		this.validateParticipantObjectIDOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", "Query/ParticipantObjectID is mandatory", diagnostic));
	}

	@Test
	public void testValidateParticipantObjectIDValue() {
		partid.setParticipantObjectID(new ElementDescription());
		partid.getParticipantObjectID().setValue("id");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_17.xml");
		this.validateParticipantObjectIDValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/@ParticipantObjectID must be 'id' if /Query/@ParticipantObjectID present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_18.xml");
		this.validateParticipantObjectIDValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/@ParticipantObjectID must be 'id' if /Query/@ParticipantObjectID present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_19.xml");
		this.validateParticipantObjectIDValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/@ParticipantObjectID must be 'id' if /Query/@ParticipantObjectID present", 
				diagnostic));
	}

	@Test
	public void testValidateParticipantObjectIDRegex() {
		partid.setParticipantObjectID(new ElementDescription());
		partid.getParticipantObjectID().setRegex("id");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_17.xml");
		this.validateParticipantObjectIDRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/@ParticipantObjectID must have this form 'id' if Query/@ParticipantObjectID present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_18.xml");
		this.validateParticipantObjectIDRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/@ParticipantObjectID must have this form 'id' if Query/@ParticipantObjectID present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_19.xml");
		this.validateParticipantObjectIDRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/@ParticipantObjectID must have this form 'id' if Query/@ParticipantObjectID present", 
				diagnostic));
	}

	@Test
	public void testValidateParticipantObjectNameOpt() {
		partid.setParticipantObjectName(new ElementDescription());
		partid.getParticipantObjectName().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_20.xml");
		this.validateParticipantObjectNameOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", "Query/ParticipantObjectName is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_21.xml");
		this.validateParticipantObjectNameOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", "Query/ParticipantObjectName is mandatory", diagnostic));
	}

	@Test
	public void testValidateParticipantObjectNameValue() {
		partid.setParticipantObjectName(new ElementDescription());
		partid.getParticipantObjectName().setValue("MOI");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_20.xml");
		this.validateParticipantObjectNameValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/ParticipantObjectName must be 'MOI' if /Query/ParticipantObjectName present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_21.xml");
		this.validateParticipantObjectNameValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/ParticipantObjectName must be 'MOI' if /Query/ParticipantObjectName present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_22.xml");
		this.validateParticipantObjectNameValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/ParticipantObjectName must be 'MOI' if /Query/ParticipantObjectName present", 
				diagnostic));
	}

	@Test
	public void testValidateParticipantObjectNameRegex() {
		partid.setParticipantObjectName(new ElementDescription());
		partid.getParticipantObjectName().setRegex("MOI");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_20.xml");
		this.validateParticipantObjectNameRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/ParticipantObjectName must have this form 'MOI' if Query/ParticipantObjectName present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_21.xml");
		this.validateParticipantObjectNameRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/ParticipantObjectName must have this form 'MOI' if Query/ParticipantObjectName present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_22.xml");
		this.validateParticipantObjectNameRegex(document, diagnostic, partid, distinguisher);
//		for (Notification notification : diagnostic) {
//			System.out.println(notification.getClass().getSimpleName() + "::" +  notification.getDescription());
//		}
		assertTrue(this.containDescription("error", 
				"Query/ParticipantObjectName must have this form 'MOI' if Query/ParticipantObjectName present", 
				diagnostic));
	}

	@Test
	public void testValidateParticipantObjectQueryOpt() {
		partid.setParticipantObjectQuery(new ElementDescription());
		partid.getParticipantObjectQuery().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_23.xml");
		this.validateParticipantObjectQueryOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", "Query/ParticipantObjectQuery is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_24.xml");
		this.validateParticipantObjectQueryOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", "Query/ParticipantObjectQuery is mandatory", diagnostic));
	}

	@Test
	public void testValidateParticipantObjectQueryValue() {
		partid.setParticipantObjectQuery(new ElementDescription());
		partid.getParticipantObjectQuery().setValue("VXRpbC5qYXhiTWFyc2hhbFRvU3RyaW5nKGJvZHkp");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_23.xml");
		this.validateParticipantObjectQueryValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/ParticipantObjectQuery must be 'VXRpbC5qYXhiTWFyc2hhbFRvU3RyaW5nKGJvZHkp' if /Query/ParticipantObjectQuery present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_24.xml");
		this.validateParticipantObjectQueryValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/ParticipantObjectQuery must be 'VXRpbC5qYXhiTWFyc2hhbFRvU3RyaW5nKGJvZHkp' if /Query/ParticipantObjectQuery present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_25.xml");
		this.validateParticipantObjectQueryValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/ParticipantObjectQuery must be 'VXRpbC5qYXhiTWFyc2hhbFRvU3RyaW5nKGJvZHkp' if /Query/ParticipantObjectQuery present", 
				diagnostic));
	}

	@Test
	public void testValidateParticipantObjectQueryRegex() {
		partid.setParticipantObjectQuery(new ElementDescription());
		partid.getParticipantObjectQuery().setRegex("VXRpbC5qYXhiTWFyc2hhbFRvU3RyaW5nKGJvZHkp");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_23.xml");
		this.validateParticipantObjectQueryRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/ParticipantObjectQuery must have this form 'VXRpbC5qYXhiTWFyc2hhbFRvU3RyaW5nKGJvZHkp' if Query/ParticipantObjectQuery present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_24.xml");
		this.validateParticipantObjectQueryRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/ParticipantObjectQuery must have this form 'VXRpbC5qYXhiTWFyc2hhbFRvU3RyaW5nKGJvZHkp' if Query/ParticipantObjectQuery present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_25.xml");
		this.validateParticipantObjectQueryRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/ParticipantObjectQuery must have this form 'VXRpbC5qYXhiTWFyc2hhbFRvU3RyaW5nKGJvZHkp' if Query/ParticipantObjectQuery present", 
				diagnostic));
	}

	@Test
	public void testValidateParticipantObjectDetailOpt() {
		partid.setParticipantObjectDetail(new ElementDescription());
		partid.getParticipantObjectDetail().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_26.xml");
		this.validateParticipantObjectDetailOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", "Query/ParticipantObjectDetail is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_27.xml");
		this.validateParticipantObjectDetailOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", "Query/ParticipantObjectDetail is mandatory", diagnostic));
	}
	
	@Test
	public void testValidateParticipantObjectDescriptionOpt() {
		partid.setParticipantObjectDescription(new ElementDescription());
		partid.getParticipantObjectDescription().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_28.xml");
		this.validateParticipantObjectDescriptionOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", "Query/ParticipantObjectDescription is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_29.xml");
		this.validateParticipantObjectDescriptionOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", "Query/ParticipantObjectDescription is mandatory", diagnostic));
	}

	@Test
	public void testValidateParticipantObjectDescriptionValue() {
		partid.setParticipantObjectDescription(new ElementDescription());
		partid.getParticipantObjectDescription().setValue("MOI");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_30.xml");
		this.validateParticipantObjectDescriptionValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/ParticipantObjectDescription/text() must be 'MOI' if /Query/ParticipantObjectDescription present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_31.xml");
		this.validateParticipantObjectDescriptionValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/ParticipantObjectDescription/text() must be 'MOI' if /Query/ParticipantObjectDescription present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_32.xml");
		this.validateParticipantObjectDescriptionValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/ParticipantObjectDescription/text() must be 'MOI' if /Query/ParticipantObjectDescription present", 
				diagnostic));
	}

	@Test
	public void testValidateParticipantObjectDescriptionRegex() {
		partid.setParticipantObjectDescription(new ElementDescription());
		partid.getParticipantObjectDescription().setRegex("MOI");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_33.xml");
		this.validateParticipantObjectDescriptionRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/ParticipantObjectDescription/text() must have this form 'MOI' if Query/ParticipantObjectDescription present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_34.xml");
		this.validateParticipantObjectDescriptionRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/ParticipantObjectDescription/text() must have this form 'MOI' if Query/ParticipantObjectDescription present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_35.xml");
		this.validateParticipantObjectDescriptionRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/ParticipantObjectDescription/text() must have this form 'MOI' if Query/ParticipantObjectDescription present", 
				diagnostic));
	}
	
	@Test
	public void testValidateMPPSOpt() {
		partid.setMpps(new ElementDescription());
		partid.getMpps().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_36.xml");
		this.validateMPPSOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", "Query/MPPS is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_37.xml");
		this.validateMPPSOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", "Query/MPPS is mandatory", diagnostic));
	}

	@Test
	public void testValidateMPPSValue() {
		partid.setMpps(new ElementDescription());
		partid.getMpps().setValue("MOI");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_38.xml");
		this.validateMPPSValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/MPPS/@UID must be 'MOI' if /Query/MPPS/@UID present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_39.xml");
		this.validateMPPSValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/MPPS/@UID must be 'MOI' if /Query/MPPS/@UID present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_40.xml");
		this.validateMPPSValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/MPPS/@UID must be 'MOI' if /Query/MPPS/@UID present", 
				diagnostic));
	}

	@Test
	public void testValidateMPPSRegex() {
		partid.setMpps(new ElementDescription());
		partid.getMpps().setRegex("MOI");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_41.xml");
		this.validateMPPSRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/MPPS/@UID must have this form 'MOI' if Query/MPPS/@UID present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_42.xml");
		this.validateMPPSRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/MPPS/@UID must have this form 'MOI' if Query/MPPS/@UID present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_43.xml");
		this.validateMPPSRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/MPPS/@UID must have this form 'MOI' if Query/MPPS/@UID present", 
				diagnostic));
	}
	
	@Test
	public void testValidateAccessionOpt() {
		partid.setAccession(new ElementDescription());
		partid.getAccession().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_44.xml");
		this.validateAccessionOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", "Query/Accession is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_45.xml");
		this.validateAccessionOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", "Query/Accession is mandatory", diagnostic));
	}

	@Test
	public void testValidateAccessionValue() {
		partid.setAccession(new ElementDescription());
		partid.getAccession().setValue("1");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_46.xml");
		this.validateAccessionValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/Accession/@Number must be '1' if /Query/Accession/@Number present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_47.xml");
		this.validateAccessionValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/Accession/@Number must be '1' if /Query/Accession/@Number present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_48.xml");
		this.validateAccessionValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/Accession/@Number must be '1' if /Query/Accession/@Number present", 
				diagnostic));
	}

	@Test
	public void testValidateAccessionRegex() {
		partid.setAccession(new ElementDescription());
		partid.getAccession().setRegex("1");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_49.xml");
		this.validateAccessionRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/Accession/@Number must have this form '1' if Query/Accession/@Number present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_50.xml");
		this.validateAccessionRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/Accession/@Number must have this form '1' if Query/Accession/@Number present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_51.xml");
		this.validateAccessionRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/Accession/@Number must have this form '1' if Query/Accession/@Number present", 
				diagnostic));
	}
	
	@Test
	public void testValidateSOPClassOpt() {
		partid.setSopClass(new ElementDescription());
		partid.getSopClass().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_52.xml");
		this.validateSOPClassOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", "Query/SOPClass is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_53.xml");
		this.validateSOPClassOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", "Query/SOPClass is mandatory", diagnostic));
	}

	@Test
	public void testValidateSOPClassValue() {
		partid.setSopClass(new ElementDescription());
		partid.getSopClass().setValue("1");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_54.xml");
		this.validateSOPClassValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/SOPClass/@UID must be '1' if /Query/SOPClass/@UID present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_55.xml");
		this.validateSOPClassValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/SOPClass/@UID must be '1' if /Query/SOPClass/@UID present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_56.xml");
		this.validateSOPClassValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/SOPClass/@UID must be '1' if /Query/SOPClass/@UID present", 
				diagnostic));
	}

	@Test
	public void testValidateSOPClassRegex() {
		partid.setSopClass(new ElementDescription());
		partid.getSopClass().setRegex("1");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_57.xml");
		this.validateSOPClassRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/SOPClass/@UID must have this form '1' if Query/SOPClass/@UID present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_58.xml");
		this.validateSOPClassRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/SOPClass/@UID must have this form '1' if Query/SOPClass/@UID present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_59.xml");
		this.validateSOPClassRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/SOPClass/@UID must have this form '1' if Query/SOPClass/@UID present", 
				diagnostic));
	}
	
	@Test
	public void testValidateParticipantObjectContainsStudyOpt() {
		partid.setParticipantObjectContainsStudy(new ElementDescription());
		partid.getParticipantObjectContainsStudy().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_60.xml");
		this.validateParticipantObjectContainsStudyOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", "Query/ParticipantObjectContainsStudy/StudyIDs is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_61.xml");
		this.validateParticipantObjectContainsStudyOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", "Query/ParticipantObjectContainsStudy/StudyIDs is mandatory", diagnostic));
	}

	@Test
	public void testValidateParticipantObjectContainsStudyValue() {
		partid.setParticipantObjectContainsStudy(new ElementDescription());
		partid.getParticipantObjectContainsStudy().setValue("1");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_62.xml");
		this.validateParticipantObjectContainsStudyValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/ParticipantObjectContainsStudy/StudyIDs/@UID must be '1' if /Query/ParticipantObjectContainsStudy/StudyIDs/@UID present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_63.xml");
		this.validateParticipantObjectContainsStudyValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/ParticipantObjectContainsStudy/StudyIDs/@UID must be '1' if /Query/ParticipantObjectContainsStudy/StudyIDs/@UID present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_64.xml");
		this.validateParticipantObjectContainsStudyValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/ParticipantObjectContainsStudy/StudyIDs/@UID must be '1' if /Query/ParticipantObjectContainsStudy/StudyIDs/@UID present", 
				diagnostic));
	}

	@Test
	public void testValidateParticipantObjectContainsStudyRegex() {
		partid.setParticipantObjectContainsStudy(new ElementDescription());
		partid.getParticipantObjectContainsStudy().setRegex("1");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_65.xml");
		this.validateParticipantObjectContainsStudyRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/ParticipantObjectContainsStudy/StudyIDs/@UID must have this form '1' if Query/ParticipantObjectContainsStudy/StudyIDs/@UID present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_66.xml");
		this.validateParticipantObjectContainsStudyRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/ParticipantObjectContainsStudy/StudyIDs/@UID must have this form '1' if Query/ParticipantObjectContainsStudy/StudyIDs/@UID present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_67.xml");
		this.validateParticipantObjectContainsStudyRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/ParticipantObjectContainsStudy/StudyIDs/@UID must have this form '1' if Query/ParticipantObjectContainsStudy/StudyIDs/@UID present", 
				diagnostic));
	}
	
	@Test
	public void testValidateEncryptedOpt() {
		partid.setEncrypted(new ElementDescription());
		partid.getEncrypted().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_68.xml");
		this.validateEncryptedOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", "Query/Encrypted is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_69.xml");
		this.validateEncryptedOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", "Query/Encrypted is mandatory", diagnostic));
	}

	@Test
	public void testValidateEncryptedValue() {
		partid.setEncrypted(new ElementDescription());
		partid.getEncrypted().setValue("true");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_70.xml");
		this.validateEncryptedValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/Encrypted/text() must be 'true' if /Query/Encrypted present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_71.xml");
		this.validateEncryptedValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/Encrypted/text() must be 'true' if /Query/Encrypted present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_72.xml");
		this.validateEncryptedValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/Encrypted/text() must be 'true' if /Query/Encrypted present", 
				diagnostic));
	}

	@Test
	public void testValidateEncryptedRegex() {
		partid.setEncrypted(new ElementDescription());
		partid.getEncrypted().setRegex("true");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_73.xml");
		this.validateEncryptedRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/Encrypted/text() must have this form 'true' if Query/Encrypted present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_74.xml");
		this.validateEncryptedRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/Encrypted/text() must have this form 'true' if Query/Encrypted present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_75.xml");
		this.validateEncryptedRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/Encrypted/text() must have this form 'true' if Query/Encrypted present", 
				diagnostic));
	}
	
	@Test
	public void testValidateAnonymizedOpt() {
		partid.setAnonymized(new ElementDescription());
		partid.getAnonymized().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_76.xml");
		this.validateAnonymizedOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", "Query/Anonymized is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_77.xml");
		this.validateAnonymizedOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", "Query/Anonymized is mandatory", diagnostic));
	}

	@Test
	public void testValidateAnonymizedValue() {
		partid.setAnonymized(new ElementDescription());
		partid.getAnonymized().setValue("true");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_78.xml");
		this.validateAnonymizedValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/Anonymized/text() must be 'true' if /Query/Anonymized present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_79.xml");
		this.validateAnonymizedValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/Anonymized/text() must be 'true' if /Query/Anonymized present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_80.xml");
		this.validateAnonymizedValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/Anonymized/text() must be 'true' if /Query/Anonymized present", 
				diagnostic));
	}

	@Test
	public void testValidateAnonymizedRegex() {
		partid.setAnonymized(new ElementDescription());
		partid.getAnonymized().setRegex("true");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_81.xml");
		this.validateAnonymizedRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/Anonymized/text() must have this form 'true' if Query/Anonymized present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_82.xml");
		this.validateAnonymizedRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/Anonymized/text() must have this form 'true' if Query/Anonymized present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_83.xml");
		this.validateAnonymizedRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/Anonymized/text() must have this form 'true' if Query/Anonymized present", 
				diagnostic));
	}
	
	@Test
	public void testValidateInstanceOpt() {
		partid.setInstance(new ElementDescription());
		partid.getInstance().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_84.xml");
		this.validateInstanceOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", "Query/SOPClass/Instance is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_85.xml");
		this.validateInstanceOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", "Query/SOPClass/Instance is mandatory", diagnostic));
	}

	@Test
	public void testValidateInstanceValue() {
		partid.setInstance(new ElementDescription());
		partid.getInstance().setValue("1");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_86.xml");
		this.validateInstanceValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/SOPClass/Instance/@UID must be '1' if /Query/SOPClass/Instance/@UID present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_87.xml");
		this.validateInstanceValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/SOPClass/Instance/@UID must be '1' if /Query/SOPClass/Instance/@UID present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_88.xml");
		this.validateInstanceValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/SOPClass/Instance/@UID must be '1' if /Query/SOPClass/Instance/@UID present", 
				diagnostic));
	}

	@Test
	public void testValidateInstanceRegex() {
		partid.setInstance(new ElementDescription());
		partid.getInstance().setRegex("1");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_89.xml");
		this.validateInstanceRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/SOPClass/Instance/@UID must have this form '1' if Query/SOPClass/Instance/@UID present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_90.xml");
		this.validateInstanceRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/SOPClass/Instance/@UID must have this form '1' if Query/SOPClass/Instance/@UID present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_91.xml");
		this.validateInstanceRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/SOPClass/Instance/@UID must have this form '1' if Query/SOPClass/Instance/@UID present", 
				diagnostic));
	}
	
	@Test
	public void testValidateNumberOfInstancesOpt() {
		partid.setNumberOfInstances(new ElementDescription());
		partid.getNumberOfInstances().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_92.xml");
		this.validateNumberOfInstancesOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", "Query/SOPClass/@NumberOfInstances is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_93.xml");
		this.validateNumberOfInstancesOpt(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", "Query/SOPClass/@NumberOfInstances is mandatory", diagnostic));
	}

	@Test
	public void testValidateNumberOfInstancesValue() {
		partid.setNumberOfInstances(new ElementDescription());
		partid.getNumberOfInstances().setValue("1");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_94.xml");
		this.validateNumberOfInstancesValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/SOPClass/@NumberOfInstances must be '1' if /Query/SOPClass/@NumberOfInstances present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_95.xml");
		this.validateNumberOfInstancesValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/SOPClass/@NumberOfInstances must be '1' if /Query/SOPClass/@NumberOfInstances present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_96.xml");
		this.validateNumberOfInstancesValue(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/SOPClass/@NumberOfInstances must be '1' if /Query/SOPClass/@NumberOfInstances present", 
				diagnostic));
	}

	@Test
	public void testValidateNumberOfInstancesRegex() {
		partid.setNumberOfInstances(new ElementDescription());
		partid.getNumberOfInstances().setRegex("1");
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_97.xml");
		this.validateNumberOfInstancesRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/SOPClass/@NumberOfInstances must have this form '1' if Query/SOPClass/@NumberOfInstances present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_98.xml");
		this.validateNumberOfInstancesRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("note", 
				"Query/SOPClass/@NumberOfInstances must have this form '1' if Query/SOPClass/@NumberOfInstances present", 
				diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/poi/poi_99.xml");
		this.validateNumberOfInstancesRegex(document, diagnostic, partid, distinguisher);
		assertTrue(this.containDescription("error", 
				"Query/SOPClass/@NumberOfInstances must have this form '1' if Query/SOPClass/@NumberOfInstances present", 
				diagnostic));
	}
	
	private boolean containDescription(String kind, String desc, List<Notification> diagnostic){
		for (Notification notification : diagnostic) {
			if (notification.getClass().getSimpleName().equalsIgnoreCase(kind.toLowerCase())
					&& notification.getDescription().equals(desc)){
				return true;
			}
		}
		return false;
	}

}
