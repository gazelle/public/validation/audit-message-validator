/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.xdstar.audit.validator.test;

import net.ihe.gazelle.audit.message.desc.ActiveParticipantDescription;
import net.ihe.gazelle.audit.message.desc.AuditMessageSpecification;
import net.ihe.gazelle.audit.message.desc.ElementDescription;
import net.ihe.gazelle.audit.message.desc.Optionality;
import net.ihe.gazelle.audit.message.validator.ActiveParticipantValidator;
import net.ihe.gazelle.validation.Notification;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class ActiveParticipantValidatorTest extends ActiveParticipantValidator {

    List<Notification> diagnostic;

    String document;

    AuditMessageSpecification ams;

    static String distinguisher = "RoleIDCode[@code='110153']";

    ActiveParticipantDescription act;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        diagnostic = new ArrayList<Notification>();
        ams = new AuditMessageSpecification();
        ams.setIsDicomCompatible(false);
        act = new ActiveParticipantDescription();
        act.setName("Source");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testValidate() {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_01.xml");
        ams.getActiveParticipants().add(new ActiveParticipantDescription());
        ams.getActiveParticipants().get(0).setDistinguisher("RoleIDCode[@code='110153']");
        ams.getActiveParticipants().get(0).setName("Source");
        ams.getActiveParticipants().get(0).setUserID(new ElementDescription());
        ams.getActiveParticipants().get(0).getUserID().setOptionality(Optionality.M);
        this.validate(document, diagnostic, ams.getActiveParticipants().get(0), ams.getActiveParticipants().get(0).getDistinguisher(),
                ams.getIsDicomCompatibleAsBoolean());
        assertTrue(diagnostic.size() > 0);
    }

    @Test
    public void testValidateUserIDOpt() {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_01.xml");
        act.setUserID(new ElementDescription());
        act.getUserID().setOptionality(Optionality.C);
        this.validateUserIDOpt(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note", "Source/UserID has optionality 'C'", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_02.xml");
        this.validateUserIDOpt(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("warning", "Source/UserID has optionality 'C'", diagnostic));

    }

    @Test
    public void testValidateUserIDValue() {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_01.xml");
        act.setUserID(new ElementDescription());
        act.getUserID().setValue("wsaReplyToContents");
        this.validateUserIDValue(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@UserID must be 'wsaReplyToContents' if /Source/@UserID present", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_02.xml");
        this.validateUserIDValue(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@UserID must be 'wsaReplyToContents' if /Source/@UserID present", diagnostic));
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_03.xml");
        this.validateUserIDValue(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("error",
                "Source/@UserID must be 'wsaReplyToContents' if /Source/@UserID present", diagnostic));
    }

    @Test
    public void testValidateUserIDRegex() {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_01.xml");
        act.setUserID(new ElementDescription());
        act.getUserID().setRegex("^wsaReplyToContents.*$");
        this.validateUserIDRegex(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@UserID must have this form '^wsaReplyToContents.*$' if Source/@UserID present", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_02.xml");
        this.validateUserIDRegex(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@UserID must have this form '^wsaReplyToContents.*$' if Source/@UserID present", diagnostic));
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_03.xml");
        this.validateUserIDRegex(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("error",
                "Source/@UserID must have this form '^wsaReplyToContents.*$' if Source/@UserID present", diagnostic));
    }

    @Test
    public void testValidateAlternativeUserIDOpt() {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_04.xml");
        act.setAlternativeUserID(new ElementDescription());
        act.getAlternativeUserID().setOptionality(Optionality.M);
        this.validateAlternativeUserIDOpt(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note", "Source/@AlternativeUserID is mandatory", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_05.xml");
        this.validateAlternativeUserIDOpt(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("error", "Source/@AlternativeUserID is mandatory", diagnostic));
    }

    @Test
    public void testValidateAlternativeUserIDValue() {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_04.xml");
        act.setAlternativeUserID(new ElementDescription());
        act.getAlternativeUserID().setValue("BR-549");
        this.validateAlternativeUserIDValue(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@AlternativeUserID must be 'BR-549' if Source/@AlternativeUserID present", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_05.xml");
        this.validateAlternativeUserIDValue(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@AlternativeUserID must be 'BR-549' if Source/@AlternativeUserID present", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_06.xml");
        this.validateAlternativeUserIDValue(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("error",
                "Source/@AlternativeUserID must be 'BR-549' if Source/@AlternativeUserID present", diagnostic));
    }

    @Test
    public void testValidateAlternativeUserIDRegex() {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_04.xml");
        act.setAlternativeUserID(new ElementDescription());
        act.getAlternativeUserID().setRegex("^[a-zA-Z0-9\\-]*$");
        this.validateAlternativeUserIDRegex(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@AlternativeUserID must have this form '^[a-zA-Z0-9\\-]*$' if Source/@AlternativeUserID present", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_05.xml");
        this.validateAlternativeUserIDRegex(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@AlternativeUserID must have this form '^[a-zA-Z0-9\\-]*$' if Source/@AlternativeUserID present", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_06.xml");
        this.validateAlternativeUserIDRegex(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("error",
                "Source/@AlternativeUserID must have this form '^[a-zA-Z0-9\\-]*$' if Source/@AlternativeUserID present", diagnostic));

    }

    @Test
    public void testValidateUserNameOpt() {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_07.xml");
        act.setUserName(new ElementDescription());
        act.getUserName().setOptionality(Optionality.M);
        this.validateUserNameOpt(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note", "Source/@UserName is mandatory", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_08.xml");
        this.validateUserNameOpt(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("error", "Source/@UserName is mandatory", diagnostic));
    }

    @Test
    public void testValidateUserNameValue() {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_09.xml");
        act.setUserName(new ElementDescription());
        act.getUserName().setValue("moi");
        this.validateUserNameValue(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@UserName must be 'moi' if Source/@UserName present", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_10.xml");
        this.validateUserNameValue(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@UserName must be 'moi' if Source/@UserName present", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_11.xml");
        this.validateUserNameValue(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("error",
                "Source/@UserName must be 'moi' if Source/@UserName present", diagnostic));
    }

    @Test
    public void testValidateUserNameRegex() {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_09.xml");
        act.setUserName(new ElementDescription());
        act.getUserName().setRegex("^moi$");
        this.validateUserNameRegex(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@UserName must have this form '^moi$' if Source/@UserName present", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_10.xml");
        this.validateUserNameRegex(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@UserName must have this form '^moi$' if Source/@UserName present", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_11.xml");
        this.validateUserNameRegex(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("error",
                "Source/@UserName must have this form '^moi$' if Source/@UserName present", diagnostic));
    }

    @Test
    public void testValidateUserIsRequestorOpt() {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_12.xml");
        act.setUserIsRequestor(new ElementDescription());
        act.getUserIsRequestor().setOptionality(Optionality.M);
        this.validateUserIsRequestorOpt(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note", "Source/@UserIsRequestor is mandatory", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_13.xml");
        this.validateUserIsRequestorOpt(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("error", "Source/@UserIsRequestor is mandatory", diagnostic));
    }

    @Test
    public void testValidateUserIsRequestorValue() {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_12.xml");
        act.setUserIsRequestor(new ElementDescription());
        act.getUserIsRequestor().setValue("true");
        this.validateUserIsRequestorValue(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@UserIsRequestor must be 'true' if Source/@UserIsRequestor present", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_13.xml");
        this.validateUserIsRequestorValue(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@UserIsRequestor must be 'true' if Source/@UserIsRequestor present", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_14.xml");
        this.validateUserIsRequestorValue(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("error",
                "Source/@UserIsRequestor must be 'true' if Source/@UserIsRequestor present", diagnostic));
    }

    @Test
    public void testValidateUserIsRequestorRegex() {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_12.xml");
        act.setUserIsRequestor(new ElementDescription());
        act.getUserIsRequestor().setRegex("true");
        this.validateUserIsRequestorRegex(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@UserIsRequestor must have this form 'true' if Source/@UserIsRequestor present", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_13.xml");
        this.validateUserIsRequestorRegex(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@UserIsRequestor must have this form 'true' if Source/@UserIsRequestor present", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_14.xml");
        this.validateUserIsRequestorRegex(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("error",
                "Source/@UserIsRequestor must have this form 'true' if Source/@UserIsRequestor present", diagnostic));
    }

    @Test
    public void testValidateRoleIDCode() {

        // ------------ Mandatory RoleIDCode cases setup --------------------------------------------------------
        act.setRoleIDCode(new ElementDescription());
        act.getRoleIDCode().setValue("EV('110153', 'DCM', 'Source')");
        act.getRoleIDCode().setOptionality(Optionality.M);

        // Mandatory RoleIDCode: element is present case
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_roleidcode_nominal_dicom.xml");
        this.validateRoleIDCode(document, diagnostic, act, "RoleIDCode[@csd-code='110153']", true);
        assertTrue(this.containDescription("note", "Source/RoleIDCode is mandatory", diagnostic));

        // Mandatory RoleIDCode: element is missing case
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_roleidcode_missing_dicom.xml");
        this.validateRoleIDCode(document, diagnostic, act, "@UserIsRequestor='true'", true);
        assertTrue(this.containDescription("error", "Source/RoleIDCode is mandatory", diagnostic));

        // ------------ Optional RoleIDCode cases setup --------------------------------------------------------
        act.getRoleIDCode().setOptionality(Optionality.C);

        // Optional: RoleIDCode present case
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_roleidcode_nominal_rfc3881.xml");
        this.validateRoleIDCode(document, diagnostic, act, "RoleIDCode[@code='110153']", false);
        assertTrue(this.containDescription("note", "Source/RoleIDCode has optionality", diagnostic));

        // Optionnal missing case
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_roleidcode_missing_rfc3881.xml");
        this.validateRoleIDCode(document, diagnostic, act, "@UserIsRequestor='true'", false);
        assertTrue(this.containDescription("warning", "Source/RoleIDCode has optionality", diagnostic));

        // ------------ Forbidden RoleIDCode cases setup --------------------------------------------------------
        act.getRoleIDCode().setOptionality(Optionality.NA);

        // Forbidden not present case
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_roleidcode_missing_dicom.xml");
        this.validateRoleIDCode(document, diagnostic, act, "RoleIDCode[@csd-code='110153']", true);
        assertTrue(this.containDescription("note", "Source/RoleIDCode is prohibited", diagnostic));

        // Forbidden present case
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_roleidcode_nominal_dicom.xml");
        this.validateRoleIDCode(document, diagnostic, act, "RoleIDCode[@csd-code='110153']", true);
        assertTrue(this.containDescription("error", "Source/RoleIDCode is prohibited", diagnostic));
    }

    @Test
    public void testValidateMandatoryRoleIdCode() throws Exception {

        //Nominal case RFC3881 compliant
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_roleidcode_nominal_rfc3881.xml");
        this.validateMandatoryRoleIDCode("110153", "DCM", "Source", document, diagnostic, act, "RoleIDCode[@csd-code='110153']", false);
        assertTrue(this.containDescription("note", "Source/RoleIDCode must have ", diagnostic));
        assertTrue(this.containDescription("note", "Source/RoleIDCode should have ", diagnostic));

        //Nominal case Dicom compliant
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_roleidcode_nominal_dicom.xml");
        this.validateMandatoryRoleIDCode("110153", "DCM", "Source", document, diagnostic, act, "RoleIDCode[@csd-code='110153']", true);
        assertTrue(this.containDescription("note", "Source/RoleIDCode must have ", diagnostic));
        assertTrue(this.containDescription("note", "Source/RoleIDCode should have ", diagnostic));


        //Wrong values RFC3881 compliants
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_roleidcode_wrong_rfc3881.xml");
        this.validateMandatoryRoleIDCode("110153", "DCM", "Source", document, diagnostic, act, "@UserIsRequestor='true'", false);
        assertTrue(this.containDescription("error", "Source/RoleIDCode must have ", diagnostic));
        assertTrue(this.containDescription("warning", "Source/RoleIDCode should have ", diagnostic));

        //Wrong values Dicom compliant
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_roleidcode_wrong_dicom.xml");
        this.validateMandatoryRoleIDCode("110153", "DCM", "Source", document, diagnostic, act, "@UserIsRequestor='true'", true);
        assertTrue(this.containDescription("error", "Source/RoleIDCode must have ", diagnostic));
        assertTrue(this.containDescription("warning", "Source/RoleIDCode should have ", diagnostic));

    }

    @Test
    public void testValidateOptionalRoleIdCode() throws Exception {

        //Nominal case RFC3881 compliant
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_roleidcode_nominal_rfc3881.xml");
        this.validateOptionalRoleIDCode("110153", "DCM", "Source", document, diagnostic, act, "RoleIDCode[@csd-code='110153']", false);
        assertTrue(this.containDescription("note", "Source/RoleIDCode must have ", diagnostic));
        assertTrue(this.containDescription("note", "Source/RoleIDCode should have ", diagnostic));

        //Nominal case Dicom compliant
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_roleidcode_nominal_dicom.xml");
        this.validateOptionalRoleIDCode("110153", "DCM", "Source", document, diagnostic, act, "RoleIDCode[@csd-code='110153']", true);
        assertTrue(this.containDescription("note", "Source/RoleIDCode must have ", diagnostic));
        assertTrue(this.containDescription("note", "Source/RoleIDCode should have ", diagnostic));

        //Wrong values RFC3881 compliants
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_roleidcode_wrong_rfc3881.xml");
        this.validateOptionalRoleIDCode("110153", "DCM", "Source", document, diagnostic, act, "@UserIsRequestor='true'", false);
        assertTrue(this.containDescription("error", "Source/RoleIDCode must have ", diagnostic));
        assertTrue(this.containDescription("warning", "Source/RoleIDCode should have ", diagnostic));

        //Wrong values Dicom compliant
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_roleidcode_wrong_dicom.xml");
        this.validateOptionalRoleIDCode("110153", "DCM", "Source", document, diagnostic, act, "@UserIsRequestor='true'", true);
        assertTrue(this.containDescription("error", "Source/RoleIDCode must have ", diagnostic));
        assertTrue(this.containDescription("warning", "Source/RoleIDCode should have ", diagnostic));

    }

    @Test
    public void testValidateNetworkAccessPointTypeCodeOpt() {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_18.xml");
        act.setNetworkAccessPointTypeCode(new ElementDescription());
        act.getNetworkAccessPointTypeCode().setOptionality(Optionality.M);
        this.validateNetworkAccessPointTypeCodeOpt(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note", "Source/@NetworkAccessPointTypeCode is mandatory", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_19.xml");
        this.validateNetworkAccessPointTypeCodeOpt(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("error", "Source/@NetworkAccessPointTypeCode is mandatory", diagnostic));
    }

    @Test
    public void testValidateNetworkAccessPointTypeCodeValue() {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_18.xml");
        act.setNetworkAccessPointTypeCode(new ElementDescription());
        act.getNetworkAccessPointTypeCode().setValue("1");
        this.validateNetworkAccessPointTypeCodeValue(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@NetworkAccessPointTypeCode must be '1' if Source/@NetworkAccessPointTypeCode present",
                diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_19.xml");
        this.validateNetworkAccessPointTypeCodeValue(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@NetworkAccessPointTypeCode must be '1' if Source/@NetworkAccessPointTypeCode present",
                diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_20.xml");
        this.validateNetworkAccessPointTypeCodeValue(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("error",
                "Source/@NetworkAccessPointTypeCode must be '1' if Source/@NetworkAccessPointTypeCode present",
                diagnostic));
    }

    @Test
    public void testValidateNetworkAccessPointTypeCodeRegex() {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_18.xml");
        act.setNetworkAccessPointTypeCode(new ElementDescription());
        act.getNetworkAccessPointTypeCode().setRegex("1");
        this.validateNetworkAccessPointTypeCodeRegex(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@NetworkAccessPointTypeCode must have this form '1' " +
                        "if Source/@NetworkAccessPointTypeCode present",
                diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_19.xml");
        this.validateNetworkAccessPointTypeCodeRegex(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@NetworkAccessPointTypeCode must have this form '1' " +
                        "if Source/@NetworkAccessPointTypeCode present",
                diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_20.xml");
        this.validateNetworkAccessPointTypeCodeRegex(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("error",
                "Source/@NetworkAccessPointTypeCode must have this form '1' " +
                        "if Source/@NetworkAccessPointTypeCode present",
                diagnostic));
    }

    @Test
    public void testValidateNetworkAccessPointIDOpt() {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_21.xml");
        act.setNetworkAccessPointID(new ElementDescription());
        act.getNetworkAccessPointID().setOptionality(Optionality.M);
        this.validateNetworkAccessPointIDOpt(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note", "Source/@NetworkAccessPointID is mandatory", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_22.xml");
        this.validateNetworkAccessPointIDOpt(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("error", "Source/@NetworkAccessPointID is mandatory", diagnostic));

    }

    @Test
    public void testValidateNetworkAccessPointIDValue() {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_21.xml");
        act.setNetworkAccessPointID(new ElementDescription());
        act.getNetworkAccessPointID().setValue("server.source.org");
        this.validateNetworkAccessPointIDValue(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@NetworkAccessPointID must be 'server.source.org' if Source/@NetworkAccessPointID present",
                diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_22.xml");
        this.validateNetworkAccessPointIDValue(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@NetworkAccessPointID must be 'server.source.org' if Source/@NetworkAccessPointID present",
                diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_23.xml");
        this.validateNetworkAccessPointIDValue(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("error",
                "Source/@NetworkAccessPointID must be 'server.source.org' if Source/@NetworkAccessPointID present",
                diagnostic));
    }

    @Test
    public void testValidateNetworkAccessPointIDRegex() {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_21.xml");
        act.setNetworkAccessPointID(new ElementDescription());
        act.getNetworkAccessPointID().setRegex("^server.source.org$");
        this.validateNetworkAccessPointIDRegex(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@NetworkAccessPointID must have this form '^server.source.org$' " +
                        "if Source/@NetworkAccessPointID present",
                diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_22.xml");
        this.validateNetworkAccessPointIDRegex(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/@NetworkAccessPointID must have this form '^server.source.org$' " +
                        "if Source/@NetworkAccessPointID present",
                diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/act_23.xml");
        this.validateNetworkAccessPointIDRegex(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("error",
                "Source/@NetworkAccessPointID must have this form '^server.source.org$' " +
                        "if Source/@NetworkAccessPointID present",
                diagnostic));
    }

    @Test
    public void testValidateMediaIdentifierOpt() {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/med_01.xml");
        act.setMediaIdentifier(new ElementDescription());
        act.getMediaIdentifier().setOptionality(Optionality.M);
        this.validateMediaIdentifierOpt(document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note", "Source/MediaIdentifier is mandatory", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/med_02.xml");
        this.validateMediaIdentifierOpt(document, diagnostic, act, "@UserIsRequestor='true'");
        assertTrue(this.containDescription("error", "Source/MediaIdentifier is mandatory", diagnostic));
    }

    @Test
    public void testValidateMediaIdentifierValue() {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/med_03.xml");
        act.setMediaIdentifier(new ElementDescription());
        act.getMediaIdentifier().setValue("EV('1', '2', '3')");
        this.validateMediaIdentifierValue(document, diagnostic, act, distinguisher, false);
        assertTrue(this.containDescription("note",
                "Source/MediaIdentifier/MediaType@code must be '1' if Source/MediaIdentifier/MediaType present", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/med_04.xml");
        this.validateMediaIdentifierValue(document, diagnostic, act, "@UserIsRequestor='true'", false);
        assertTrue(this.containDescription("note",
                "Source/MediaIdentifier/MediaType@code must be '1' if Source/MediaIdentifier/MediaType present", diagnostic));
        document = FileReadWrite.readDoc("src/test/resources/samples/act/med_05.xml");
        this.validateMediaIdentifierValue(document, diagnostic, act, "@UserIsRequestor='true'", false);
        assertTrue(this.containDescription("note",
                "Source/MediaIdentifier/MediaType@code must be '1' if Source/MediaIdentifier/MediaType present", diagnostic));
    }

    @Test
    public void testValidateMediaIdentifierCodeValue() throws Exception {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/med_06.xml");
        this.validateMediaIdentifierCodeValue("1", document, diagnostic, act, distinguisher, false);
        assertTrue(this.containDescription("note",
                "Source/MediaIdentifier/MediaType@code must be '1' if Source/MediaIdentifier/MediaType present", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/med_07.xml");
        this.validateMediaIdentifierCodeValue("2", document, diagnostic, act, "@UserIsRequestor='true'", false);
        assertTrue(this.containDescription("error",
                "Source/MediaIdentifier/MediaType@code must be '2' if Source/MediaIdentifier/MediaType present", diagnostic));
    }

    @Test
    public void testValidateMediaIdentifierCodeSystemNameValue() throws Exception {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/med_08.xml");
        this.validateMediaIdentifierCodeSystemNameValue("2", document, diagnostic, act, distinguisher);
        assertTrue(this.containDescription("note",
                "Source/MediaIdentifier/MediaType@codeSystemName must be '2' if Source/MediaIdentifier/MediaType present", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/med_09.xml");
        this.validateMediaIdentifierCodeSystemNameValue("22", document, diagnostic, act, "@UserIsRequestor='true'");
        assertTrue(this.containDescription("error",
                "Source/MediaIdentifier/MediaType@codeSystemName must be '22' if Source/MediaIdentifier/MediaType present", diagnostic));
    }

    @Test
    public void testValidateMediaIdentifierDisplayNameValue() throws Exception {
        document = FileReadWrite.readDoc("src/test/resources/samples/act/med_10.xml");
        this.validateMediaIdentifierDisplayNameValue("3", document, diagnostic, act, distinguisher, false);
        assertTrue(this.containDescription("note",
                "Source/MediaIdentifier/MediaType@displayName must be '3' if Source/MediaIdentifier/MediaType present", diagnostic));
        diagnostic = new ArrayList<Notification>();
        document = FileReadWrite.readDoc("src/test/resources/samples/act/med_11.xml");
        this.validateMediaIdentifierDisplayNameValue("33", document, diagnostic, act, "@UserIsRequestor='true'", false);
        assertTrue(this.containDescription("error",
                "Source/MediaIdentifier/MediaType@displayName must be '33' if Source/MediaIdentifier/MediaType present", diagnostic));
    }

    private boolean containDescription(String kind, String desc, List<Notification> diagnostic) {
        for (Notification notification : diagnostic) {
            if (notification.getClass().getSimpleName().equalsIgnoreCase(kind.toLowerCase())
                    && notification.getDescription().contains(desc)) {
                return true;
            }
        }
        return false;
    }

}
