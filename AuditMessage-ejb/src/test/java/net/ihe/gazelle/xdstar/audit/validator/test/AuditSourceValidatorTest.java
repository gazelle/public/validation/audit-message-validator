/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.xdstar.audit.validator.test;

import net.ihe.gazelle.audit.message.desc.AuditMessageSpecification;
import net.ihe.gazelle.audit.message.desc.AuditSourceDescription;
import net.ihe.gazelle.audit.message.desc.ElementDescription;
import net.ihe.gazelle.audit.message.desc.Optionality;
import net.ihe.gazelle.audit.message.validator.AuditSourceValidator;
import net.ihe.gazelle.validation.Notification;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class AuditSourceValidatorTest extends AuditSourceValidator {

	List<Notification> diagnostic;

	String document;

	AuditMessageSpecification ams;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		diagnostic = new ArrayList<Notification>();
		ams = new AuditMessageSpecification();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testValidate() {
		ams.setAuditSource(new AuditSourceDescription());
		ams.getAuditSource().setAuditSourceID(new ElementDescription());
		ams.getAuditSource().getAuditSourceID().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_1.xml");
		this.validate(document, ams, diagnostic);
		assertTrue(diagnostic.size()>0);
	}

	@Test
	public void testValidateAuditSourceIDOpt() {
		ams.setAuditSource(new AuditSourceDescription());
		ams.getAuditSource().setAuditSourceID(new ElementDescription());
		ams.getAuditSource().getAuditSourceID().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_1.xml");
		this.validateAuditSourceIDOpt(document, ams, diagnostic);
//		for (Notification notification : diagnostic) {
//		System.out.println(notification.getClass().getSimpleName() + "::" +  notification.getDescription());
//	}
		assertTrue(this.containDescription("note", "AuditSource/@AuditSourceID is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_2.xml");
		this.validateAuditSourceIDOpt(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "AuditSource/@AuditSourceID is mandatory", diagnostic));
	}

	@Test
	public void testValidateAuditSourceIDValue() {
		ams.setAuditSource(new AuditSourceDescription());
		ams.getAuditSource().setAuditSourceID(new ElementDescription());
		ams.getAuditSource().getAuditSourceID().setValue("toto");
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_3.xml");
		this.validateAuditSourceIDValue(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "AuditSource/@AuditSourceID must be 'toto' if AuditSource/@AuditSourceID present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_4.xml");
		this.validateAuditSourceIDValue(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "AuditSource/@AuditSourceID must be 'toto' if AuditSource/@AuditSourceID present", diagnostic));
	}

	@Test
	public void testValidateAuditSourceIDRegex() {
		ams.setAuditSource(new AuditSourceDescription());
		ams.getAuditSource().setAuditSourceID(new ElementDescription());
		ams.getAuditSource().getAuditSourceID().setRegex("[to]+");
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_3.xml");
		this.validateAuditSourceIDRegex(document, ams, diagnostic);
		assertTrue(this.containDescription("note", 
				"AuditSource/@AuditSourceID must have this form : '[to]+' if AuditSource/@AuditSourceID present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_4.xml");
		this.validateAuditSourceIDRegex(document, ams, diagnostic);
		assertTrue(this.containDescription("error", 
				"AuditSource/@AuditSourceID must have this form : '[to]+' if AuditSource/@AuditSourceID present", diagnostic));

	}

	@Test
	public void testValidateAuditEnterpriseSiteIDOpt() {
		ams.setAuditSource(new AuditSourceDescription());
		ams.getAuditSource().setAuditEnterpriseSiteID(new ElementDescription());
		ams.getAuditSource().getAuditEnterpriseSiteID().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_5.xml");
		this.validateAuditEnterpriseSiteIDOpt(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "AuditSource/@AuditEnterpriseSiteID is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_6.xml");
		this.validateAuditEnterpriseSiteIDOpt(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "AuditSource/@AuditEnterpriseSiteID is mandatory", diagnostic));
	}

	@Test
	public void testValidateAuditEnterpriseSiteIDValue() {
		ams.setAuditSource(new AuditSourceDescription());
		ams.getAuditSource().setAuditEnterpriseSiteID(new ElementDescription());
		ams.getAuditSource().getAuditEnterpriseSiteID().setValue("WC");
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_5.xml");
		this.validateAuditEnterpriseSiteIDValue(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "AuditSource/@AuditEnterpriseSiteID must be 'WC' if AuditSource/@AuditEnterpriseSiteID present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_6.xml");
		this.validateAuditEnterpriseSiteIDValue(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "AuditSource/@AuditEnterpriseSiteID must be 'WC' if AuditSource/@AuditEnterpriseSiteID present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_7.xml");
		this.validateAuditEnterpriseSiteIDValue(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "AuditSource/@AuditEnterpriseSiteID must be 'WC' if AuditSource/@AuditEnterpriseSiteID present", diagnostic));
	}

	@Test
	public void testValidateAuditEnterpriseSiteIDRegex() {
		ams.setAuditSource(new AuditSourceDescription());
		ams.getAuditSource().setAuditEnterpriseSiteID(new ElementDescription());
		ams.getAuditSource().getAuditEnterpriseSiteID().setRegex("^WC$");
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_5.xml");
		this.validateAuditEnterpriseSiteIDRegex(document, ams, diagnostic);
		assertTrue(this.containDescription("note", 
				"AuditSource/@AuditEnterpriseSiteID must have this form : '^WC$' if AuditSource/@AuditEnterpriseSiteID present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_7.xml");
		this.validateAuditEnterpriseSiteIDRegex(document, ams, diagnostic);
		assertTrue(this.containDescription("error", 
				"AuditSource/@AuditEnterpriseSiteID must have this form : '^WC$' if AuditSource/@AuditEnterpriseSiteID present", diagnostic));
	}

	@Test
	public void testValidateAuditSourceTypeCodeOpt() {
		ams.setAuditSource(new AuditSourceDescription());
		ams.getAuditSource().setAuditSourceTypeCode(new ElementDescription());
		ams.getAuditSource().getAuditSourceTypeCode().setOptionality(Optionality.M);
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_8.xml");
		this.validateAuditSourceTypeCodeOpt(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "AuditSource/AuditSourceTypeCode is mandatory", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_9.xml");
		this.validateAuditSourceTypeCodeOpt(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "AuditSource/AuditSourceTypeCode is mandatory", diagnostic));
	}

	@Test
	public void testValidateAuditSourceTypeCodeValue() {
		ams.setAuditSource(new AuditSourceDescription());
		ams.getAuditSource().setAuditSourceTypeCode(new ElementDescription());
		ams.getAuditSource().getAuditSourceTypeCode().setValue("EV('T','U','V')");
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_8.xml");
		this.validateAuditSourceTypeCodeValue(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "AuditSource/AuditSourceTypeCode@code must be 'T' if AuditSource/AuditSourceTypeCode present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_9.xml");
		this.validateAuditSourceTypeCodeValue(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "AuditSource/AuditSourceTypeCode@code must be 'T' if AuditSource/AuditSourceTypeCode present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_10.xml");
		this.validateAuditSourceTypeCodeValue(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "AuditSource/AuditSourceTypeCode@code must be 'T' if AuditSource/AuditSourceTypeCode present", diagnostic));
	}

	@Test
	public void testValidateAuditSourceTypeCodeCodeValue() throws Exception {
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_8.xml");
		this.validateAuditSourceTypeCodeCodeValue("T", document, ams, diagnostic);
		assertTrue(this.containDescription("note", "AuditSource/AuditSourceTypeCode@code must be 'T' if AuditSource/AuditSourceTypeCode present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_9.xml");
		this.validateAuditSourceTypeCodeCodeValue("T", document, ams, diagnostic);
		assertTrue(this.containDescription("note", "AuditSource/AuditSourceTypeCode@code must be 'T' if AuditSource/AuditSourceTypeCode present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_10.xml");
		this.validateAuditSourceTypeCodeCodeValue("T", document, ams, diagnostic);
		assertTrue(this.containDescription("error", "AuditSource/AuditSourceTypeCode@code must be 'T' if AuditSource/AuditSourceTypeCode present", diagnostic));

	}

	@Test
	public void testValidateAuditSourceTypeCodeCodeSystemNameValue() throws Exception {
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_8.xml");
		
		this.validateAuditSourceTypeCodeCodeSystemNameValue("U", document, ams, diagnostic);
//		for (Notification notification : diagnostic) {
//			System.out.println(notification.getClass().getSimpleName() + "::" +  notification.getDescription());
//		}	
		assertTrue(this.containDescription("note", "AuditSource/AuditSourceTypeCode@codeSystemName must be 'U' if AuditSource/AuditSourceTypeCode present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_9.xml");
		this.validateAuditSourceTypeCodeCodeSystemNameValue("U", document, ams, diagnostic);
		assertTrue(this.containDescription("note", "AuditSource/AuditSourceTypeCode@codeSystemName must be 'U' if AuditSource/AuditSourceTypeCode present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_10.xml");
		this.validateAuditSourceTypeCodeCodeSystemNameValue("U", document, ams, diagnostic);
		assertTrue(this.containDescription("error", "AuditSource/AuditSourceTypeCode@codeSystemName must be 'U' if AuditSource/AuditSourceTypeCode present", diagnostic));

	}

	@Test
	public void testValidateAuditSourceTypeCodeDisplayNameValue() throws Exception {
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_8.xml");
		
		this.validateAuditSourceTypeCodeDisplayNameValue("V", document, ams, diagnostic);
//		for (Notification notification : diagnostic) {
//			System.out.println(notification.getClass().getSimpleName() + "::" +  notification.getDescription());
//		}	
        assertTrue(this.containDescription("note",
                "AuditSource/AuditSourceTypeCode@displayName should be 'V' if AuditSource/AuditSourceTypeCode present", diagnostic));
        diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_9.xml");
		this.validateAuditSourceTypeCodeDisplayNameValue("V", document, ams, diagnostic);
        assertTrue(this.containDescription("note",
                "AuditSource/AuditSourceTypeCode@displayName should be 'V' if AuditSource/AuditSourceTypeCode present", diagnostic));
        diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_10.xml");
		this.validateAuditSourceTypeCodeDisplayNameValue("V", document, ams, diagnostic);
        assertTrue(this.containDescription("warning",
                "AuditSource/AuditSourceTypeCode@displayName should be 'V' if AuditSource/AuditSourceTypeCode present", diagnostic));
    }
	
	@Test
	public void testValidateAuditSourceTypeCodeValue2() {
		ams.setIsDicomCompatible(true);
		ams.setAuditSource(new AuditSourceDescription());
		ams.getAuditSource().setAuditSourceTypeCode(new ElementDescription());
		ams.getAuditSource().getAuditSourceTypeCode().setValue("AA");
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_11.xml");
		this.validateAuditSourceTypeCodeValue(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "AuditSource/AuditSourceTypeCode must be 'AA' if AuditSource/AuditSourceTypeCode present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_12.xml");
		ams.getAuditSource().getAuditSourceTypeCode().setValue("BB");
		this.validateAuditSourceTypeCodeValue(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "AuditSource/AuditSourceTypeCode must be 'BB' if AuditSource/AuditSourceTypeCode present", diagnostic));
	}
	
	@Test
	public void testValidateAuditSourceCodeValueValue() {
		ams.setAuditSource(new AuditSourceDescription());
		ams.getAuditSource().setAuditSourceCodeValue(new ElementDescription());
		ams.getAuditSource().getAuditSourceCodeValue().setValue("EV('T','U','V')");
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_13.xml");
		this.validateAuditSourceCodeValueValue(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "AuditSource/@code must be 'T' if AuditSource present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_14.xml");
		ams.getAuditSource().getAuditSourceCodeValue().setValue("EV('E','U','V')");
		this.validateAuditSourceCodeValueValue(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "AuditSource/@code must be 'E' if AuditSource present", diagnostic));
	}
	
	@Test
	public void testValidateAuditSourceCodeValueCodeValue() throws Exception {
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_15.xml");
		this.validateAuditSourceCodeValueCodeValue("T", document, ams, diagnostic);
		assertTrue(this.containDescription("note", "AuditSource/@code must be 'T' if AuditSource present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_16.xml");
		this.validateAuditSourceCodeValueCodeValue("T", document, ams, diagnostic);
		assertTrue(this.containDescription("note", "AuditSource/@code must be 'T' if AuditSource present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_17.xml");
		this.validateAuditSourceCodeValueCodeValue("T", document, ams, diagnostic);
		assertTrue(this.containDescription("error", "AuditSource/@code must be 'T' if AuditSource present", diagnostic));

	}

	@Test
	public void testValidateAuditSourceCodeValueCodeSystemNameValue() throws Exception {
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_18.xml");
		
		this.validateAuditSourceCodeValueCodeSystemNameValue("U", document, ams, diagnostic);
		assertTrue(this.containDescription("note", "AuditSource/@codeSystemName must be 'U' if AuditSource present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_19.xml");
		this.validateAuditSourceCodeValueCodeSystemNameValue("U", document, ams, diagnostic);
		assertTrue(this.containDescription("note", "AuditSource/@codeSystemName must be 'U' if AuditSource present", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_20.xml");
		this.validateAuditSourceCodeValueCodeSystemNameValue("U", document, ams, diagnostic);
		assertTrue(this.containDescription("error", "AuditSource/@codeSystemName must be 'U' if AuditSource present", diagnostic));

	}

	@Test
	public void testValidateAuditSourceCodeValueDisplayNameValue() throws Exception {
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_21.xml");
		
		this.validateAuditSourceCodeValueDisplayNameValue("V", document, ams, diagnostic);
        assertTrue(this.containDescription("note", "AuditSource/@originalText should be 'V' if AuditSource present", diagnostic));
        diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_22.xml");
		this.validateAuditSourceCodeValueDisplayNameValue("V", document, ams, diagnostic);
        assertTrue(this.containDescription("note", "AuditSource/@originalText should be 'V' if AuditSource present", diagnostic));
        diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/as/as_23.xml");
		this.validateAuditSourceCodeValueDisplayNameValue("V", document, ams, diagnostic);
        assertTrue(this.containDescription("warning", "AuditSource/@originalText should be 'V' if AuditSource present", diagnostic));
    }
	
	private boolean containDescription(String kind, String desc, List<Notification> diagnostic){
		for (Notification notification : diagnostic) {
			if (notification.getClass().getSimpleName().equalsIgnoreCase(kind.toLowerCase())
					&& notification.getDescription().equals(desc)){
				return true;
			}
		}
		return false;
	}

}
