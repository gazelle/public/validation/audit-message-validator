/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.xdstar.audit.validator.test;

import net.ihe.gazelle.audit.message.desc.ActiveParticipantDescription;
import net.ihe.gazelle.audit.message.desc.AuditMessageSpecification;
import net.ihe.gazelle.audit.message.desc.AuditSourceDescription;
import net.ihe.gazelle.audit.message.desc.ConstraintSpecification;
import net.ihe.gazelle.audit.message.desc.EventIdentificationDesc;
import net.ihe.gazelle.audit.message.desc.ParticipantObjectIdentificationDesc;
import net.ihe.gazelle.audit.message.validator.AuditMessageValidator;
import net.ihe.gazelle.validation.Notification;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class AuditMessageValidatorTest extends AuditMessageValidator{
	
	List<Notification> diagnostic;
	
	String document;
	
	AuditMessageSpecification ams;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		diagnostic = new ArrayList<Notification>();
		ams = new AuditMessageSpecification();
		ams.setIsDicomCompatible(false);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testValidate() {
		ams.setEvent(new EventIdentificationDesc());
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_01.xml");
		this.validateEventCardinality(document, ams, diagnostic);
		assertTrue(diagnostic.size()>0);
	}

	@Test
	public void testValidateEventCardinality() {
		ams.setEvent(new EventIdentificationDesc());
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_01.xml");
		this.validateEventCardinality(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "The cardinality of Event is 1..1", diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_011.xml");
		this.validateEventCardinality(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "The cardinality of Event is 1..1", diagnostic));
	}

	
	@Test
	public void testValidateAuditSourceCardinalityMax1() {
		ams.setAuditSource(new AuditSourceDescription());
		ams.getAuditSource().setMin("1");
		ams.getAuditSource().setMax("1");
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_08.xml");
		this.validateAuditSourceCardinality(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "The number of AuditSource SHALL be less than or equal to 1", diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_09.xml");
		this.validateAuditSourceCardinality(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "The number of AuditSource SHALL be less than or equal to 1", diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_10.xml");
		this.validateAuditSourceCardinality(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "The number of AuditSource SHALL be less than or equal to 1", diagnostic));
	}
	
	@Test
	public void testValidateAuditSourceCardinalityMax2() {
		ams.setAuditSource(new AuditSourceDescription());
		ams.getAuditSource().setMin("1");
		ams.getAuditSource().setMax("3");
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_11.xml");
		this.validateAuditSourceCardinality(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "The number of AuditSource SHALL be less than or equal to 3", diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_12.xml");
		this.validateAuditSourceCardinality(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "The number of AuditSource SHALL be less than or equal to 3", diagnostic));
	}
	
	@Test
	public void testValidateAuditSourceCardinalityMax3() {
		ams.setAuditSource(new AuditSourceDescription());
		ams.getAuditSource().setMin("0");
		ams.getAuditSource().setMax("*");
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_12.xml");
		this.validateAuditSourceCardinality(document, ams, diagnostic);
		assertTrue(diagnostic.size()==0);
	}
	
	@Test
	public void testValidateAuditSourceCardinalityMin1() {
		ams.setAuditSource(new AuditSourceDescription());
		ams.getAuditSource().setMin("0");
		ams.getAuditSource().setMax("*");
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_12.xml");
		this.validateAuditSourceCardinality(document, ams, diagnostic);
		assertTrue(diagnostic.size()==0);
	}
	
	@Test
	public void testValidateAuditSourceCardinalityMin2() {
		ams.setAuditSource(new AuditSourceDescription());
		ams.getAuditSource().setMin("2");
		ams.getAuditSource().setMax("*");
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_13.xml");
		this.validateAuditSourceCardinality(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "The number of AuditSource SHALL be greater than or equal to 2", diagnostic));
	}
	
	@Test
	public void testValidateAuditSourceCardinalityMin3() {
		ams.setAuditSource(new AuditSourceDescription());
		ams.getAuditSource().setMin("2");
		ams.getAuditSource().setMax("*");
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_14.xml");
		this.validateAuditSourceCardinality(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "The number of AuditSource SHALL be greater than or equal to 2", diagnostic));
	}
	
	
	@Test
	public void testValidateActiveParticipantsCardinalityMax1() {
		ams.getActiveParticipants().add(new ActiveParticipantDescription());
		ams.getActiveParticipants().get(0).setMin("1");
		ams.getActiveParticipants().get(0).setMax("1");
		ams.getActiveParticipants().get(0).setName("Source");
		ams.getActiveParticipants().get(0).setDistinguisher("RoleIDCode[@code='110153']");
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_01.xml");
		this.validateActiveParticipantsCardinality(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "The number of Source SHALL be less than or equal to 1", diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_02.xml");
		this.validateActiveParticipantsCardinality(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "The number of Source SHALL be less than or equal to 1", diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_03.xml");
		this.validateActiveParticipantsCardinality(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "The number of Source SHALL be less than or equal to 1", diagnostic));
	}
	
	@Test
	public void testValidateActiveParticipantsCardinalityMax2() {
		ams.getActiveParticipants().add(new ActiveParticipantDescription());
		ams.getActiveParticipants().get(0).setMin("0");
		ams.getActiveParticipants().get(0).setMax("3");
		ams.getActiveParticipants().get(0).setName("Source");
		ams.getActiveParticipants().get(0).setDistinguisher("RoleIDCode[@code='110153']");
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_04.xml");
		this.validateActiveParticipantsCardinality(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "The number of Source SHALL be less than or equal to 3", diagnostic));
		
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_05.xml");
		this.validateActiveParticipantsCardinality(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "The number of Source SHALL be less than or equal to 3", diagnostic));
	}
	
	@Test
	public void testValidateActiveParticipantsCardinalityMax3() {
		ams.getActiveParticipants().add(new ActiveParticipantDescription());
		ams.getActiveParticipants().get(0).setMin("0");
		ams.getActiveParticipants().get(0).setMax("*");
		ams.getActiveParticipants().get(0).setName("Source");
		ams.getActiveParticipants().get(0).setDistinguisher("RoleIDCode[@code='110153']");
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_05.xml");
		this.validateActiveParticipantsCardinality(document, ams, diagnostic);
		assertTrue(diagnostic.size()==0);
	}
	
	@Test
	public void testValidateActiveParticipantsCardinalityMin1() {
		ams.getActiveParticipants().add(new ActiveParticipantDescription());
		ams.getActiveParticipants().get(0).setMin("0");
		ams.getActiveParticipants().get(0).setMax("*");
		ams.getActiveParticipants().get(0).setName("Source");
		ams.getActiveParticipants().get(0).setDistinguisher("RoleIDCode[@code='110153']");
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_05.xml");
		this.validateActiveParticipantsCardinality(document, ams, diagnostic);
		assertTrue(diagnostic.size()==0);
	}
	
	@Test
	public void testValidateActiveParticipantsCardinalityMin2() {
		ams.getActiveParticipants().add(new ActiveParticipantDescription());
		ams.getActiveParticipants().get(0).setMin("2");
		ams.getActiveParticipants().get(0).setMax("*");
		ams.getActiveParticipants().get(0).setName("Source");
		ams.getActiveParticipants().get(0).setDistinguisher("RoleIDCode[@code='110153']");
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_06.xml");
		this.validateActiveParticipantsCardinality(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "The number of Source SHALL be greater than or equal to 2", diagnostic));
	}
	
	@Test
	public void testValidateActiveParticipantsCardinalityMin3() {
		ams.getActiveParticipants().add(new ActiveParticipantDescription());
		ams.getActiveParticipants().get(0).setMin("2");
		ams.getActiveParticipants().get(0).setMax("*");
		ams.getActiveParticipants().get(0).setName("Source");
		ams.getActiveParticipants().get(0).setDistinguisher("RoleIDCode[@code='110153']");
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_07.xml");
		this.validateActiveParticipantsCardinality(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "The number of Source SHALL be greater than or equal to 2", diagnostic));
	}

	@Test
	public void testValidateParticipantObjectIdentificationCardinality() {
		ams.getParticipantObjectIdentifications().add(new ParticipantObjectIdentificationDesc());
		ams.getParticipantObjectIdentifications().get(0).setMin("2");
		ams.getParticipantObjectIdentifications().get(0).setMax("*");
		ams.getParticipantObjectIdentifications().get(0).setName("Query");
		ams.getParticipantObjectIdentifications().get(0).setDistinguisher("@ParticipantObjectTypeCode='2'");
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_15.xml");
		this.validateParticipantObjectIdentificationCardinality(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "The number of Query SHALL be greater than or equal to 2", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_16.xml");
		this.validateParticipantObjectIdentificationCardinality(document, ams, diagnostic);
		assertTrue(this.containDescription("error", "The number of Query SHALL be greater than or equal to 2", diagnostic));
	}
	
	@Test
	public void testValidateAllowedActiveParticipants() {
		ams.getActiveParticipants().add(new ActiveParticipantDescription());
		ams.getActiveParticipants().get(0).setName("Source");
		ams.getActiveParticipants().get(0).setDistinguisher("RoleIDCode[@code='110153']");
		ams.getActiveParticipants().add(new ActiveParticipantDescription());
		ams.getActiveParticipants().get(1).setName("Destination");
		ams.getActiveParticipants().get(1).setDistinguisher("RoleIDCode[@code='110152']");
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_17.xml");
		this.validateAllowedActiveParticipants(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "Allowed ActiveParticipants are Source, Destination", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_18.xml");
		this.validateAllowedActiveParticipants(document, ams, diagnostic);
		assertTrue(this.containDescription("warning", "Allowed ActiveParticipants are Source, Destination", diagnostic));
	}
	
	@Test
	public void testValidateAllowedParticipantObjectIdentifications() {
		ams.getParticipantObjectIdentifications().add(new ParticipantObjectIdentificationDesc());
		ams.getParticipantObjectIdentifications().get(0).setName("Query");
		ams.getParticipantObjectIdentifications().get(0).setDistinguisher("@ParticipantObjectTypeCode='2' and @ParticipantObjectTypeCodeRole='24'");
		ams.getParticipantObjectIdentifications().add(new ParticipantObjectIdentificationDesc());
		ams.getParticipantObjectIdentifications().get(1).setName("Patient");
		ams.getParticipantObjectIdentifications().get(1).setDistinguisher("@ParticipantObjectTypeCode='2' and @ParticipantObjectTypeCodeRole='24'");
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_19.xml");
		this.validateAllowedParticipantObjectIdentifications(document, ams, diagnostic);
		assertTrue(this.containDescription("note", "Allowed ParticipantObjectIdentifications are Query, Patient", diagnostic));
		diagnostic = new ArrayList<Notification>();
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_20.xml");
		this.validateAllowedParticipantObjectIdentifications(document, ams, diagnostic);
		assertTrue(this.containDescription("warning", "Allowed ParticipantObjectIdentifications are Query, Patient", diagnostic));
	}
	
	@Test
	public void testValidateCostraintSpecification() {
		ConstraintSpecification cons = new ConstraintSpecification();
		cons.setXpath("/AuditMessage/EventIdentification");
		cons.setDescription("test desc");
		cons.setKind("error");
		document = FileReadWrite.readDoc("src/test/resources/samples/am/am_20.xml");
		this.validateCostraintSpecification(document, cons, diagnostic);
		assertTrue(this.containDescription("note", "test desc", diagnostic));
	}

	private boolean containDescription(String kind, String desc, List<Notification> diagnostic){
		for (Notification notification : diagnostic) {
			if (notification.getClass().getSimpleName().equalsIgnoreCase(kind.toLowerCase())
					&& notification.getDescription().equals(desc)){
				return true;
			}
		}
		return false;
	}

}
