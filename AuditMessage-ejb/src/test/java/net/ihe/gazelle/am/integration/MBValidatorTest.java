/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.am.integration;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import net.ihe.gazelle.mb.validator.client.MBValidator;

import org.jboss.seam.util.Base64;
import org.junit.Test;

/**
 * Class Description : Integration testing related to webservices used on the validation of CDA Documents and XD* metadatas.
 * All model based webservice validator has the same JAVA interface, which is @net.ihe.gazelle.simulator.ws.ModelBasedValidationRemote. 
 * It contains 3 methods : 
 * <ul>
 * <li>validateDocument(String document, String validator)</li>
 * <li>validateBase64Document(String base64Document, String validator)</li>
 * <li>List<String> getListOfValidators(String descriminator)</li>
 * </ul>
 * The aim of this class is to test the reachability of each method using the webservice of XDS and CDA 
 * 
 * @author          abderrazek boufahja / Kereval / IHE-Europe
 * @see	> 			abderrazek.boufahja@ihe-europe.com  -  http://www.ihe-europe.org
 * @version			1.0 - 15 jul 2013
 *
 */
public class MBValidatorTest {
	
	private static final String AM_ENDPOINT = "http://131.254.209.20:8080/XDStarClient-XDStarClient-ejb/AuditMessageValidatorWS?wsdl";
	
	/**
	 * test the possibility to retrieve all availables validators' name from the CDA endpoint
	 * 
	 */
	public void testAMValidatorListAvailableValidators() {
		MBValidator mbValidator = new MBValidator(AM_ENDPOINT);
		List<String> liss = mbValidator.listAvailableValidators("IHE");
		assertTrue(liss.size()>0);
	}
	
//	@Test
	public void testAMValidatorTestValidation01OK() throws IOException {
		String document = readDoc("src/test/resources/am_integration/01_OK.xml");
		MBValidator mbValidator = new MBValidator(AM_ENDPOINT);
		String liss = mbValidator.validate(document, "IHE - ITI-55 XCPD Initiating Gateway audit message", false);
		assertTrue(liss != null && liss.length()>0);
		assertTrue(liss.contains("<Result>PASSED</Result>"));
	}
	
//	@Test
	public void testAMValidatorTestValidation01KO() throws IOException {
		String document = readDoc("src/test/resources/am_integration/01_KO.xml");
		MBValidator mbValidator = new MBValidator(AM_ENDPOINT);
		String liss = mbValidator.validate(document, "IHE - ITI-55 XCPD Initiating Gateway audit message", false);
		assertTrue(liss != null && liss.length()>0);
		assertTrue(liss.contains("<Result>FAILED</Result>"));
	}
	
//	@Test
	public void testAMValidatorTestValidation02OK() throws IOException {
		String document = readDoc("src/test/resources/am_integration/02_OK.xml");
		MBValidator mbValidator = new MBValidator(AM_ENDPOINT);
		String liss = mbValidator.validate(document, "IHE - ITI-55 XCPD Responding Gateway audit message", false);
		assertTrue(liss != null && liss.length()>0);
		assertTrue(liss.contains("<Result>PASSED</Result>"));
	}
	
//	@Test
	public void testAMValidatorTestValidation02KO() throws IOException {
		String document = readDoc("src/test/resources/am_integration/02_KO.xml");
		MBValidator mbValidator = new MBValidator(AM_ENDPOINT);
		String liss = mbValidator.validate(document, "IHE - ITI-55 XCPD Responding Gateway audit message", false);
		assertTrue(liss != null && liss.length()>0);
		assertTrue(liss.contains("<Result>FAILED</Result>"));
	}
	
//	@Test
	public void testAMValidatorTestValidation04KO() throws IOException {
		String document = readDoc("src/test/resources/am_integration/04_KO.xml");
		MBValidator mbValidator = new MBValidator(AM_ENDPOINT);
		String liss = mbValidator.validate(document, "IHE - ITI-56 XCPD Initiating Gateway audit message", false);
		assertTrue(liss != null && liss.length()>0);
		assertTrue(liss.contains("<Result>FAILED</Result>"));
	}
	
//	@Test
	public void testAMValidatorTestValidation05OK() throws IOException {
		String document = readDoc("src/test/resources/am_integration/05_OK.xml");
		MBValidator mbValidator = new MBValidator(AM_ENDPOINT);
		String liss = mbValidator.validate(document, "IHE - ITI-61 Document Registry audit message", false);
		assertTrue(liss != null && liss.length()>0);
		assertTrue(liss.contains("<Result>PASSED</Result>"));
	}
	
//	@Test
	public void testAMValidatorTestValidation05KO() throws IOException {
		String document = readDoc("src/test/resources/am_integration/05_KO.xml");
		MBValidator mbValidator = new MBValidator(AM_ENDPOINT);
		String liss = mbValidator.validate(document, "IHE - ITI-61 Document Registry audit message", false);
		assertTrue(liss != null && liss.length()>0);
		assertTrue(liss.contains("<Result>FAILED</Result>"));
	}
	
//	@Test
	public void testCDAValidatorTestValidationBase64() throws IOException {
		String document = readDoc("src/test/resources/am_integration/01_OK.xml");
		MBValidator mbValidator = new MBValidator(AM_ENDPOINT);
		String liss = mbValidator.validate(Base64.encodeBytes(document.getBytes()), "IHE - ITI-55 XCPD Initiating Gateway audit message", true);
		assertTrue(liss != null && liss.length()>0);
		assertTrue(liss.contains("<Result>PASSED</Result>"));
	}
	
	private static String readDoc(String name) throws IOException {
		BufferedReader scanner = new BufferedReader(new InputStreamReader(new FileInputStream(name)));
		StringBuilder res = new StringBuilder();
		try {
			String line = scanner.readLine();
			while (line != null) {
				res.append(line + "\n");
				line = scanner.readLine();
			}
		} finally {
			scanner.close();
		}
		return res.toString();
	}

}
