/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.audit.message.utils;

import net.ihe.gazelle.audit.message.desc.AuditMessageSpecification;
import net.ihe.gazelle.audit.message.menu.AuditMessagePages;
import net.ihe.gazelle.common.LinkDataProvider;
import org.kohsuke.MetaInfServices;

import java.util.ArrayList;
import java.util.List;

@MetaInfServices(LinkDataProvider.class)
public class AuditMessageLinkProvider implements LinkDataProvider {

	public static final AuditMessageLinkProvider instance() {
		return new AuditMessageLinkProvider();
	}

	private static List<Class<?>> supportedClasses;

	static {
		supportedClasses = new ArrayList<Class<?>>();
		supportedClasses.add(AuditMessageSpecification.class);
	}

	@Override
	public List<Class<?>> getSupportedClasses() {
		return supportedClasses;
	}

	@Override
	public String getLabel(Object o, boolean detailed) {
		StringBuilder label = new StringBuilder();
		if (o instanceof AuditMessageSpecification) {
			AuditMessageSpecification msg = (AuditMessageSpecification) o;
			label.append(msg.getOid());
		}
		return label.toString();
	}

	@Override
	public String getLink(Object o) {
		StringBuilder url = new StringBuilder();
		if (o instanceof AuditMessageSpecification) {
			AuditMessageSpecification msg = (AuditMessageSpecification) o;
            url.append(AuditMessagePages.AM_VIEW.getMenuLink());
            url.append("?id=");
            url.append(msg.getId());
		}
		return url.toString();
	}

	@Override
	public String getTooltip(Object o) {
		StringBuilder tip = new StringBuilder();
		if (o instanceof AuditMessageSpecification) {
			AuditMessageSpecification msg = (AuditMessageSpecification) o;
			tip.append(msg.getName());
		}
		return tip.toString();
	}

}
