/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.audit.message.ws;

import net.ihe.gazelle.audit.message.desc.AuditMessageSpecification;
import net.ihe.gazelle.audit.message.desc.AuditMessageSpecificationQuery;
import net.ihe.gazelle.audit.message.validator.DicomExtensionDefinition;
import net.ihe.gazelle.audit.message.validator.GlobalAuditMessageValidator;
import net.ihe.gazelle.validation.exception.GazelleValidationException;
import net.ihe.gazelle.validation.model.ValidatorDescription;
import net.ihe.gazelle.validation.ws.AbstractModelBasedValidation;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.xml.soap.SOAPException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class  AbstractAMValidatorWS extends AbstractModelBasedValidation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@WebMethod
	@WebResult(name = "DetailedResult")
	public String validateDocument(@WebParam(name = "document") String document,
			@WebParam(name = "validator") String validator) throws SOAPException {

		if ((null == validator) || (validator.isEmpty())){
			throw new SOAPException("You must provide a validator");
		}
		if ((null == document) || (document.isEmpty())){
			throw new SOAPException("You must provide a document");
		}
		String res = GlobalAuditMessageValidator.validateDocument(document, validator);
		String status = res.contains("<ValidationTestResult>FAILED</ValidationTestResult>") ? "FAILED" : "PASSED";
		addValidatorUsage(validator, status);
		return res;
	}

	@Override
	@WebMethod
	@WebResult(name = "about")
	public String about() {
		String res = "This webservice is developped by IHE-europe / gazelle team. The aim of this validator is to validate "
				+ "audit trail messages using model based validation.\n";
		res = res + "For more information please contact the manager of gazelle project eric.poiseau@inria.fr";
		return res;
	}

	@Override
	@WebMethod
	@WebResult(name = "Validators")
	public List<String> getListOfValidators(@WebParam(name = "descriminator") String descriminator)
			throws SOAPException {
		List<String> res = new ArrayList<String>();
		AuditMessageSpecificationQuery qq = new AuditMessageSpecificationQuery();
		if (descriminator != null && !descriminator.equals("")) {
			for (AuditMessageSpecification ams : qq.getList()) {
				if (ams.getName().toLowerCase().contains(descriminator.toLowerCase())) {
					addValidationTool(res, ams);
				}
			}
		} else {
			for (AuditMessageSpecification ams : qq.getList()) {
				addValidationTool(res, ams);
			}
		}
		return res;
	}

	private void addValidationTool(List<String> res, AuditMessageSpecification ams) {
		if (ams.getIsDeprecated()!= null && ams.getIsDeprecated()){
			return;
		}
		res.add(ams.getName() + " " + getKindAuditMessage(ams));
		if (!ams.getForceDicomCompatibilityAsBoolean() && 
				getKindAuditMessage(ams).equals(DicomExtensionDefinition.DICOM_COMPATIBLE.getValue())){
			res.add(ams.getName() + " " + DicomExtensionDefinition.RFC3881_COMPATIBLE.getValue());
		}
	}
	
	private String getKindAuditMessage(AuditMessageSpecification ams){
		if (ams.getIsDicomCompatibleAsBoolean()){
			return DicomExtensionDefinition.DICOM_COMPATIBLE.getValue();
		}
		else{
			return DicomExtensionDefinition.RFC3881_COMPATIBLE.getValue();
		}
	}

	@Override
	protected ValidatorDescription getValidatorByOidOrName(String value) {
		//TODO stub to implement
		return null;
	}

	@Override
	protected List<ValidatorDescription> getValidatorsForDescriminator(String descriminator) {
		//TODO stub to implement
		return null;
	}

	@Override
	protected String buildReportOnParsingFailure(GazelleValidationException e, ValidatorDescription selectedValidator) {
		//TODO stib to implement
		return null;
	}

	@Override
	protected String executeValidation(String document, ValidatorDescription validator, boolean extracted) throws GazelleValidationException {
		try {
			return validateDocument(document, validator.getName());
		} catch (SOAPException e) {
			throw  new GazelleValidationException(e.getMessage(), e);
		}
	}
}
