/*
 *   Copyright 2010-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package net.ihe.gazelle.audit.message.action;

import net.ihe.gazelle.audit.message.desc.AuditMessageSpecification;
import net.ihe.gazelle.audit.message.desc.AuditMessageSpecificationQuery;
import net.ihe.gazelle.audit.message.menu.AuditMessagePages;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Map;

/**
 * @author Abderrazek Boufahja, Cédric Eoche-Duval
 */
@Name("AuditMessageViewer")
@Scope(ScopeType.PAGE)
public class AuditMessageViewer implements Serializable, UserAttributeCommon {

    private static final long serialVersionUID = 7511552670180083515L;

    private static Logger log = LoggerFactory.getLogger(AuditMessageViewer.class);

    private AuditMessageSpecification selectedAuditMessage;

    @In(value = "gumUserService")
    private UserService userService;

    public AuditMessageSpecification getSelectedAuditMessage() {
        return selectedAuditMessage;
    }

    public void setSelectedAuditMessage(AuditMessageSpecification selectedAuditMessage) {
        this.selectedAuditMessage = selectedAuditMessage;
    }

    @PostConstruct
    public void initBean() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String messageIdString = params.get("id");
        String messageOidString = params.get("oid");
        String messageNameString = params.get("name");
        if (messageIdString != null && !messageIdString.equals("")) {
            try {
                Integer messageId = null;
                messageId = Integer.valueOf(messageIdString);
                EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
                this.selectedAuditMessage = entityManager.find(AuditMessageSpecification.class, messageId);
                if (this.selectedAuditMessage == null) {
                    throw new Exception();
                }
            } catch (Exception e) {
                FacesMessages.instance()
                        .add(StatusMessage.Severity.ERROR, "There are no AuditMessage description with the id you specified. Please set a valid Id.");
                this.selectedAuditMessage = null;
            }
        } else if (messageNameString != null && !messageNameString.equals("")) {
            messageNameString = messageNameString.trim();
            try {
                AuditMessageSpecificationQuery quer = new AuditMessageSpecificationQuery();
                quer.name().eq(messageNameString);
                this.selectedAuditMessage = quer.getUniqueResult();
                if (this.selectedAuditMessage == null) {
                    throw new Exception();
                }
            } catch (Exception e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "There are no AuditMessage description with the name you specified. Please set a valid name.");
                this.selectedAuditMessage = null;
            }
        } else if (messageOidString != null && !messageOidString.isEmpty()) {
            messageOidString = messageOidString.trim();
            try {
                AuditMessageSpecificationQuery query = new AuditMessageSpecificationQuery();
                query.oid().eq(messageOidString);
                selectedAuditMessage = query.getUniqueResult();
                if (this.selectedAuditMessage == null) {
                    throw new Exception();
                }
            } catch (Exception e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "There are no AuditMessage description with the OID you specified. Please set a valid OID.");
                this.selectedAuditMessage = null;
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "You have to specify on the URL, the name or the id of the AuditMessage you are trying to view.");
            this.selectedAuditMessage = null;
        }
    }

    public String linkToListAuditMessage() {
        return PreferenceService.getString("application_url") + AuditMessagePages.AM_LIST.getMenuLink();
    }

    @Restrict("#{s:hasRole('admin_role') or s:hasRole('tests_editor_role')}")
    public String linkToEditAuditMessage(Integer id) {
        return PreferenceService.getString("application_url") + AuditMessagePages.AM_EDIT.getMenuLink() + "?id=" + id;
    }

    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }

}
