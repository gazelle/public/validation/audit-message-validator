/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.audit.message.action;

import net.ihe.gazelle.audit.message.desc.AuditMessageSpecification;
import net.ihe.gazelle.audit.message.desc.AuditMessageSpecificationQuery;
import net.ihe.gazelle.audit.message.menu.AuditMessagePages;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.preferences.PreferenceService;

import javax.faces.context.FacesContext;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by cel on 16/10/15.
 */
public abstract class AuditMessageBrowser implements QueryModifier<AuditMessageSpecification> {

    private FilterDataModel<AuditMessageSpecification> amsDataModel;
    private Filter<AuditMessageSpecification> amsFilter;

    public FilterDataModel<AuditMessageSpecification> getAmsDataModel() {
        if (amsDataModel == null) {
            amsDataModel = new FilterDataModel<AuditMessageSpecification>(getFilter()) {
                @Override
                protected Object getId(AuditMessageSpecification t) {
                    return t.getId();
                }
            };
        }
        return this.amsDataModel;
    }

    public Filter<AuditMessageSpecification> getFilter() {
        if (amsFilter == null) {
            amsFilter = new Filter<AuditMessageSpecification>(getHQLCriterionsForFilter(), FacesContext.getCurrentInstance().getExternalContext()
                    .getRequestParameterMap());
        }
        return amsFilter;
    }

    protected HQLCriterionsForFilter<AuditMessageSpecification> getHQLCriterionsForFilter() {
        AuditMessageSpecificationQuery query = new AuditMessageSpecificationQuery();
        HQLCriterionsForFilter<AuditMessageSpecification> criterions = query.getHQLCriterionsForFilter();
        criterions.addQueryModifier(this);
        return criterions;
    }

    public static List<AuditMessageSpecification> getListAuditMessageSpecifications() {
        AuditMessageSpecificationQuery qq = new AuditMessageSpecificationQuery();
        List<AuditMessageSpecification> res = qq.getList();
        if (res != null) {
            Collections.sort(res);
        }
        return res;
    }

    public void modifyQuery(HQLQueryBuilder<AuditMessageSpecification> hqlQueryBuilder, Map<String, Object> map) {
        //No need to modify query for now.
    }

    public String navigateViewAuditMessage(int id) {
        return AuditMessagePages.AM_VIEW.getLink() + "?id=" + id;
    }

    public String linkToViewAuditMessage(AuditMessageSpecification auditMessageSpecification) {
        return PreferenceService.getString("application_url") + AuditMessagePages.AM_VIEW.getMenuLink() + "?id=" + auditMessageSpecification.getId();
    }

    public String linkToCreateAuditMessage() {
        return PreferenceService.getString("application_url") + AuditMessagePages.AM_EDIT.getMenuLink();
    }

    public String navigateListAuditMessage() {
        return AuditMessagePages.AM_LIST.getLink();
    }

    public String linkToListAuditMessage() {
        return PreferenceService.getString("application_url") + AuditMessagePages.AM_LIST.getMenuLink();
    }
}
