/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.audit.message.validator;

import net.ihe.gazelle.audit.message.desc.ActiveParticipantDescription;
import net.ihe.gazelle.audit.message.desc.AuditMessageSpecification;
import net.ihe.gazelle.audit.message.desc.ConstraintSpecification;
import net.ihe.gazelle.audit.message.desc.ElementDescription;
import net.ihe.gazelle.audit.message.desc.Optionality;
import net.ihe.gazelle.audit.message.desc.ParticipantObjectIdentificationDesc;
import net.ihe.gazelle.validation.Notification;

import java.util.List;

public class AuditMessageValidator implements AuditValidatorInterface {

    private static final String ERROR = "error";

    private static final String WARN = "warning";

    protected AuditMessageValidator() {
    }

    public static final AuditMessageValidator instance = new AuditMessageValidator();

    public void validate(String document, AuditMessageSpecification ams, List<Notification> diagnostic)
            throws AuditMessageValidatorException {
        testConformance(document, ams, diagnostic);
        if (ams.getEvent() != null) {
            EventValidator.instance.validate(document, ams, diagnostic);
        }

        if (ams.getActiveParticipants() != null) {
            for (ActiveParticipantDescription act : ams.getActiveParticipants()) {
                if (act.getMediaIdentifier() == null) {
                    act.setMediaIdentifier(initElementDescription());
                }
                String distinguisher = createDistinguisher(act.getDistinguisher(), ams.getIsDicomCompatibleAsBoolean());
                ActiveParticipantValidator.instance
                        .validate(document, diagnostic, act, distinguisher, ams.getIsDicomCompatibleAsBoolean());
            }
        }

        if (ams.getAuditSource() != null) {
            AuditSourceValidator.instance.validate(document, ams, diagnostic);
        }

        if (ams.getParticipantObjectIdentifications() != null) {
            for (ParticipantObjectIdentificationDesc desc : ams.getParticipantObjectIdentifications()) {
                String distinguisher = createDistinguisher(desc.getDistinguisher(),
                        ams.getIsDicomCompatibleAsBoolean());
                ParticipantObjectIdentificationValidator.instance
                        .validate(document, diagnostic, desc, distinguisher, ams.getIsDicomCompatibleAsBoolean());
            }
        }

        validateActiveParticipantsCardinality(document, ams, diagnostic);
        validateAuditSourceCardinality(document, ams, diagnostic);
        validateEventCardinality(document, ams, diagnostic);
        validateParticipantObjectIdentificationCardinality(document, ams, diagnostic);
        validateAllowedActiveParticipants(document, ams, diagnostic);
        validateAllowedParticipantObjectIdentifications(document, ams, diagnostic);

        if (ams.getExtraConstraintSpecifications() != null) {
            for (ConstraintSpecification conspec : ams.getExtraConstraintSpecifications()) {
                this.validateCostraintSpecification(document, conspec, diagnostic);
            }
        }
    }

    private String createDistinguisher(String distinguisher, Boolean isDicomCompatible) {
        if (isDicomCompatible == null) isDicomCompatible = false;
        String res = distinguisher;
        if (isDicomCompatible) {
            res = res.replace("@code", "@csd-code");
        } else {
            res = res.replace("@csd-code", "@code");
        }
        return res;
    }

    private ElementDescription initElementDescription() {
        ElementDescription ed = new ElementDescription();
        ed.setOptionality(Optionality.NA);
        return ed;
    }

    protected void validateCostraintSpecification(String document, ConstraintSpecification cons,
                                                  List<Notification> diagnostic) {
        if (cons != null) {
            ValidatorUtil.handleXpath(cons.getXpath(), cons.getDescription(), cons.getKind(), document, diagnostic);
        }
    }

    protected void validateEventCardinality(String document, AuditMessageSpecification ams,
                                            List<Notification> diagnostic) {
        String xpath = "/AuditMessage/EventIdentification";
        ValidatorUtil.handleXpath(xpath, "The cardinality of Event is 1..1", ERROR, document, diagnostic);
    }

    protected void validateActiveParticipantsCardinality(String document, AuditMessageSpecification ams,
                                                         List<Notification> diagnostic) {
        for (ActiveParticipantDescription act : ams.getActiveParticipants()) {
            if (act.getMin() != null && act.getMax() != null) {
                if (!act.getMax().equals("*")) {
                    String desc = "The number of " + act.getName() + " SHALL be less than or equal to " + act.getMax();
                    String xpath = "count(/AuditMessage/ActiveParticipant[" + this
                            .createDistinguisher(act.getDistinguisher(),
                                    ams.getIsDicomCompatibleAsBoolean()) + "])<" + (Integer.valueOf(act.getMax()) + 1);
                    ValidatorUtil.handleXpath(xpath, desc, ERROR, document, diagnostic);
                }
                if (!act.getMin().equals("0")) {
                    String desc = "The number of " + act.getName() + " SHALL be greater than or equal to " + act
                            .getMin();
                    String xpath = "count(/AuditMessage/ActiveParticipant[" + this
                            .createDistinguisher(act.getDistinguisher(),
                                    ams.getIsDicomCompatibleAsBoolean()) + "])>" + (Integer.valueOf(act.getMin()) - 1);
                    ValidatorUtil.handleXpath(xpath, desc, ERROR, document, diagnostic);
                }
            }
        }
    }

    protected void validateAuditSourceCardinality(String document, AuditMessageSpecification ams,
                                                  List<Notification> diagnostic) {
        if (ams != null && ams.getAuditSource() != null && ams.getAuditSource().getMin() != null && ams.getAuditSource()
                .getMax() != null) {
            if (!ams.getAuditSource().getMax().equals("*")) {
                String desc = "The number of AuditSource SHALL be less than or equal to " + ams.getAuditSource()
                        .getMax();
                String xpath = "count(/AuditMessage/AuditSourceIdentification)<" + (Integer
                        .valueOf(ams.getAuditSource().getMax()) + 1);
                ValidatorUtil.handleXpath(xpath, desc, ERROR, document, diagnostic);
            }
            if (!ams.getAuditSource().getMin().equals("0")) {
                String desc = "The number of AuditSource SHALL be greater than or equal to " + ams.getAuditSource()
                        .getMin();
                String xpath = "count(/AuditMessage/AuditSourceIdentification)>" + (Integer
                        .valueOf(ams.getAuditSource().getMin()) - 1);
                ValidatorUtil.handleXpath(xpath, desc, ERROR, document, diagnostic);
            }
        }
    }

    protected void validateParticipantObjectIdentificationCardinality(String document, AuditMessageSpecification ams,
                                                                      List<Notification> diagnostic) {
        for (ParticipantObjectIdentificationDesc act : ams.getParticipantObjectIdentifications()) {
            if (act.getMin() != null && act.getMax() != null) {
                if (!act.getMax().equals("*")) {
                    String desc = "The number of " + act.getName() + " SHALL be less than or equal to " + act.getMax();
                    String xpath = "count(/AuditMessage/ParticipantObjectIdentification[" + this
                            .createDistinguisher(act.getDistinguisher(),
                                    ams.getIsDicomCompatibleAsBoolean()) + "])<" + (Integer.valueOf(act.getMax()) + 1);
                    ValidatorUtil.handleXpath(xpath, desc, ERROR, document, diagnostic);
                }
                if (!act.getMin().equals("0")) {
                    String desc = "The number of " + act.getName() + " SHALL be greater than or equal to " + act
                            .getMin();
                    String xpath = "count(/AuditMessage/ParticipantObjectIdentification[" + this
                            .createDistinguisher(act.getDistinguisher(),
                                    ams.getIsDicomCompatibleAsBoolean()) + "])>" + (Integer.valueOf(act.getMin()) - 1);
                    ValidatorUtil.handleXpath(xpath, desc, ERROR, document, diagnostic);
                }
            }
        }
    }

    protected void validateAllowedParticipantObjectIdentifications(String document, AuditMessageSpecification ams,
                                                                   List<Notification> diagnostic) {
        StringBuilder xpath = new StringBuilder(
                "every  $partid in /AuditMessage/ParticipantObjectIdentification satisfies ");
        StringBuilder desc = new StringBuilder("Allowed ParticipantObjectIdentifications are ");
        boolean first = true;
        for (ParticipantObjectIdentificationDesc partid : ams.getParticipantObjectIdentifications()) {
            if (first) {
                xpath.append("$partid[");
                xpath.append(this.createDistinguisher(partid.getDistinguisher(), ams.getIsDicomCompatibleAsBoolean()));
                xpath.append("]");
                desc.append(partid.getName());
            } else {
                xpath.append(" or $partid[");
                xpath.append(this.createDistinguisher(partid.getDistinguisher(), ams.getIsDicomCompatibleAsBoolean()));
                xpath.append("]");
                desc.append(", ");
                desc.append(partid.getName());
            }
            first = false;
        }
        ValidatorUtil.handleXpath(xpath.toString(), desc.toString(), WARN, document, diagnostic);
    }

    protected void validateAllowedActiveParticipants(String document, AuditMessageSpecification ams,
                                                     List<Notification> diagnostic) {
        StringBuilder xpath = new StringBuilder("every  $act in /AuditMessage/ActiveParticipant satisfies ");
        StringBuilder desc = new StringBuilder("Allowed ActiveParticipants are ");
        boolean first = true;
        for (ActiveParticipantDescription act : ams.getActiveParticipants()) {
            if (first) {
                xpath.append("$act[");
                xpath.append(this.createDistinguisher(act.getDistinguisher(), ams.getIsDicomCompatibleAsBoolean()));
                xpath.append("]");
                desc.append(act.getName());
            } else {
                xpath.append(" or $act[");
                xpath.append(this.createDistinguisher(act.getDistinguisher(), ams.getIsDicomCompatibleAsBoolean()));
                xpath.append("]");
                desc.append(", ");
                desc.append(act.getName());
            }
            first = false;
        }
        ValidatorUtil.handleXpath(xpath.toString(), desc.toString(), WARN, document, diagnostic);
    }

    protected void testConformance(String document,
                                   AuditMessageSpecification ams, List<Notification> diagnostic)
            throws AuditMessageValidatorException {
        if (document == null) {
            throw new AuditMessageValidatorException("The document is empty", "AuditMessageValidator");
        }
        if (ams == null) {
            throw new AuditMessageValidatorException("The AuditMessageSpecification is null", "AuditMessageValidator");
        }

        if (diagnostic == null) {
            throw new AuditMessageValidatorException("The diagnostic list is not initialized", "AuditMessageValidator");
        }
    }

}
