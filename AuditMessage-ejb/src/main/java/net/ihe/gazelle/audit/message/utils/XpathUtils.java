/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.audit.message.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import net.sf.saxon.xpath.XPathFactoryImpl;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public final class XpathUtils {
	
	private XpathUtils(){}
	
	public static Boolean evaluateByString(String string, String expression) throws XPathExpressionException {
		return XpathUtils.evaluateByString(string, expression, null);
	}

	public static Boolean evaluateByString(String string, String expression, NamespaceContext xmlns) throws XPathExpressionException {
		Boolean b = null;
		XPathFactory fabrique = new XPathFactoryImpl();
		XPath xpath = fabrique.newXPath();
		if (xmlns != null){
			xpath.setNamespaceContext(xmlns);
		}
		InputSource is = new InputSource(new ByteArrayInputStream(string.getBytes(Charset.forName("UTF-8"))));
		b = (Boolean) xpath.evaluate(expression, is, XPathConstants.BOOLEAN);
		return b;
	}
	
	public static Boolean evaluateByNode(Node node, String expression) throws XPathExpressionException {
		return XpathUtils.evaluateByNode(node, expression, null);
	}

	public static Boolean evaluateByNode(Node node, String expression, NamespaceContext xmlns) throws XPathExpressionException {
		Boolean b = null;
		XPathFactory fabrique = new XPathFactoryImpl();
		XPath xpath = fabrique.newXPath();
		if (xmlns != null){
			xpath.setNamespaceContext(xmlns);
		}
		b = (Boolean) xpath.evaluate(expression, node, XPathConstants.BOOLEAN);
		return b;
	}

	public static Node getNodeFromString(String string, String nodeName) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(new ByteArrayInputStream(string.getBytes(Charset.forName("UTF-8"))));
		NodeList dd = doc.getElementsByTagName(nodeName);
		return dd.item(0);
	}

}
