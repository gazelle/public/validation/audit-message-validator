/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.audit.message.validator;

import net.ihe.gazelle.audit.message.desc.ParticipantObjectIdentificationDesc;
import net.ihe.gazelle.validation.Notification;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParticipantObjectIdentificationValidator {

	private static final String WARNING = "warning";
	private static final String ERROR = "error";

	protected ParticipantObjectIdentificationValidator(){}

	public static final ParticipantObjectIdentificationValidator instance = new ParticipantObjectIdentificationValidator();
	
	public void validate(String document,
			List<Notification> diagnostic, ParticipantObjectIdentificationDesc partid, String partidDistinguisher, boolean isDicomCompatible) {
		this.validateParticipantObjectDataLifeCycleOpt(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectDataLifeCycleRegex(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectDataLifeCycleValue(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectDetailOpt(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectDetailRegex(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectDetailValue(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectIDOpt(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectIDRegex(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectIDTypeCodeOpt(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectIDTypeCodeValue(document, diagnostic, partid, partidDistinguisher, isDicomCompatible);
		this.validateParticipantObjectIDValue(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectNameOpt(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectNameRegex(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectNameValue(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectQueryOpt(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectQueryRegex(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectQueryValue(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectSensitivityOpt(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectSensitivityRegex(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectSensitivityValue(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectTypeCodeOpt(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectTypeCodeRegex(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectTypeCodeValue(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectTypeCodeRoleOpt(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectTypeCodeRoleRegex(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectTypeCodeRoleValue(document, diagnostic, partid, partidDistinguisher);
		
		this.validateParticipantObjectDescriptionOpt(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectDescriptionValue(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectDescriptionRegex(document, diagnostic, partid, partidDistinguisher);
	
		this.validateMPPSOpt(document, diagnostic, partid, partidDistinguisher);
		this.validateMPPSValue(document, diagnostic, partid, partidDistinguisher);
		this.validateMPPSRegex(document, diagnostic, partid, partidDistinguisher);
		
		this.validateAccessionOpt(document, diagnostic, partid, partidDistinguisher);
		this.validateAccessionValue(document, diagnostic, partid, partidDistinguisher);
		this.validateAccessionRegex(document, diagnostic, partid, partidDistinguisher);
		
		this.validateSOPClassOpt(document, diagnostic, partid, partidDistinguisher);
		this.validateSOPClassValue(document, diagnostic, partid, partidDistinguisher);
		this.validateSOPClassRegex(document, diagnostic, partid, partidDistinguisher);
		
		this.validateParticipantObjectContainsStudyOpt(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectContainsStudyValue(document, diagnostic, partid, partidDistinguisher);
		this.validateParticipantObjectContainsStudyRegex(document, diagnostic, partid, partidDistinguisher);
		
		this.validateEncryptedOpt(document, diagnostic, partid, partidDistinguisher);
		this.validateEncryptedValue(document, diagnostic, partid, partidDistinguisher);
		this.validateEncryptedRegex(document, diagnostic, partid, partidDistinguisher);
		
		this.validateAnonymizedOpt(document, diagnostic, partid, partidDistinguisher);
		this.validateAnonymizedValue(document, diagnostic, partid, partidDistinguisher);
		this.validateAnonymizedRegex(document, diagnostic, partid, partidDistinguisher);
		
		this.validateNumberOfInstancesOpt(document, diagnostic, partid, partidDistinguisher);
		this.validateNumberOfInstancesValue(document, diagnostic, partid, partidDistinguisher);
		this.validateNumberOfInstancesRegex(document, diagnostic, partid, partidDistinguisher);
		
		this.validateInstanceOpt(document, diagnostic, partid, partidDistinguisher);
		this.validateInstanceValue(document, diagnostic, partid, partidDistinguisher);
		this.validateInstanceRegex(document, diagnostic, partid, partidDistinguisher);
		
	}
	
	protected void validateAnonymizedOpt(String document, List<Notification> diagnostic,
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher) {
		if (partid != null && partid.getAnonymized() != null && partid.getAnonymized().getOptionality() != null){
			try{
				String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + 
						"] satisfies $partid/Anonymized";

				switch (partid.getAnonymized().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/Anonymized is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/Anonymized has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies not($partid/Anonymized)";
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/Anonymized is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
					e.printStackTrace();
			}
		}
	}

	protected void validateAnonymizedValue(String document, List<Notification> diagnostic,
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher) {
		if (partid != null && partid.getAnonymized() != null && partid.getAnonymized().getValue() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/Anonymized) or $partid/Anonymized/text()='" + 
					partid.getAnonymized().getValue() + "'";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/Anonymized/text() must be '" + partid.getAnonymized().getValue() + 
					"' if /" + partid.getName() + "/Anonymized present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateAnonymizedRegex(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getAnonymized() != null && partid.getAnonymized().getRegex() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/Anonymized) or matches($partid/Anonymized/text(),'" + 
					partid.getAnonymized().getRegex() + "')";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/Anonymized/text() must have this form '" + partid.getAnonymized().getRegex() + 
					"' if " + partid.getName() +  "/Anonymized present" ,
					ERROR, document, diagnostic);
		}
	}
	
	
	protected void validateEncryptedOpt(String document, List<Notification> diagnostic,
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher) {
		if (partid != null && partid.getEncrypted() != null && partid.getEncrypted().getOptionality() != null){
			try{
				String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + 
						"] satisfies $partid/Encrypted";

				switch (partid.getEncrypted().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/Encrypted is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/Encrypted has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies not($partid/Encrypted)";
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/Encrypted is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
					e.printStackTrace();
			}
		}
	}

	protected void validateEncryptedValue(String document, List<Notification> diagnostic,
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher) {
		if (partid != null && partid.getEncrypted() != null && partid.getEncrypted().getValue() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/Encrypted) or $partid/Encrypted/text()='" + 
					partid.getEncrypted().getValue() + "'";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/Encrypted/text() must be '" + partid.getEncrypted().getValue() + 
					"' if /" + partid.getName() + "/Encrypted present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateEncryptedRegex(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getEncrypted() != null && partid.getEncrypted().getRegex() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/Encrypted) or matches($partid/Encrypted/text(),'" + 
					partid.getEncrypted().getRegex() + "')";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/Encrypted/text() must have this form '" + partid.getEncrypted().getRegex() + 
					"' if " + partid.getName() +  "/Encrypted present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateParticipantObjectContainsStudyOpt(String document, List<Notification> diagnostic,
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher) {
		if (partid != null && partid.getParticipantObjectContainsStudy() != null && partid.getParticipantObjectContainsStudy().getOptionality() != null){
			try{
				String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + 
						"] satisfies $partid/ParticipantObjectContainsStudy/StudyIDs";

				switch (partid.getParticipantObjectContainsStudy().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectContainsStudy/StudyIDs is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectContainsStudy/StudyIDs has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies not($partid/ParticipantObjectContainsStudy/StudyIDs)";
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectContainsStudy/StudyIDs is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
					e.printStackTrace();
			}
		}
	}

	protected void validateParticipantObjectContainsStudyValue(String document, List<Notification> diagnostic,
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher) {
		if (partid != null && partid.getParticipantObjectContainsStudy() != null && partid.getParticipantObjectContainsStudy().getValue() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/ParticipantObjectContainsStudy/StudyIDs/@UID) or $partid/ParticipantObjectContainsStudy/StudyIDs/@UID='" + 
					partid.getParticipantObjectContainsStudy().getValue() + "'";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/ParticipantObjectContainsStudy/StudyIDs/@UID must be '" + partid.getParticipantObjectContainsStudy().getValue() + 
					"' if /" + partid.getName() + "/ParticipantObjectContainsStudy/StudyIDs/@UID present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateParticipantObjectContainsStudyRegex(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectContainsStudy() != null && partid.getParticipantObjectContainsStudy().getRegex() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/ParticipantObjectContainsStudy/StudyIDs/@UID) or matches($partid/ParticipantObjectContainsStudy/StudyIDs/@UID,'" + 
					partid.getParticipantObjectContainsStudy().getRegex() + "')";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/ParticipantObjectContainsStudy/StudyIDs/@UID must have this form '" + partid.getParticipantObjectContainsStudy().getRegex() + 
					"' if " + partid.getName() +  "/ParticipantObjectContainsStudy/StudyIDs/@UID present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateSOPClassOpt(String document, List<Notification> diagnostic,
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher) {
		if (partid != null && partid.getSopClass() != null && partid.getSopClass().getOptionality() != null){
			try{
				String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + 
						"] satisfies $partid/SOPClass";

				switch (partid.getSopClass().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/SOPClass is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/SOPClass has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies not($partid/SOPClass)";
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/SOPClass is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
					e.printStackTrace();
			}
		}
	}

	protected void validateSOPClassValue(String document, List<Notification> diagnostic,
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher) {
		if (partid != null && partid.getSopClass() != null && partid.getSopClass().getValue() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/SOPClass/@UID) or $partid/SOPClass/@UID='" + 
					partid.getSopClass().getValue() + "'";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/SOPClass/@UID must be '" + partid.getSopClass().getValue() + 
					"' if /" + partid.getName() + "/SOPClass/@UID present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateSOPClassRegex(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getSopClass() != null && partid.getSopClass().getRegex() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/SOPClass/@UID) or matches($partid/SOPClass/@UID,'" + 
					partid.getSopClass().getRegex() + "')";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/SOPClass/@UID must have this form '" + partid.getSopClass().getRegex() + 
					"' if " + partid.getName() +  "/SOPClass/@UID present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateAccessionOpt(String document, List<Notification> diagnostic,
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher) {
		if (partid != null && partid.getAccession() != null && partid.getAccession().getOptionality() != null){
			try{
				String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + 
						"] satisfies $partid/Accession";

				switch (partid.getAccession().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/Accession is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/Accession has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies not($partid/Accession)";
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/Accession is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
					e.printStackTrace();
			}
		}
	}

	protected void validateAccessionValue(String document, List<Notification> diagnostic,
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher) {
		if (partid != null && partid.getAccession() != null && partid.getAccession().getValue() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/Accession/@Number) or $partid/Accession/@Number='" + 
					partid.getAccession().getValue() + "'";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/Accession/@Number must be '" + partid.getAccession().getValue() + 
					"' if /" + partid.getName() + "/Accession/@Number present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateAccessionRegex(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getAccession() != null && partid.getAccession().getRegex() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/Accession/@Number) or matches($partid/Accession/@Number,'" + 
					partid.getAccession().getRegex() + "')";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/Accession/@Number must have this form '" + partid.getAccession().getRegex() + 
					"' if " + partid.getName() +  "/Accession/@Number present" ,
					ERROR, document, diagnostic);
		}
	}

	protected void validateMPPSOpt(String document, List<Notification> diagnostic,
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher) {
		if (partid != null && partid.getMpps() != null && partid.getMpps().getOptionality() != null){
			try{
				String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + 
						"] satisfies $partid/MPPS";

				switch (partid.getMpps().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/MPPS is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/MPPS has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies not($partid/MPPS)";
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/MPPS is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
					e.printStackTrace();
			}
		}
	}

	protected void validateMPPSValue(String document, List<Notification> diagnostic,
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher) {
		if (partid != null && partid.getMpps() != null && partid.getMpps().getValue() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/MPPS/@UID) or $partid/MPPS/@UID='" + 
					partid.getMpps().getValue() + "'";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/MPPS/@UID must be '" + partid.getMpps().getValue() + 
					"' if /" + partid.getName() + "/MPPS/@UID present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateMPPSRegex(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getMpps() != null && partid.getMpps().getRegex() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/MPPS/@UID) or matches($partid/MPPS/@UID,'" + 
					partid.getMpps().getRegex() + "')";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/MPPS/@UID must have this form '" + partid.getMpps().getRegex() + 
					"' if " + partid.getName() +  "/MPPS/@UID present" ,
					ERROR, document, diagnostic);
		}
	}

	protected void validateParticipantObjectTypeCodeOpt(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectTypeCode() != null && partid.getParticipantObjectTypeCode().getOptionality() != null){
			try{
				String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies $partid/@ParticipantObjectTypeCode";

				switch (partid.getParticipantObjectTypeCode().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectTypeCode is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectTypeCode has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies not($partid/@ParticipantObjectTypeCode)";
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectTypeCode is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	protected void validateParticipantObjectTypeCodeValue(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectTypeCode() != null && partid.getParticipantObjectTypeCode().getValue() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/@ParticipantObjectTypeCode) or $partid/@ParticipantObjectTypeCode='" + 
					partid.getParticipantObjectTypeCode().getValue() + "'";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/@ParticipantObjectTypeCode must be '" + partid.getParticipantObjectTypeCode().getValue() + 
					"' if /" + partid.getName() + "/@ParticipantObjectTypeCode present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateParticipantObjectTypeCodeRegex(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectTypeCode() != null && partid.getParticipantObjectTypeCode().getRegex() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/@ParticipantObjectTypeCode) or matches($partid/@ParticipantObjectTypeCode,'" + 
					partid.getParticipantObjectTypeCode().getRegex() + "')";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/@ParticipantObjectTypeCode must have this form '" + partid.getParticipantObjectTypeCode().getRegex() + 
					"' if " + partid.getName() +  "/@ParticipantObjectTypeCode present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateParticipantObjectTypeCodeRoleOpt(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectTypeCodeRole() != null && partid.getParticipantObjectTypeCodeRole().getOptionality() != null){
			try{
				String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies $partid/@ParticipantObjectTypeCodeRole";

				switch (partid.getParticipantObjectTypeCodeRole().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectTypeCodeRole is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectTypeCodeRole has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies not($partid/@ParticipantObjectTypeCodeRole)";
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectTypeCodeRole is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	protected void validateParticipantObjectTypeCodeRoleValue(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectTypeCodeRole() != null && partid.getParticipantObjectTypeCodeRole().getValue() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/@ParticipantObjectTypeCodeRole) or $partid/@ParticipantObjectTypeCodeRole='" + 
					partid.getParticipantObjectTypeCodeRole().getValue() + "'";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/@ParticipantObjectTypeCodeRole must be '" + partid.getParticipantObjectTypeCodeRole().getValue() + 
					"' if /" + partid.getName() + "/@ParticipantObjectTypeCodeRole present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateParticipantObjectTypeCodeRoleRegex(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectTypeCodeRole() != null && partid.getParticipantObjectTypeCodeRole().getRegex() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/@ParticipantObjectTypeCodeRole) or matches($partid/@ParticipantObjectTypeCodeRole,'" + 
					partid.getParticipantObjectTypeCodeRole().getRegex() + "')";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/@ParticipantObjectTypeCodeRole must have this form '" + partid.getParticipantObjectTypeCodeRole().getRegex() + 
					"' if " + partid.getName() +  "/@ParticipantObjectTypeCodeRole present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateParticipantObjectDataLifeCycleOpt(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectDataLifeCycle() != null && partid.getParticipantObjectDataLifeCycle().getOptionality() != null){
			try{
				String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher +
						"] satisfies $partid/@ParticipantObjectDataLifeCycle";

				switch (partid.getParticipantObjectDataLifeCycle().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/@ParticipantObjectDataLifeCycle is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/@ParticipantObjectDataLifeCycle has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + 
					"] satisfies not($partid/@ParticipantObjectDataLifeCycle)";
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/@ParticipantObjectDataLifeCycle is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	protected void validateParticipantObjectDataLifeCycleValue(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectDataLifeCycle() != null && partid.getParticipantObjectDataLifeCycle().getValue() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/@ParticipantObjectDataLifeCycle) or $partid/@ParticipantObjectDataLifeCycle='" + 
					partid.getParticipantObjectDataLifeCycle().getValue() + "'";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/@ParticipantObjectDataLifeCycle must be '" + partid.getParticipantObjectDataLifeCycle().getValue() + 
					"' if /" + partid.getName() + "/@ParticipantObjectDataLifeCycle present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateParticipantObjectDataLifeCycleRegex(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectDataLifeCycle() != null && partid.getParticipantObjectDataLifeCycle().getRegex() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/@ParticipantObjectDataLifeCycle) or matches($partid/@ParticipantObjectDataLifeCycle,'" + 
					partid.getParticipantObjectDataLifeCycle().getRegex() + "')";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/@ParticipantObjectDataLifeCycle must have this form '" + partid.getParticipantObjectDataLifeCycle().getRegex() + 
					"' if " + partid.getName() +  "/@ParticipantObjectDataLifeCycle present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateParticipantObjectIDTypeCodeOpt(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectIDTypeCode() != null && partid.getParticipantObjectIDTypeCode().getOptionality() != null){
			try{
				String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies $partid/ParticipantObjectIDTypeCode";

				switch (partid.getParticipantObjectIDTypeCode().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectIDTypeCode is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectIDTypeCode has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies not($partid/ParticipantObjectIDTypeCode)";
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectIDTypeCode is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	protected void validateParticipantObjectIDTypeCodeValue(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher, boolean isDicomCompatible){
		if (partid != null && partid.getParticipantObjectIDTypeCode() != null && partid.getParticipantObjectIDTypeCode().getValue() != null){
			try{
				String val = partid.getParticipantObjectIDTypeCode().getValue();
				Pattern pat = Pattern.compile("^\\s*EV\\s*\\(\\s*'(.*?)'\\s*,\\s*'(.*?)'\\s*,\\s*'(.*?)'\\s*\\)\\s*$");
				Matcher matcher = pat.matcher(val);
				if (matcher.find()){
					this.validateParticipantObjectIDTypeCodeCodeValue(matcher.group(1), document , diagnostic, partid, partidDistinguisher, isDicomCompatible);
					this.validateParticipantObjectIDTypeCodeCodeSystemNameValue(matcher.group(2), document , diagnostic, partid, partidDistinguisher);
					this.validateParticipantObjectIDTypeCodeDisplayNameValue(matcher.group(3), document , diagnostic, partid, partidDistinguisher, isDicomCompatible);
				}
			}
			catch(Exception e){

			}
		}
	}
	
	protected void validateParticipantObjectIDTypeCodeCodeValue(String code, String document,  List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher, boolean isDicomCompatible) 
			throws Exception{
		String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " + 
				"(every $i in $partid/ParticipantObjectIDTypeCode satisfies  $i/@" + ValidatorUtil.getCodeIdentifier(isDicomCompatible) + "='" + code + "')";
		ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectIDTypeCode@" + ValidatorUtil.getCodeIdentifier(isDicomCompatible) + 
				" must be '" + code + 
				"' if " + partid.getName() + "/ParticipantObjectIDTypeCode present", ERROR, document, diagnostic);
	}
	
	protected void validateParticipantObjectIDTypeCodeCodeSystemNameValue(String codeSystemName, String document,  List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher) 
			throws Exception{
		String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " + 
				"(every $i in $partid/ParticipantObjectIDTypeCode satisfies  $i/@codeSystemName='" + codeSystemName + "')";
		ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectIDTypeCode@codeSystemName must be '" + codeSystemName + 
				"' if " + partid.getName() + "/ParticipantObjectIDTypeCode present", ERROR, document, diagnostic);
	}
	
	protected void validateParticipantObjectIDTypeCodeDisplayNameValue(String displayName, String document,  List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher, boolean isDicomCompatible) 
			throws Exception{
		String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " + 
				"(every $i in $partid/ParticipantObjectIDTypeCode satisfies  $i/@" + ValidatorUtil.getDisplayNameIdentifier(isDicomCompatible) + 
				"='" + displayName + "')";
		ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectIDTypeCode@" + ValidatorUtil.getDisplayNameIdentifier(isDicomCompatible) +
                " should be '" + displayName +
                "' if " + partid.getName() + "/ParticipantObjectIDTypeCode present", WARNING, document, diagnostic);
    }
	
	protected void validateParticipantObjectSensitivityOpt(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectSensitivity() != null && partid.getParticipantObjectSensitivity().getOptionality() != null){
			try{
				String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies $partid/@ParticipantObjectSensitivity";

				switch (partid.getParticipantObjectSensitivity().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/@ParticipantObjectSensitivity is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/@ParticipantObjectSensitivity has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies not($partid/@ParticipantObjectSensitivity)";
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/@ParticipantObjectSensitivity is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	protected void validateParticipantObjectSensitivityValue(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectSensitivity() != null && partid.getParticipantObjectSensitivity().getValue() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/@ParticipantObjectSensitivity) or $partid/@ParticipantObjectSensitivity='" + 
					partid.getParticipantObjectSensitivity().getValue() + "'";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/@ParticipantObjectSensitivity must be '" + partid.getParticipantObjectSensitivity().getValue() + 
					"' if /" + partid.getName() + "/@ParticipantObjectSensitivity present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateParticipantObjectSensitivityRegex(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectSensitivity() != null && partid.getParticipantObjectSensitivity().getRegex() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/@ParticipantObjectSensitivity) or matches($partid/@ParticipantObjectSensitivity,'" + 
					partid.getParticipantObjectSensitivity().getRegex() + "')";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/@ParticipantObjectSensitivity must have this form '" + partid.getParticipantObjectSensitivity().getRegex() + 
					"' if " + partid.getName() +  "/@ParticipantObjectSensitivity present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateParticipantObjectIDOpt(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectID() != null && partid.getParticipantObjectID().getOptionality() != null){
			try{
				String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + 
						"] satisfies $partid/@ParticipantObjectID";

				switch (partid.getParticipantObjectID().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectID is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/@ParticipantObjectID has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies not($partid/@ParticipantObjectID)";
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/@ParticipantObjectID is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	protected void validateParticipantObjectIDValue(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectID() != null && partid.getParticipantObjectID().getValue() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/@ParticipantObjectID) or $partid/@ParticipantObjectID='" + 
					partid.getParticipantObjectID().getValue() + "'";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/@ParticipantObjectID must be '" + partid.getParticipantObjectID().getValue() + 
					"' if /" + partid.getName() + "/@ParticipantObjectID present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateParticipantObjectIDRegex(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectID() != null && partid.getParticipantObjectID().getRegex() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/@ParticipantObjectID) or matches($partid/@ParticipantObjectID,'" + 
					partid.getParticipantObjectID().getRegex() + "')";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/@ParticipantObjectID must have this form '" + partid.getParticipantObjectID().getRegex() + 
					"' if " + partid.getName() +  "/@ParticipantObjectID present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateParticipantObjectNameOpt(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectName() != null && partid.getParticipantObjectName().getOptionality() != null){
			try{
				String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + 
						"] satisfies $partid/ParticipantObjectName";

				switch (partid.getParticipantObjectName().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectName is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectName has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies not($partid/ParticipantObjectName)";
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectName is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	protected void validateParticipantObjectNameValue(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectName() != null && partid.getParticipantObjectName().getValue() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/ParticipantObjectName) or $partid/ParticipantObjectName='" + 
					partid.getParticipantObjectName().getValue() + "'";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/ParticipantObjectName must be '" + partid.getParticipantObjectName().getValue() + 
					"' if /" + partid.getName() + "/ParticipantObjectName present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateParticipantObjectNameRegex(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectName() != null && partid.getParticipantObjectName().getRegex() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/ParticipantObjectName) or matches($partid/ParticipantObjectName,'" + 
					partid.getParticipantObjectName().getRegex() + "')";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/ParticipantObjectName must have this form '" + partid.getParticipantObjectName().getRegex() + 
					"' if " + partid.getName() +  "/ParticipantObjectName present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateParticipantObjectQueryOpt(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectQuery() != null && partid.getParticipantObjectQuery().getOptionality() != null){
			try{
				String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + 
						"] satisfies $partid/ParticipantObjectQuery";

				switch (partid.getParticipantObjectQuery().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectQuery is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectQuery has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies not($partid/ParticipantObjectQuery)";
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectQuery is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	protected void validateParticipantObjectQueryValue(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectQuery() != null && partid.getParticipantObjectQuery().getValue() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/ParticipantObjectQuery) or $partid/ParticipantObjectQuery='" + 
					partid.getParticipantObjectQuery().getValue() + "'";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/ParticipantObjectQuery must be '" + partid.getParticipantObjectQuery().getValue() + 
					"' if /" + partid.getName() + "/ParticipantObjectQuery present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateParticipantObjectQueryRegex(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectQuery() != null && partid.getParticipantObjectQuery().getRegex() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/ParticipantObjectQuery) or matches($partid/ParticipantObjectQuery,'" + 
					partid.getParticipantObjectQuery().getRegex() + "')";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/ParticipantObjectQuery must have this form '" + partid.getParticipantObjectQuery().getRegex() + 
					"' if " + partid.getName() +  "/ParticipantObjectQuery present" ,
					ERROR, document, diagnostic);
		}
	}
	
	
	protected void validateParticipantObjectDetailOpt(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectDetail() != null && partid.getParticipantObjectDetail().getOptionality() != null){
			try{
				String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + 
						"] satisfies $partid/ParticipantObjectDetail";

				switch (partid.getParticipantObjectDetail().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectDetail is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectDetail has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies not($partid/ParticipantObjectDetail)";
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectDetail is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	protected void validateParticipantObjectDescriptionOpt(String document, List<Notification> diagnostic,
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher) {
		if (partid != null && partid.getParticipantObjectDescription() != null && partid.getParticipantObjectDescription().getOptionality() != null){
			try{
				String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + 
						"] satisfies $partid/ParticipantObjectDescription";

				switch (partid.getParticipantObjectDescription().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectDescription is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectDescription has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies not($partid/ParticipantObjectDescription)";
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/ParticipantObjectDescription is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
					e.printStackTrace();
			}
		}
	}

	protected void validateParticipantObjectDescriptionValue(String document, List<Notification> diagnostic,
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher) {
		if (partid != null && partid.getParticipantObjectDescription() != null && partid.getParticipantObjectDescription().getValue() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/ParticipantObjectDescription/text()) or $partid/ParticipantObjectDescription/text()='" + 
					partid.getParticipantObjectDescription().getValue() + "'";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/ParticipantObjectDescription/text() must be '" + partid.getParticipantObjectDescription().getValue() + 
					"' if /" + partid.getName() + "/ParticipantObjectDescription present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateParticipantObjectDescriptionRegex(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getParticipantObjectDescription() != null && partid.getParticipantObjectDescription().getRegex() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/ParticipantObjectDescription/text()) or matches($partid/ParticipantObjectDescription/text(),'" + 
					partid.getParticipantObjectDescription().getRegex() + "')";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/ParticipantObjectDescription/text() must have this form '" + partid.getParticipantObjectDescription().getRegex() + 
					"' if " + partid.getName() +  "/ParticipantObjectDescription present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateNumberOfInstancesOpt(String document, List<Notification> diagnostic,
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher) {
		if (partid != null && partid.getNumberOfInstances() != null && partid.getNumberOfInstances().getOptionality() != null){
			try{
				String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + 
						"] satisfies $partid/SOPClass/@NumberOfInstances";

				switch (partid.getNumberOfInstances().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/SOPClass/@NumberOfInstances is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/SOPClass/@NumberOfInstances has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies not($partid/SOPClass/@NumberOfInstances)";
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/SOPClass/@NumberOfInstances is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
					e.printStackTrace();
			}
		}
	}

	protected void validateNumberOfInstancesValue(String document, List<Notification> diagnostic,
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher) {
		if (partid != null && partid.getNumberOfInstances() != null && partid.getNumberOfInstances().getValue() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/SOPClass/@NumberOfInstances) or $partid/SOPClass/@NumberOfInstances='" + 
					partid.getNumberOfInstances().getValue() + "'";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/SOPClass/@NumberOfInstances must be '" + partid.getNumberOfInstances().getValue() + 
					"' if /" + partid.getName() + "/SOPClass/@NumberOfInstances present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateNumberOfInstancesRegex(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getNumberOfInstances() != null && partid.getNumberOfInstances().getRegex() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/SOPClass/@NumberOfInstances) or matches($partid/SOPClass/@NumberOfInstances,'" + 
					partid.getNumberOfInstances().getRegex() + "')";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/SOPClass/@NumberOfInstances must have this form '" + partid.getNumberOfInstances().getRegex() + 
					"' if " + partid.getName() +  "/SOPClass/@NumberOfInstances present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateInstanceOpt(String document, List<Notification> diagnostic,
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher) {
		if (partid != null && partid.getInstance() != null && partid.getInstance().getOptionality() != null){
			try{
				String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + 
						"] satisfies $partid/SOPClass/Instance";

				switch (partid.getInstance().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/SOPClass/Instance is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/SOPClass/Instance has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies not($partid/SOPClass/Instance)";
					ValidatorUtil.handleXpath(xpath, partid.getName() + "/SOPClass/Instance is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
					e.printStackTrace();
			}
		}
	}

	protected void validateInstanceValue(String document, List<Notification> diagnostic,
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher) {
		if (partid != null && partid.getInstance() != null && partid.getInstance().getValue() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/SOPClass/Instance/@UID) or $partid/SOPClass/Instance/@UID='" + 
					partid.getInstance().getValue() + "'";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/SOPClass/Instance/@UID must be '" + partid.getInstance().getValue() + 
					"' if /" + partid.getName() + "/SOPClass/Instance/@UID present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateInstanceRegex(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		if (partid != null && partid.getInstance() != null && partid.getInstance().getRegex() != null){
			String xpath = "every $partid in /AuditMessage/ParticipantObjectIdentification[" + partidDistinguisher + "] satisfies " +
					"not($partid/SOPClass/Instance/@UID) or matches($partid/SOPClass/Instance/@UID,'" + 
					partid.getInstance().getRegex() + "')";
			ValidatorUtil.handleXpath(xpath, 
					partid.getName() + "/SOPClass/Instance/@UID must have this form '" + partid.getInstance().getRegex() + 
					"' if " + partid.getName() +  "/SOPClass/Instance/@UID present" ,
					ERROR, document, diagnostic);
		}
	}
	

	
	private void validateParticipantObjectDetailValue(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		// TODO
	}
	
	private void validateParticipantObjectDetailRegex(String document, List<Notification> diagnostic, 
			ParticipantObjectIdentificationDesc partid, String partidDistinguisher){
		// TODO
	}
	
}
