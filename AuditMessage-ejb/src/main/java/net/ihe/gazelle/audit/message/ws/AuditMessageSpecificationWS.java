/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.audit.message.ws;

import net.ihe.gazelle.audit.message.action.AuditMessageZipper;
import net.ihe.gazelle.audit.message.desc.AuditMessageSpecification;
import net.ihe.gazelle.audit.message.desc.AuditMessageSpecificationQuery;
import net.ihe.gazelle.audit.message.utils.AuditMessagePreferences;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.annotations.Name;

import javax.ejb.Stateless;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * Created by cel on 08/03/17.
 */
@Stateless
@Name("auditMessageSpecificationWS")
public class AuditMessageSpecificationWS implements AuditMessageSpecificationWSApi {

    private static final String ARCHIVE_PREFIX = "ams-";
    private static final String ARCHIVE_EXTENSION = "zip";

    @Override
    public Response getAuditMessageSpecificationArchive() {
        List<AuditMessageSpecification> specs = getListAuditMessageSpecifications();
        File archive;
        try {
            archive = AuditMessageZipper.zip(specs);
        } catch (JAXBException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .header("Error", "Unable to transform specifications into XML file: " + e.getMessage()).build();
        } catch (IOException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .header("Error", "Unexpected error while creating ZIP file: " + e.getMessage()).build();
        }

        String archiveName = ARCHIVE_PREFIX + AuditMessagePreferences.getAuditMessageRootOID() + ARCHIVE_EXTENSION;
        Response.ResponseBuilder response = Response.ok(archive);
        response.type("application/zip");
        response.header("Content-Disposition", "attachment; filename=\"" + archiveName + "\"");
        return response.build();
    }

    private static List<AuditMessageSpecification> getListAuditMessageSpecifications() {
        AuditMessageSpecificationQuery query = new AuditMessageSpecificationQuery(
                EntityManagerService.provideEntityManager());
        List<AuditMessageSpecification> res = query.getList();
        if (res != null) {
            Collections.sort(res);
        }
        return res;
    }

}
