/*
 *   Copyright 2010-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package net.ihe.gazelle.audit.message.action;

import net.ihe.gazelle.audit.message.desc.ActiveParticipantDescription;
import net.ihe.gazelle.audit.message.desc.AuditMessageSpecification;
import net.ihe.gazelle.audit.message.desc.AuditSourceDescription;
import net.ihe.gazelle.audit.message.desc.ConstraintSpecification;
import net.ihe.gazelle.audit.message.desc.ElementDescription;
import net.ihe.gazelle.audit.message.desc.EventIdentificationDesc;
import net.ihe.gazelle.audit.message.desc.Optionality;
import net.ihe.gazelle.audit.message.desc.ParticipantObjectIdentificationDesc;
import net.ihe.gazelle.audit.message.utils.ConfigurationException;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.international.StatusMessage.Severity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Abderrazek Boufahja, Cédric Eoche-Duval
 */

@Name("auditMessageEditor")
@Scope(ScopeType.PAGE)
public class AuditMessageEditor extends AuditMessageBrowser implements Serializable, UserAttributeCommon {

    private static final long serialVersionUID = 1L;
    private static final String UTF8_NAME = "UTF8";
    private static final Charset CHARSET_UTF8 = Charset.forName(UTF8_NAME);

    private static final Logger LOG = LoggerFactory.getLogger(AuditMessageEditor.class);

    private Boolean createMode = false;

    @In(create = true)
    private AuditMessageOIDProviderLocal auditMessageOIDProvider;

    @In(value = "gumUserService")
    private UserService userService;

    private AuditMessageSpecification selectedAuditMessageSpecification;

    private ConstraintSpecification selectedExtraConstraint;

    private ActiveParticipantDescription selectedActiveParticipantDesc;

    private ParticipantObjectIdentificationDesc selectedParticipantObjectIdentificationDesc;

    private List<String> listIHEAuditContents = Arrays.asList(IHEAuditContent.EVENT_IDENTIFICATION.getValue(),
            IHEAuditContent.AUDIT_SOURCE_IDENTIFICATION.getValue());

    private List<String> listAllIHEAuditContents;

    private String selectedOptionalPI;
    private String selectedOptionalAP;
    private String selectedOptionalEV;
    private Object selectedOptionalAS;

    public Object getSelectedOptionalAS() {
        return selectedOptionalAS;
    }

    public void setSelectedOptionalAS(Object selectedOptionalAS) {
        this.selectedOptionalAS = selectedOptionalAS;
    }

    public String getSelectedOptionalEV() {
        return selectedOptionalEV;
    }

    public void setSelectedOptionalEV(String selectedOptionalEV) {
        this.selectedOptionalEV = selectedOptionalEV;
    }

    public String getSelectedOptionalAP() {
        return selectedOptionalAP;
    }

    public void setSelectedOptionalAP(String selectedOptionalAP) {
        this.selectedOptionalAP = selectedOptionalAP;
    }

    public ActiveParticipantDescription getSelectedActiveParticipantDesc() {
        return selectedActiveParticipantDesc;
    }

    public void setSelectedActiveParticipantDesc(ActiveParticipantDescription selectedActiveParticipantDesc) {
        this.selectedActiveParticipantDesc = selectedActiveParticipantDesc;
    }

    public String getSelectedOptionalPI() {
        return selectedOptionalPI;
    }

    public void setSelectedOptionalPI(String selectedOptionalPI) {
        this.selectedOptionalPI = selectedOptionalPI;
    }

    public ConstraintSpecification getSelectedExtraConstraint() {
        return selectedExtraConstraint;
    }

    public void setSelectedExtraConstraint(ConstraintSpecification selectedExtraConstraint) {
        this.selectedExtraConstraint = selectedExtraConstraint;
    }

    public ParticipantObjectIdentificationDesc getSelectedParticipantObjectIdentificationDesc() {
        return selectedParticipantObjectIdentificationDesc;
    }

    public void setSelectedParticipantObjectIdentificationDesc(
            ParticipantObjectIdentificationDesc selectedParticipantObjectIdentificationDesc) {
        this.selectedParticipantObjectIdentificationDesc = selectedParticipantObjectIdentificationDesc;
    }

    public List<String> getListAllIHEAuditContents() {
        if (listAllIHEAuditContents == null) {
            listAllIHEAuditContents = new ArrayList<String>();
            for (IHEAuditContent ii : IHEAuditContent.values()) {
                listAllIHEAuditContents.add(ii.getValue());
            }
        }
        return listAllIHEAuditContents;
    }

    public List<String> getListIHEAuditContents() {
        return listIHEAuditContents;
    }

    public void setListIHEAuditContents(List<String> listIHEAuditContents) {
        this.listIHEAuditContents = listIHEAuditContents;
    }

    public Boolean isCreateMode() {
        return createMode;
    }

    public void setCreateMode(Boolean createMode) {
        this.createMode = createMode;
    }

    public AuditMessageSpecification getSelectedAuditMessageSpecification() {
        return selectedAuditMessageSpecification;
    }

    public void setSelectedAuditMessageSpecification(AuditMessageSpecification selectedAuditMessageSpecification) {
        this.selectedAuditMessageSpecification = selectedAuditMessageSpecification;
    }

    @Create
    @Restrict("#{s:hasRole('admin_role') or s:hasRole('tests_editor_role')}")
    public void initAuditMessageFromParam() {
        String idString = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
        if (idString != null && !idString.isEmpty()) {
            createMode = false;
            try {
                Integer id = Integer.parseInt(idString);
                loadSelectedAuditMessage(id);
            } catch (NumberFormatException e) {
                String msg = "Failed to load audit message specification, '" + idString + "' is not an Id.";
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg, e);
                LOG.error(msg, e);
            }
        } else {
            createMode = true;
            selectedAuditMessageSpecification = new AuditMessageSpecification();
        }
    }

    public void initNewAuditMessageSpecification() {
        if (listIHEAuditContents != null) {
            if (listIHEAuditContents.contains(IHEAuditContent.AUDIT_SOURCE_IDENTIFICATION.getValue())) {
                selectedAuditMessageSpecification.setAuditSource(initAuditSourceDescription());
            }
            //TODO else throw error, at least one auditSourceIdentification is required
            if (listIHEAuditContents.contains(IHEAuditContent.EVENT_IDENTIFICATION.getValue())) {
                selectedAuditMessageSpecification.setEvent(initIHEEventIdentificationDesc());
            }
            //TODO else throw error, exactly one EventIdentification is required
            if (listIHEAuditContents.contains(IHEAuditContent.SOURCE.getValue())) {
                selectedAuditMessageSpecification.getActiveParticipants().add(returnSourceParticipant());
            }
            if (listIHEAuditContents.contains(IHEAuditContent.DESTINATION.getValue())) {
                selectedAuditMessageSpecification.getActiveParticipants().add(returnDestinationParticipant());
            }
            if (listIHEAuditContents.contains(IHEAuditContent.HUMAN_REQUESTOR.getValue())) {
                selectedAuditMessageSpecification.getActiveParticipants().add(returnHumanRequestorParticipant());
            }
            //TODO else throw error, at least one ActiveParticipant is required (Source, Destination, HumanRequestor or default ActiveParticipant)
        }
        createMode = false;
    }

    public List<Optionality> getOptionalityValues() {
        return Arrays.asList(Optionality.values());
    }

    public List<String> getCardinalityValues() {
        List<String> ls = new ArrayList<String>();
        ls.add("0..1");
        ls.add("0..*");
        ls.add("1..1");
        ls.add("1..*");
        return ls;
    }

    public ActiveParticipantDescription getSourceOfSelectedAuditMessageSpecification() {
        return IHEAuditMessageUtil.getSourceOfAuditMessageSpecification(selectedAuditMessageSpecification);
    }

    public ActiveParticipantDescription getHrOfSelectedAuditMessageSpecification() {
        return IHEAuditMessageUtil.getHrOfAuditMessageSpecification(selectedAuditMessageSpecification);
    }

    public ActiveParticipantDescription getDestinationOfSelectedAuditMessageSpecification() {
        return IHEAuditMessageUtil.getDestinationOfAuditMessageSpecification(selectedAuditMessageSpecification);
    }

    public ParticipantObjectIdentificationDesc getPatientOfSelectedAuditMessageSpecification() {
        return IHEAuditMessageUtil.getPatientOfAuditMessageSpecification(selectedAuditMessageSpecification);
    }

    public ParticipantObjectIdentificationDesc getQueryOfSelectedAuditMessageSpecification() {
        return IHEAuditMessageUtil.getQueryOfAuditMessageSpecification(selectedAuditMessageSpecification);
    }

    public void addNewParticipantIdentification() {
        ParticipantObjectIdentificationDesc poid = initParticipantObjectIdentificationDesc();
        this.selectedAuditMessageSpecification.getParticipantObjectIdentifications().add(poid);
    }

    public void addPatientToSelectedAuditMessageSpecification() {
        this.selectedAuditMessageSpecification.getParticipantObjectIdentifications().add(
                this.returnPatientParticipant());
    }

    public void addNewActiveParticipant() {
        ActiveParticipantDescription act = initActiveParticipantDescription();
        this.selectedAuditMessageSpecification.getActiveParticipants().add(act);
    }

    public void addSource() {
        ActiveParticipantDescription act = returnSourceParticipant();
        this.selectedAuditMessageSpecification.getActiveParticipants().add(act);
    }

    public void addDestination() {
        ActiveParticipantDescription act = returnDestinationParticipant();
        this.selectedAuditMessageSpecification.getActiveParticipants().add(act);
    }

    public void addHumanRequestor() {
        ActiveParticipantDescription act = returnHumanRequestorParticipant();
        this.selectedAuditMessageSpecification.getActiveParticipants().add(act);
    }

    public void addAuditSourceIdentification() {
        if (selectedAuditMessageSpecification.getAuditSource() == null) {
            selectedAuditMessageSpecification.setAuditSource(initAuditSourceDescription());
        }
    }

    public void addEventIdentification() {
        if (selectedAuditMessageSpecification.getEvent() == null) {
            selectedAuditMessageSpecification.setEvent(initIHEEventIdentificationDesc());
        }
    }

    public void initSelectedExtraConstraint() {
        this.selectedExtraConstraint = new ConstraintSpecification();
    }

    public void saveSelectedExtraConstraint() {
        if (selectedExtraConstraint.getId() != null) {
            return;
        } else {
            if (selectedAuditMessageSpecification != null) {
                if (!selectedAuditMessageSpecification.getExtraConstraintSpecifications().contains(selectedExtraConstraint)) {
                    selectedAuditMessageSpecification.getExtraConstraintSpecifications().add(selectedExtraConstraint);
                }
            }
        }
    }

    public List<String> optionalPI() {
        List<String> res = new ArrayList<String>();
        if (selectedParticipantObjectIdentificationDesc != null) {
            if (selectedParticipantObjectIdentificationDesc.getMpps() == null) {
                res.add("MPPS");
            }
            if (selectedParticipantObjectIdentificationDesc.getAccession() == null) {
                res.add("Accession");
            }
            if (selectedParticipantObjectIdentificationDesc.getSopClass() == null) {
                res.add("SOPClass");
            }
            if (selectedParticipantObjectIdentificationDesc.getParticipantObjectContainsStudy() == null) {
                res.add("ParticipantObjectContainsStudy");
            }
            if (selectedParticipantObjectIdentificationDesc.getEncrypted() == null) {
                res.add("Encrypted");
            }
            if (selectedParticipantObjectIdentificationDesc.getAnonymized() == null) {
                res.add("Anonymized");
            }
            if (selectedParticipantObjectIdentificationDesc.getParticipantObjectDescription() == null) {
                res.add("ParticipantObjectDescription");
            }
            if (selectedParticipantObjectIdentificationDesc.getInstance() == null) {
                res.add("Instance");
            }
            if (selectedParticipantObjectIdentificationDesc.getNumberOfInstances() == null) {
                res.add("NumberOfInstances");
            }
        }
        return res;
    }

    public void addSelectedExtraDicomPI() {
        if (this.selectedOptionalPI != null) {
            if (this.selectedOptionalPI.equals("MPPS")) {
                this.selectedParticipantObjectIdentificationDesc.setMpps(initElementDescription());
            } else if (this.selectedOptionalPI.equals("Accession")) {
                this.selectedParticipantObjectIdentificationDesc.setAccession(initElementDescription());
            } else if (this.selectedOptionalPI.equals("SOPClass")) {
                this.selectedParticipantObjectIdentificationDesc.setSopClass(initElementDescription());
            } else if (this.selectedOptionalPI.equals("ParticipantObjectContainsStudy")) {
                this.selectedParticipantObjectIdentificationDesc
                        .setParticipantObjectContainsStudy(initElementDescription());
            } else if (this.selectedOptionalPI.equals("Encrypted")) {
                this.selectedParticipantObjectIdentificationDesc.setEncrypted(initElementDescription());
            } else if (this.selectedOptionalPI.equals("Anonymized")) {
                this.selectedParticipantObjectIdentificationDesc.setAnonymized(initElementDescription());
            } else if (this.selectedOptionalPI.equals("ParticipantObjectDescription")) {
                this.selectedParticipantObjectIdentificationDesc
                        .setParticipantObjectDescription(initElementDescription());
            } else if (this.selectedOptionalPI.equals("Instance")) {
                this.selectedParticipantObjectIdentificationDesc.setInstance(initElementDescription());
            } else if (this.selectedOptionalPI.equals("NumberOfInstances")) {
                this.selectedParticipantObjectIdentificationDesc.setNumberOfInstances(initElementDescription());
            }
        }
    }

    public List<String> optionalAP() {
        List<String> res = new ArrayList<String>();
        if (selectedActiveParticipantDesc != null && selectedActiveParticipantDesc.getMediaIdentifier() == null) {
            res.add("MediaIdentifier");
        }
        return res;
    }

    public void addSelectedExtraDicomAP() {
        if (this.selectedOptionalAP != null && this.selectedOptionalAP.equals("MediaIdentifier")) {
            this.selectedActiveParticipantDesc.setMediaIdentifier(initElementDescription());
        }
    }

    public List<String> optionalEV() {
        List<String> res = new ArrayList<String>();
        if (selectedAuditMessageSpecification != null && selectedAuditMessageSpecification.getEvent() != null) {
            if (selectedAuditMessageSpecification.getEvent().getEventOutcomeDescription() == null) {
                res.add("EventOutcomeDescription");
            }
            if (selectedAuditMessageSpecification.getEvent().getPurposeOfUse() == null) {
                res.add("PurposeOfUse");
            }
        }
        return res;
    }

    public void addSelectedExtraDicomEV() {
        if (this.selectedOptionalEV != null) {
            if (this.selectedOptionalEV.equals("EventOutcomeDescription")) {
                selectedAuditMessageSpecification.getEvent().setEventOutcomeDescription(initElementDescription());
            } else if (this.selectedOptionalEV.equals("PurposeOfUse")) {
                selectedAuditMessageSpecification.getEvent().setPurposeOfUse(initElementDescription());
            }
        }
    }

    public List<String> optionalAS() {
        List<String> res = new ArrayList<String>();
        if (selectedAuditMessageSpecification != null &&
                selectedAuditMessageSpecification.getAuditSource() != null &&
                selectedAuditMessageSpecification.getAuditSource().getAuditSourceCodeValue() == null) {
            res.add("AuditSourceCodeValue");
        }
        return res;
    }

    public void addSelectedExtraDicomAS() {
        if (this.selectedOptionalAS != null && this.selectedOptionalAS.equals("AuditSourceCodeValue")) {
            selectedAuditMessageSpecification.getAuditSource().setAuditSourceCodeValue(initElementDescription());
        }
    }

    public boolean userAbleToDeleteElementDescription(ElementDescription elementDescription) {
        if (this.selectedAuditMessageSpecification != null) {
            if (this.selectedAuditMessageSpecification.getEvent() != null) {
                if (elementDescription == this.selectedAuditMessageSpecification.getEvent()
                        .getEventOutcomeDescription()) {
                    return true;
                }
                if (elementDescription == this.selectedAuditMessageSpecification.getEvent().getPurposeOfUse()) {
                    return true;
                }
            }
            if (this.selectedAuditMessageSpecification.getAuditSource() != null && elementDescription ==
                    this.selectedAuditMessageSpecification.getAuditSource().getAuditSourceCodeValue()) {
                return true;
            }
            if (this.selectedAuditMessageSpecification.getActiveParticipants() != null) {
                for (ActiveParticipantDescription ap : this.selectedAuditMessageSpecification.getActiveParticipants()) {
                    if (elementDescription == ap.getMediaIdentifier()) {
                        return true;
                    }
                }
            }
            if (this.selectedAuditMessageSpecification.getParticipantObjectIdentifications() != null) {
                for (ParticipantObjectIdentificationDesc poi : this.selectedAuditMessageSpecification
                        .getParticipantObjectIdentifications()) {
                    if (elementDescription == poi.getMpps()) {
                        return true;
                    }
                    if (elementDescription == poi.getAccession()) {
                        return true;
                    }
                    if (elementDescription == poi.getSopClass()) {
                        return true;
                    }
                    if (elementDescription == poi.getParticipantObjectContainsStudy()) {
                        return true;
                    }
                    if (elementDescription == poi.getAnonymized()) {
                        return true;
                    }
                    if (elementDescription == poi.getEncrypted()) {
                        return true;
                    }
                    if (elementDescription == poi.getParticipantObjectDescription()) {
                        return true;
                    }
                    if (elementDescription == poi.getNumberOfInstances()) {
                        return true;
                    }
                    if (elementDescription == poi.getInstance()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void removeElementDescription(ElementDescription elementDescription) {
        if (this.selectedAuditMessageSpecification != null) {
            if (this.selectedAuditMessageSpecification.getEvent() != null) {
                if (elementDescription == this.selectedAuditMessageSpecification.getEvent()
                        .getEventOutcomeDescription()) {
                    this.selectedAuditMessageSpecification.getEvent().setEventOutcomeDescription(null);
                }
                if (elementDescription == this.selectedAuditMessageSpecification.getEvent().getPurposeOfUse()) {
                    this.selectedAuditMessageSpecification.getEvent().setPurposeOfUse(null);
                }
            }
            if (this.selectedAuditMessageSpecification.getAuditSource() != null && elementDescription ==
                    this.selectedAuditMessageSpecification.getAuditSource().getAuditSourceCodeValue()) {
                this.selectedAuditMessageSpecification.getAuditSource().setAuditSourceCodeValue(null);
            }
            if (this.selectedAuditMessageSpecification.getActiveParticipants() != null) {
                for (ActiveParticipantDescription ap : this.selectedAuditMessageSpecification.getActiveParticipants()) {
                    if (elementDescription == ap.getMediaIdentifier()) {
                        ap.setMediaIdentifier(null);
                    }
                }
            }
            if (this.selectedAuditMessageSpecification.getParticipantObjectIdentifications() != null) {
                for (ParticipantObjectIdentificationDesc poi : this.selectedAuditMessageSpecification
                        .getParticipantObjectIdentifications()) {
                    if (elementDescription == poi.getMpps()) {
                        poi.setMpps(null);
                    } else if (elementDescription == poi.getAccession()) {
                        poi.setAccession(null);
                    } else if (elementDescription == poi.getSopClass()) {
                        poi.setSopClass(null);
                    } else if (elementDescription == poi.getParticipantObjectContainsStudy()) {
                        poi.setParticipantObjectContainsStudy(null);
                    } else if (elementDescription == poi.getAnonymized()) {
                        poi.setAnonymized(null);
                    } else if (elementDescription == poi.getEncrypted()) {
                        poi.setEncrypted(null);
                    } else if (elementDescription == poi.getParticipantObjectDescription()) {
                        poi.setParticipantObjectDescription(null);
                    } else if (elementDescription == poi.getNumberOfInstances()) {
                        poi.setNumberOfInstances(null);
                    } else if (elementDescription == poi.getInstance()) {
                        poi.setInstance(null);
                    }
                }
            }
        }
    }

    public String saveAndLeave() {
        saveSelectedAuditMessageSpecification();
        return navigateListAuditMessage();
    }

    public void saveSelectedAuditMessageSpecification() {
        try {
            selectedAuditMessageSpecification = saveAuditMessageSpecification(selectedAuditMessageSpecification);
            FacesMessages.instance().add(Severity.INFO, "Audit message specification saved");
        } catch (ConfigurationException e) {
            FacesMessages.instance().add(Severity.ERROR, "Fail to save Audit message specification: " + e.getMessage());
            LOG.error("Fail to save Audit message specification: " + e.getMessage());
        }

    }

    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }

    private AuditMessageSpecification saveAuditMessageSpecification(AuditMessageSpecification ams) throws ConfigurationException {
        if (ams != null) {
            if (ams.getOid() == null || ams.getOid().isEmpty()) {
                ams.setOid(auditMessageOIDProvider.generateNextOid());
            }
            ams.recordChange();

            EntityManager em = EntityManagerService.provideEntityManager();
            ams = em.merge(ams);
        }
        return ams;
    }

    private void loadSelectedAuditMessage(Integer id) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        setSelectedAuditMessageSpecification(entityManager.find(AuditMessageSpecification.class, id));
    }

    private EventIdentificationDesc initIHEEventIdentificationDesc() {
        EventIdentificationDesc apd = new EventIdentificationDesc();
        apd.setEventID(initElementDescription());
        apd.setEventActionCode(initElementDescription());
        apd.setEventDateTime(initElementDescription());
        apd.setEventOutcomeIndicator(initElementDescription());
        apd.setEventTypeCode(initElementDescription());
        apd.getEventID().setOptionality(Optionality.M);
        apd.getEventActionCode().setOptionality(Optionality.M);
        apd.getEventDateTime().setOptionality(Optionality.M);
        apd.getEventOutcomeIndicator().setOptionality(Optionality.M);
        apd.getEventTypeCode().setOptionality(Optionality.M);
        apd.getEventID().setValue("EV('110112', 'DCM', 'Query')");
        apd.getEventActionCode().setValue("E");
        return apd;
    }

    private AuditSourceDescription initAuditSourceDescription() {
        AuditSourceDescription apd = new AuditSourceDescription();
        apd.setMin("1");
        apd.setMax("1");
        apd.setAuditEnterpriseSiteID(initElementDescription());
        apd.setAuditSourceID(initElementDescription());
        apd.setAuditSourceTypeCode(initElementDescription());
        apd.setAuditSourceCodeValue(initElementDescription());
        apd.getAuditEnterpriseSiteID().setOptionality(Optionality.U);
        apd.getAuditSourceID().setOptionality(Optionality.U);
        apd.getAuditSourceTypeCode().setOptionality(Optionality.U);
        apd.getAuditSourceCodeValue().setOptionality(Optionality.U);
        return apd;
    }

    private ActiveParticipantDescription returnSourceParticipant() {
        ActiveParticipantDescription apd = initActiveParticipantDescription();
        apd.setName("Source");
        apd.setMin("1");
        apd.setMax("1");
        apd.getUserIsRequestor().setOptionality(Optionality.M);
        apd.getUserIsRequestor().setValue("true");
        apd.getRoleIDCode().setOptionality(Optionality.M);
        apd.getRoleIDCode().setValue("EV('110153', 'DCM', 'Source')");
        apd.getNetworkAccessPointID().setOptionality(Optionality.M);
        apd.getNetworkAccessPointTypeCode().setOptionality(Optionality.M);
        apd.getNetworkAccessPointTypeCode().setRegex("^1|2$");
        apd.setDistinguisher("RoleIDCode[@code='110153']");
        return apd;
    }

    private ActiveParticipantDescription returnDestinationParticipant() {
        ActiveParticipantDescription apd = initActiveParticipantDescription();
        apd.setName("Destination");
        apd.setMin("1");
        apd.setMax("1");
        apd.getUserIsRequestor().setOptionality(Optionality.M);
        apd.getUserIsRequestor().setValue("false");
        apd.getRoleIDCode().setOptionality(Optionality.M);
        apd.getRoleIDCode().setValue("EV('110152', 'DCM', 'Destination')");
        apd.getNetworkAccessPointID().setOptionality(Optionality.M);
        apd.getNetworkAccessPointTypeCode().setOptionality(Optionality.M);
        apd.getNetworkAccessPointTypeCode().setRegex("^1|2$");
        apd.setDistinguisher("RoleIDCode[@code='110152']");
        return apd;
    }

    private ActiveParticipantDescription returnHumanRequestorParticipant() {
        ActiveParticipantDescription apd = initActiveParticipantDescription();
        apd.setName("HumanRequestor");
        apd.setMin("0");
        apd.setMax("*");
        apd.getUserID().setOptionality(Optionality.M);
        apd.getUserIsRequestor().setOptionality(Optionality.M);
        apd.getUserIsRequestor().setValue("true");
        apd.getNetworkAccessPointID().setOptionality(Optionality.NA);
        apd.getNetworkAccessPointTypeCode().setOptionality(Optionality.NA);
        apd.setDistinguisher("not(RoleIDCode) or RoleIDCode[@code!='110153' and @code!='110152']");
        return apd;
    }

    private ParticipantObjectIdentificationDesc returnPatientParticipant() {
        ParticipantObjectIdentificationDesc apd = initParticipantObjectIdentificationDesc();
        apd.setName("Patient");
        apd.setMin("0");
        apd.setMax("1");
        apd.getParticipantObjectTypeCode().setOptionality(Optionality.M);
        apd.getParticipantObjectTypeCode().setValue("1");
        apd.getParticipantObjectTypeCodeRole().setOptionality(Optionality.M);
        apd.getParticipantObjectTypeCodeRole().setValue("1");
        apd.getParticipantObjectDataLifeCycle().setOptionality(Optionality.U);
        apd.getParticipantObjectIDTypeCode().setOptionality(Optionality.M);
        apd.getParticipantObjectIDTypeCode().setValue("EV('2','RFC-3881','Patient Number')");
        apd.getParticipantObjectSensitivity().setOptionality(Optionality.U);
        apd.getParticipantObjectID().setOptionality(Optionality.M);
        apd.getParticipantObjectID().setRegex("^.+?\\^\\^\\^.*?&amp;.+?&amp;ISO$");
        apd.getParticipantObjectName().setOptionality(Optionality.U);
        apd.getParticipantObjectQuery().setOptionality(Optionality.U);
        apd.getParticipantObjectDetail().setOptionality(Optionality.U);
        return apd;
    }

    private ActiveParticipantDescription initActiveParticipantDescription() {
        ActiveParticipantDescription apd = new ActiveParticipantDescription();
        apd.setMin("0");
        apd.setMax("1");
        apd.setUserID(initElementDescription());
        apd.setAlternativeUserID(initElementDescription());
        apd.setUserName(initElementDescription());
        apd.setUserIsRequestor(initElementDescription());
        apd.setRoleIDCode(initElementDescription());
        apd.setNetworkAccessPointID(initElementDescription());
        apd.setNetworkAccessPointTypeCode(initElementDescription());
        return apd;
    }

    private ParticipantObjectIdentificationDesc initParticipantObjectIdentificationDesc() {
        ParticipantObjectIdentificationDesc apd = new ParticipantObjectIdentificationDesc();
        apd.setMin("0");
        apd.setMax("1");
        apd.setParticipantObjectDataLifeCycle(initElementDescription());
        apd.setParticipantObjectDetail(initElementDescription());
        apd.setParticipantObjectID(initElementDescription());
        apd.setParticipantObjectIDTypeCode(initElementDescription());
        apd.setParticipantObjectName(initElementDescription());
        apd.setParticipantObjectQuery(initElementDescription());
        apd.setParticipantObjectSensitivity(initElementDescription());
        apd.setParticipantObjectTypeCode(initElementDescription());
        apd.setParticipantObjectTypeCodeRole(initElementDescription());
        return apd;
    }

    private ElementDescription initElementDescription() {
        ElementDescription ed = new ElementDescription();
        ed.setOptionality(Optionality.NA);
        return ed;
    }


}
