/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.audit.message.menu;

import net.ihe.gazelle.audit.message.utils.AuditMessagePreferences;
import net.ihe.gazelle.common.pages.Authorization;
import org.jboss.seam.security.Identity;

public enum AuditMessageAuthorizations implements Authorization {
	
	ATNA,
	ADMIN,
	AM_EDITOR;

	@Override
	public boolean isGranted(Object... context) {
        Boolean isAtna = AuditMessagePreferences.isAtnaModeEnabled();
        switch (this) {
		case ATNA:
			return (isAtna != null && isAtna);
		case ADMIN:
			return Identity.instance().hasRole("admin_role");
		case AM_EDITOR:
			return Identity.instance().hasRole("tests_editor_role") || Identity.instance().hasRole("admin_role");
		default:
			return false;
		}
	}

}
