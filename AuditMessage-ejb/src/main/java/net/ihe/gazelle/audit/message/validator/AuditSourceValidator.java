/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.audit.message.validator;

import net.ihe.gazelle.audit.message.desc.AuditMessageSpecification;
import net.ihe.gazelle.validation.Notification;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AuditSourceValidator implements AuditValidatorInterface {
	
	private static final String WARNING = "warning";
	private static final String ERROR = "error";

	protected AuditSourceValidator(){}
	
	public static final AuditSourceValidator instance = new AuditSourceValidator();

	@Override
	public void validate(String document, AuditMessageSpecification ams,
			List<Notification> diagnostic) {
		this.validateAuditEnterpriseSiteIDOpt(document, ams, diagnostic);
		this.validateAuditEnterpriseSiteIDRegex(document, ams, diagnostic);
		this.validateAuditEnterpriseSiteIDValue(document, ams, diagnostic);
		this.validateAuditSourceIDOpt(document, ams, diagnostic);
		this.validateAuditSourceIDRegex(document, ams, diagnostic);
		this.validateAuditSourceIDValue(document, ams, diagnostic);
		this.validateAuditSourceTypeCodeOpt(document, ams, diagnostic);
		this.validateAuditSourceTypeCodeValue(document, ams, diagnostic);
		this.validateAuditSourceCodeValueOpt(document, ams, diagnostic);
		this.validateAuditSourceCodeValueValue(document, ams, diagnostic);
	}
	
	protected void validateAuditSourceIDOpt(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getAuditSource() != null && ams.getAuditSource().getAuditSourceID() != null 
				&& ams.getAuditSource().getAuditSourceID().getOptionality() != null){

			try{
				String xpath = "every $as in /AuditMessage/AuditSourceIdentification satisfies $as/@AuditSourceID";

				switch (ams.getAuditSource().getAuditSourceID().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, "AuditSource/@AuditSourceID is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, "AuditSource/@AuditSourceID has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "count(/AuditMessage/AuditSourceIdentification/@AuditSourceID)=0";
					ValidatorUtil.handleXpath(xpath, "AuditSource/@AuditSourceID is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	protected void validateAuditSourceIDValue(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getAuditSource() != null && ams.getAuditSource().getAuditSourceID() != null 
				&& ams.getAuditSource().getAuditSourceID().getValue() != null){
			String xpath = "every $as in /AuditMessage/AuditSourceIdentification satisfies not($as/@AuditSourceID) or $as/@AuditSourceID='" + 
				ams.getAuditSource().getAuditSourceID().getValue() + "'";
			ValidatorUtil.handleXpath(xpath, 
					"AuditSource/@AuditSourceID must be '" + ams.getAuditSource().getAuditSourceID().getValue() + "' if AuditSource/@AuditSourceID present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateAuditSourceIDRegex(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getAuditSource() != null && ams.getAuditSource().getAuditSourceID() != null 
				&& ams.getAuditSource().getAuditSourceID().getRegex() != null){
			String xpath = "every $as in /AuditMessage/AuditSourceIdentification satisfies not($as/@AuditSourceID) or matches($as/@AuditSourceID,'" + 
					 ams.getAuditSource().getAuditSourceID().getRegex() + "')";
			ValidatorUtil.handleXpath(xpath, 
					"AuditSource/@AuditSourceID must have this form : '" +  ams.getAuditSource().getAuditSourceID().getRegex() + 
					"' if AuditSource/@AuditSourceID present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateAuditEnterpriseSiteIDOpt(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getAuditSource() != null && ams.getAuditSource().getAuditEnterpriseSiteID() != null 
				&& ams.getAuditSource().getAuditEnterpriseSiteID().getOptionality() != null){

			try{
				String xpath = "every $as in /AuditMessage/AuditSourceIdentification satisfies $as/@AuditEnterpriseSiteID";

				switch (ams.getAuditSource().getAuditEnterpriseSiteID().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, "AuditSource/@AuditEnterpriseSiteID is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, "AuditSource/@AuditEnterpriseSiteID has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "count(/AuditMessage/AuditSourceIdentification/@AuditEnterpriseSiteID)=0";
					ValidatorUtil.handleXpath(xpath, "AuditSource/@AuditEnterpriseSiteID is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	protected void validateAuditEnterpriseSiteIDValue(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getAuditSource() != null && ams.getAuditSource().getAuditEnterpriseSiteID() != null 
				&& ams.getAuditSource().getAuditEnterpriseSiteID().getValue() != null){
			String xpath = "every $as in /AuditMessage/AuditSourceIdentification satisfies not($as/@AuditEnterpriseSiteID) or $as/@AuditEnterpriseSiteID='" + 
				ams.getAuditSource().getAuditEnterpriseSiteID().getValue() + "'";
			ValidatorUtil.handleXpath(xpath, 
					"AuditSource/@AuditEnterpriseSiteID must be '" + ams.getAuditSource().getAuditEnterpriseSiteID().getValue() + 
					"' if AuditSource/@AuditEnterpriseSiteID present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateAuditEnterpriseSiteIDRegex(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getAuditSource() != null && ams.getAuditSource().getAuditEnterpriseSiteID() != null 
				&& ams.getAuditSource().getAuditEnterpriseSiteID().getRegex() != null){
			String xpath = "every $as in /AuditMessage/AuditSourceIdentification satisfies not($as/@AuditEnterpriseSiteID) or matches($as/@AuditEnterpriseSiteID,'" + 
					 ams.getAuditSource().getAuditEnterpriseSiteID().getRegex() + "')";
			ValidatorUtil.handleXpath(xpath, 
					"AuditSource/@AuditEnterpriseSiteID must have this form : '" +  ams.getAuditSource().getAuditEnterpriseSiteID().getRegex() + 
					"' if AuditSource/@AuditEnterpriseSiteID present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateAuditSourceTypeCodeOpt(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getAuditSource() != null && ams.getAuditSource().getAuditSourceTypeCode() != null 
				&& ams.getAuditSource().getAuditSourceTypeCode().getOptionality() != null){

			try{
				String xpath = "every $as in /AuditMessage/AuditSourceIdentification satisfies $as/AuditSourceTypeCode";

				switch (ams.getAuditSource().getAuditSourceTypeCode().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, "AuditSource/AuditSourceTypeCode is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, "AuditSource/AuditSourceTypeCode has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "count(/AuditMessage/AuditSourceIdentification/AuditSourceTypeCode)=0";
					ValidatorUtil.handleXpath(xpath, "AuditSource/AuditSourceTypeCode is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	protected void validateAuditSourceTypeCodeValue(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getAuditSource() != null && ams.getAuditSource().getAuditSourceTypeCode() != null 
				&& ams.getAuditSource().getAuditSourceTypeCode().getValue() != null){
			try{
				String val = ams.getAuditSource().getAuditSourceTypeCode().getValue();
				Pattern pat = Pattern.compile("^\\s*?EV\\s*?\\(\\s*?'(.*?)'\\s*?,\\s*?'(.*?)'\\s*?,\\s*?'(.*?)'\\s*?\\)\\s*?$");
				Matcher matcher = pat.matcher(val);
				if (matcher.find()){
					this.validateAuditSourceTypeCodeCodeValue(matcher.group(1), document , ams, diagnostic);
					this.validateAuditSourceTypeCodeCodeSystemNameValue(matcher.group(2), document , ams, diagnostic);
					this.validateAuditSourceTypeCodeDisplayNameValue(matcher.group(3), document , ams, diagnostic);
				}
				else{
					if (ams.getIsDicomCompatible() != null && ams.getIsDicomCompatibleAsBoolean()){
						this.validateAuditSourceTypeCodeValueNotDicom(document , ams, diagnostic);
						this.validateAuditSourceTypeCodeRegexNotDicom(document , ams, diagnostic);
					}
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	private void validateAuditSourceTypeCodeRegexNotDicom(String document, AuditMessageSpecification ams,
			List<Notification> diagnostic) {
		if (ams != null && ams.getAuditSource() != null && ams.getAuditSource().getAuditSourceTypeCode() != null 
				&& ams.getAuditSource().getAuditSourceTypeCode().getRegex() != null){
			String xpath = "every $as in /AuditMessage/AuditSourceIdentification satisfies not($as/AuditSourceTypeCode) or matches($as/AuditSourceTypeCode/text(),'" + 
					 ams.getAuditSource().getAuditSourceTypeCode().getRegex() + "')";
			ValidatorUtil.handleXpath(xpath, 
					"AuditSource/AuditSourceTypeCode must have this form : '" +  ams.getAuditSource().getAuditSourceTypeCode().getRegex() + 
					"' if AuditSource/AuditSourceTypeCode present" ,
					ERROR, document, diagnostic);
		}
		
	}

	private void validateAuditSourceTypeCodeValueNotDicom(String document, AuditMessageSpecification ams,
			List<Notification> diagnostic) {
		if (ams != null && ams.getAuditSource() != null && ams.getAuditSource().getAuditSourceTypeCode() != null 
				&& ams.getAuditSource().getAuditSourceTypeCode().getValue() != null){
			String xpath = "every $as in /AuditMessage/AuditSourceIdentification satisfies not($as/AuditSourceTypeCode) or $as/AuditSourceTypeCode/text()='" + 
				ams.getAuditSource().getAuditSourceTypeCode().getValue() + "'";
			ValidatorUtil.handleXpath(xpath, 
					"AuditSource/AuditSourceTypeCode must be '" + ams.getAuditSource().getAuditSourceTypeCode().getValue() + 
					"' if AuditSource/AuditSourceTypeCode present" ,
					ERROR, document, diagnostic);
		}
		
	}

	protected void validateAuditSourceTypeCodeCodeValue(String code, String document, AuditMessageSpecification ams, List<Notification> diagnostic) throws Exception{
		String xpath = "every $as in /AuditMessage/AuditSourceIdentification satisfies " +
				"(every $i in $as/AuditSourceTypeCode satisfies $i/@code='" + code + "')";
		ValidatorUtil.handleXpath(xpath, "AuditSource/AuditSourceTypeCode@code must be '" + code + "' if AuditSource/AuditSourceTypeCode present", ERROR, document, diagnostic);
	}
	
	protected void validateAuditSourceTypeCodeCodeSystemNameValue(String codeSystemName, String document, AuditMessageSpecification ams, List<Notification> diagnostic) throws Exception{
		String xpath = "every $as in /AuditMessage/AuditSourceIdentification satisfies " +
				"(every $i in $as/AuditSourceTypeCode satisfies $i/@codeSystemName='" + codeSystemName + "')";
		ValidatorUtil.handleXpath(xpath, "AuditSource/AuditSourceTypeCode@codeSystemName must be '" + codeSystemName + "' if AuditSource/AuditSourceTypeCode present", ERROR, document, diagnostic);
	}
	
	protected void validateAuditSourceTypeCodeDisplayNameValue(String displayName, String document, AuditMessageSpecification ams, List<Notification> diagnostic) throws Exception{
		String xpath = "every $as in /AuditMessage/AuditSourceIdentification satisfies " +
				"(every $i in $as/AuditSourceTypeCode satisfies $i/@displayName='" + displayName + "')";
        ValidatorUtil.handleXpath(xpath,
                "AuditSource/AuditSourceTypeCode@displayName should be '" + displayName + "' if AuditSource/AuditSourceTypeCode present", WARNING,
                document, diagnostic);
    }
	
	protected void validateAuditSourceCodeValueOpt(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getAuditSource() != null && ams.getAuditSource().getAuditSourceCodeValue() != null 
				&& ams.getAuditSource().getAuditSourceCodeValue().getOptionality() != null){

			try{
				String xpath = "every $as in /AuditMessage/AuditSourceIdentification satisfies $as/@code";

				switch (ams.getAuditSource().getAuditSourceCodeValue().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, "AuditSource/@code is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, "AuditSource/@code has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "count(/AuditMessage/AuditSourceIdentification/@code)=0";
					ValidatorUtil.handleXpath(xpath, "AuditSource/@code is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	protected void validateAuditSourceCodeValueValue(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getAuditSource() != null && ams.getAuditSource().getAuditSourceCodeValue() != null 
				&& ams.getAuditSource().getAuditSourceCodeValue().getValue() != null){
			try{
				String val = ams.getAuditSource().getAuditSourceCodeValue().getValue();
				Pattern pat = Pattern.compile("^\\s*?EV\\s*?\\(\\s*?'(.*?)'\\s*?,\\s*?'(.*?)'\\s*?,\\s*?'(.*?)'\\s*?\\)\\s*?$");
				Matcher matcher = pat.matcher(val);
				if (matcher.find()){
					this.validateAuditSourceCodeValueCodeValue(matcher.group(1), document , ams, diagnostic);
					this.validateAuditSourceCodeValueCodeSystemNameValue(matcher.group(2), document , ams, diagnostic);
					this.validateAuditSourceCodeValueDisplayNameValue(matcher.group(3), document , ams, diagnostic);
				}
			}
			catch(Exception e){

			}
		}
	}
	
	protected void validateAuditSourceCodeValueCodeValue(String code, String document, AuditMessageSpecification ams, List<Notification> diagnostic) throws Exception{
		String xpath = "every $as in /AuditMessage/AuditSourceIdentification satisfies $as/@code='" + code + "'";
		ValidatorUtil.handleXpath(xpath, "AuditSource/@code must be '" + code + "' if AuditSource present", ERROR, document, diagnostic);
	}
	
	protected void validateAuditSourceCodeValueCodeSystemNameValue(String codeSystemName, String document, AuditMessageSpecification ams, List<Notification> diagnostic) throws Exception{
		String xpath = "every $as in /AuditMessage/AuditSourceIdentification satisfies $as/@codeSystemName='" + codeSystemName + "'";
		ValidatorUtil.handleXpath(xpath, "AuditSource/@codeSystemName must be '" + codeSystemName + "' if AuditSource present", ERROR, document, diagnostic);
	}
	
	protected void validateAuditSourceCodeValueDisplayNameValue(String displayName, String document, AuditMessageSpecification ams, List<Notification> diagnostic) throws Exception{
		String xpath = "every $as in /AuditMessage/AuditSourceIdentification satisfies $as/@originalText='" + displayName + "'";
        ValidatorUtil.handleXpath(xpath, "AuditSource/@originalText should be '" + displayName + "' if AuditSource present", WARNING, document,
                diagnostic);
    }
}
