/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.audit.message.action;

import net.ihe.gazelle.audit.message.desc.AuditMessageSpecification;
import net.ihe.gazelle.audit.message.menu.AuditMessagePages;
import net.ihe.gazelle.audit.message.utils.ConfigurationException;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * @author Cédric Eoche-Duval
 */
@Name("auditMessageList")
@Scope(ScopeType.PAGE)
public class AuditMessageList extends AuditMessageBrowser implements UserAttributeCommon {

    private static final Logger LOG = LoggerFactory.getLogger(AuditMessageList.class);

    private AuditMessageSpecification selectedAuditMessageSpecification;

    @In(create = true)
    private AuditMessageOIDProviderLocal auditMessageOIDProvider;

    @In(value = "gumUserService")
    private UserService userService;

    private byte[] amsFileBytes = {};

    private byte[] amsArchiveFileBytes = {};

    public byte[] getAmsFileBytes() {
        return amsFileBytes.clone();
    }

    public void setAmsFileBytes(byte[] amsFileBytes) {
        this.amsFileBytes = amsFileBytes.clone();
    }

    public byte[] getAmsArchiveFileBytes() {
        return amsArchiveFileBytes.clone();
    }

    public void setAmsArchiveFileBytes(byte[] amsArchiveFileBytes) {
        this.amsArchiveFileBytes = amsArchiveFileBytes.clone();
    }

    public AuditMessageSpecification getSelectedAuditMessageSpecification() {
        return selectedAuditMessageSpecification;
    }

    public void setSelectedAuditMessageSpecification(AuditMessageSpecification selectedAuditMessageSpecification) {
        this.selectedAuditMessageSpecification = selectedAuditMessageSpecification;
    }

    public void uploadAmsListener(FileUploadEvent event) throws Exception {
        UploadedFile item = event.getUploadedFile();
        setAmsFileBytes(item.getData());
    }

    public void uploadAmsArchiveListener(FileUploadEvent event) throws Exception {
        UploadedFile item = event.getUploadedFile();
        setAmsArchiveFileBytes(item.getData());
    }

    @Restrict("#{s:hasRole('admin_role') or s:hasRole('tests_editor_role')}")
    public void downloadAll() throws IOException {
        try {
            AuditMessageExporter.downloadArchive(this.getListAuditMessageSpecifications());
        } catch (JAXBException e) {
            FacesMessages.instance()
                    .add(StatusMessage.Severity.ERROR,
                            "Error while exporting audit-messages to XML: " + e.getMessage());
            LOG.warn("Error while exporting audit-messages: jaxb exception: " + e.getMessage());
        }
    }

    @Restrict("#{s:hasRole('admin_role') or s:hasRole('tests_editor_role')}")
    public void exportToXml(AuditMessageSpecification selectedAuditMessageSpecification) {
        try {
            AuditMessageExporter.downloadOne(selectedAuditMessageSpecification);
        } catch (JAXBException e) {
            FacesMessages.instance()
                    .add(StatusMessage.Severity.ERROR, "Error while exporting audit-message to XML: " + e.getMessage());
            LOG.warn("Error while exporting Audit Message: jaxb exception: " + e.getMessage());
        }
    }

    @Restrict("#{s:hasRole('admin_role') or s:hasRole('tests_editor_role')}")
    public String editSelectedAuditMessageSpecification(AuditMessageSpecification selectedAuditMessageSpecification) {
        return PreferenceService.getString("application_url") + AuditMessagePages.AM_EDIT.getMenuLink() +
                "?id=" + selectedAuditMessageSpecification.getId();
    }

    @Restrict("#{s:hasRole('admin_role') or s:hasRole('tests_editor_role')}")
    public void copyAuditMessage(AuditMessageSpecification ams) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            AuditMessageMarshaller.objectToXml(ams, baos);
        } catch (JAXBException e) {
            LOG.error("Error while copying Audit Message : ", e);
        }
        try {
            AuditMessageSpecification newams = AuditMessageMarshaller.xmlToObject(
                    new ByteArrayInputStream(baos.toByteArray()));
            newams.setName(newams.getName() + "_COPY");
            newams.setOid(auditMessageOIDProvider.generateNextOid());
            EntityManager em = EntityManagerService.provideEntityManager();
            em.merge(newams);
            em.flush();
        } catch (ConfigurationException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Fail to copy Audit message specification: " + e.getMessage());
            LOG.error("Fail to copy Audit message specification: " + e.getMessage());
        } catch (JAXBException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Not able to parse the specified xml : " + e.getMessage());
            LOG.warn("Error while copying Audit Message : jaxb exception : " + e.getMessage());
        }
    }

    @Restrict("#{s:hasRole('admin_role') or s:hasRole('tests_editor_role')}")
    public void importSpecification() {
        if (amsFileBytes != null) {
            try {
                if (xmlAuditMessageLoading(amsFileBytes)) {
                    FacesMessages.instance().add(StatusMessage.Severity.INFO, "Specification uploaded.");
                }
            } catch (Exception e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "An error occurs while importing the specification : " + e.getMessage());
                LOG.error("An error occurs while importing an Audit Message specification. ", e);
            }
        }
    }

    @Restrict("#{s:hasRole('admin_role') or s:hasRole('tests_editor_role')}")
    public void importSpecificationArchive() {

        File amsTempFile = buildTempAmsArchiveFile();
        if (amsTempFile != null) {
            List<byte[]> listUploaded = null;
            try {
                listUploaded = AuditMessageZipper.unzip(amsTempFile);
            } catch (IOException e) {
                FacesMessages.instance()
                        .add(StatusMessage.Severity.ERROR, "Unable to extract Audit Message archive : " + e.getMessage());
                LOG.warn("Unable to extract Audit Message archive : " + e.getMessage());
            }
            if (listUploaded != null) {
                if (!listUploaded.isEmpty()) {
                    try {
                        int counter = 0;
                        boolean hasErrorMessages = false;
                        for (byte[] spec : listUploaded) {
                            counter++;
                            if (!xmlAuditMessageLoading(spec)) {
                                hasErrorMessages = true;
                                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Fail to import element #" + counter);
                            }
                        }
                        if (!hasErrorMessages) {
                            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Specification archive uploaded");
                        }
                    } catch (Exception e) {
                        FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                                "An error occurs while importing the specification : " + e.getMessage());
                        LOG.error("An error occurs while importing an Audit Message specification. ", e);
                    }
                } else {
                    FacesMessages.instance().add(StatusMessage.Severity.WARN, "Specification archive is empty");
                }
            }
        }
    }

    @Restrict("#{s:hasRole('admin_role') or s:hasRole('tests_editor_role')}")
    public void deleteSelectedAuditMessageSpecification() {
        if (this.selectedAuditMessageSpecification != null) {
            EntityManager em = EntityManagerService.provideEntityManager();
            this.selectedAuditMessageSpecification = em.find(AuditMessageSpecification.class,
                    this.selectedAuditMessageSpecification.getId());
            em.remove(this.selectedAuditMessageSpecification);
            em.flush();
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "The select AuditMessage type has been deleted");
        }
    }

    @Restrict("#{s:hasRole('admin_role')}")
    public void deleteAllSpecifications() {
        EntityManager em = EntityManagerService.provideEntityManager();
        List<AuditMessageSpecification> amList = (List<AuditMessageSpecification>) em.createQuery(
                AuditMessageSpecification.FINDALL_QUERY).getResultList();
        for (AuditMessageSpecification am : amList) {
            em.remove(am);
        }
        em.flush();
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "All AuditMessage specifications have been deleted");
    }

    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }

    private boolean xmlAuditMessageLoading(byte[] byteArray) {
        try {
            AuditMessageSpecification newams = AuditMessageMarshaller.xmlToObject(new ByteArrayInputStream(byteArray));
            if (newams.getOid() == null || newams.getOid().isEmpty()) {
                newams.setOid(auditMessageOIDProvider.generateNextOid());
            }
            newams.setTrackChanges(false);
            EntityManager em = EntityManagerService.provideEntityManager();
            em.persist(newams);
            em.flush();
            return true;
        } catch (ConfigurationException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Fail to import Audit message specification: " + e.getMessage());
            LOG.error("Fail to import Audit message specification: " + e.getMessage());
        } catch (JAXBException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Not able to parse the specified xml : " + e.getMessage());
            LOG.warn("Error while importing AuditMessage : Not able to parse the xml : " + e.getMessage());
        }
        return false;
    }

    private File buildTempAmsArchiveFile() {
        File amsTempFile = null;
        try {
            amsTempFile = File.createTempFile("ams", null);
            try (FileOutputStream fos = new FileOutputStream(amsTempFile)) {
                fos.write(this.amsArchiveFileBytes);
            }
        } catch (IOException e) {
            FacesMessages.instance()
                    .add(StatusMessage.Severity.ERROR, "Error while uploading the audit-message archive : " + e.getMessage());
            LOG.error("Error while uploading the audit-message archive", e);
            amsTempFile = null;
        }
        return amsTempFile;
    }

}
