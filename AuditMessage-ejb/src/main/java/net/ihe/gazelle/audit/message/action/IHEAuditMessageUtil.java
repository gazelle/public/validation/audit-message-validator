/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.audit.message.action;

import net.ihe.gazelle.audit.message.desc.ActiveParticipantDescription;
import net.ihe.gazelle.audit.message.desc.AuditMessageSpecification;
import net.ihe.gazelle.audit.message.desc.ParticipantObjectIdentificationDesc;

public class IHEAuditMessageUtil {

	public static ActiveParticipantDescription getSourceOfAuditMessageSpecification(AuditMessageSpecification selectedAuditMessageSpecification){
		if (selectedAuditMessageSpecification != null){
			if (selectedAuditMessageSpecification.getActiveParticipants() != null){
				for (ActiveParticipantDescription act : selectedAuditMessageSpecification.getActiveParticipants()) {
					if (act.getName() != null
							&& act.getName().equals("Source")){
						return act;
					}
				}
			}
		}
		return null;
	}

	public static ActiveParticipantDescription getHrOfAuditMessageSpecification(AuditMessageSpecification selectedAuditMessageSpecification){
		if (selectedAuditMessageSpecification != null){
			if (selectedAuditMessageSpecification.getActiveParticipants() != null){
				for (ActiveParticipantDescription act : selectedAuditMessageSpecification.getActiveParticipants()) {
					if (act.getName() != null && act.getName().equals("HumanRequestor")){
						return act;
					}
				}
			}
		}
		return null;
	}

	public static ActiveParticipantDescription getDestinationOfAuditMessageSpecification(AuditMessageSpecification selectedAuditMessageSpecification){
		if (selectedAuditMessageSpecification != null){
			if (selectedAuditMessageSpecification.getActiveParticipants() != null){
				for (ActiveParticipantDescription act : selectedAuditMessageSpecification.getActiveParticipants()) {
					if (act.getName() != null && act.getName().equals("Destination")){
						return act;
					}
				}
			}
		}
		return null;
	}

	public static ParticipantObjectIdentificationDesc getPatientOfAuditMessageSpecification(AuditMessageSpecification selectedAuditMessageSpecification){
		if (selectedAuditMessageSpecification != null){
			if (selectedAuditMessageSpecification.getParticipantObjectIdentifications() != null){
				for (ParticipantObjectIdentificationDesc act : selectedAuditMessageSpecification.getParticipantObjectIdentifications()) {
					if (act.getName() != null && act.getName().equals("Patient")){
						return act;
					}
				}
			}
		}
		return null;
	}

	public static ParticipantObjectIdentificationDesc getQueryOfAuditMessageSpecification(AuditMessageSpecification selectedAuditMessageSpecification){
		if (selectedAuditMessageSpecification != null){
			if (selectedAuditMessageSpecification.getParticipantObjectIdentifications() != null){
				for (ParticipantObjectIdentificationDesc act : selectedAuditMessageSpecification.getParticipantObjectIdentifications()) {
					if (act.getName() != null && act.getName().equals("Query")){
						return act;
					}
				}
			}
		}
		return null;
	}

}
