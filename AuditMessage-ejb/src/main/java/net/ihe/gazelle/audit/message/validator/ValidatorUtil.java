/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.audit.message.validator;

import net.ihe.gazelle.audit.message.desc.AuditMessageSpecification;
import net.ihe.gazelle.audit.message.utils.XpathUtils;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.Warning;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ValidatorUtil {

	private static final Logger LOG = LoggerFactory.getLogger(ValidatorUtil.class);
	
	private ValidatorUtil(){}
	
	public static String getCodeIdentifier(boolean isDocumentCompatible){
		if(isDocumentCompatible){
			return "csd-code";
		}
		return "code";
	}
	
	public static String getDisplayNameIdentifier(boolean isDocumentCompatible){
		if(isDocumentCompatible){
			return "originalText";
		}
		return "displayName";
	}
	
	public static String getCodeIdentifier(AuditMessageSpecification ams){
		if(ams != null && ams.getIsDicomCompatible() != null && ams.getIsDicomCompatibleAsBoolean()){
			return "csd-code";
		}
		return "code";
	}
	
	public static String getDisplayNameIdentifier(AuditMessageSpecification ams){
		if(ams != null && ams.getIsDicomCompatible() != null && ams.getIsDicomCompatibleAsBoolean()){
			return "originalText";
		}
		return "displayName";
	}
	
	public static void handleXpath(String xpath, String description, String optionality, String document, List<Notification> diagnostic){
		diagnostic.add(handleXpath(xpath, description, optionality, document));
	}
	
	public static Notification handleXpath(String xpath, String description, String optionality, String document){
		try{
			boolean res = XpathUtils.evaluateByString(document, xpath);
			if (res){
				Note not = new Note();
				not.setDescription(description);
				not.setTest(xpath);
				return not;
			}
			else{
				if (optionality.equalsIgnoreCase("error")){
					Error not = new Error();
					not.setDescription(description);
					not.setTest(xpath);
					return not;
				}
				else if (optionality.equalsIgnoreCase("warning")){
					Warning not = new Warning();
					not.setDescription(description);
					not.setTest(xpath);
					return not;
				}
			}
		}
		catch(Exception e){
			LOG.warn("Unable to evaluate xPath {}\n{}", xpath, e.getMessage());
			Warning not = new Warning();
            not.setDescription("Internal error while evaluating the xPath. This condition can not be verified by the validator : " + description);
            not.setTest(xpath);
			return not;
		}
		return null;
	}

}
