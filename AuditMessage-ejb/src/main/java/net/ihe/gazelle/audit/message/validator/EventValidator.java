/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.audit.message.validator;

import net.ihe.gazelle.audit.message.desc.AuditMessageSpecification;
import net.ihe.gazelle.validation.Notification;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EventValidator implements AuditValidatorInterface {

	private static final String WARNING = "warning";
	private static final String ERROR = "error";

	protected EventValidator(){}

	public static final EventValidator instance = new EventValidator();

	@Override
	public void validate(String document, AuditMessageSpecification ams,
			List<Notification> diagnostic) {
		this.validateEventIDOpt(document, ams, diagnostic);
		this.validateEventIDValue(document, ams, diagnostic);
		this.validateEventActionCodeOpt(document, ams, diagnostic);
		this.validateEventActionCodeValue(document, ams, diagnostic);
		this.validateEventActionCodeRegex(document, ams, diagnostic);
		this.validateEventDateTimeOpt(document, ams, diagnostic);
		this.validateEventDateTimeValue(document, ams, diagnostic);
		this.validateEventDateTimeRegex(document, ams, diagnostic);
		this.validateEventOutcomeIndicatorOpt(document, ams, diagnostic);
		this.validateEventOutcomeIndicatorValue(document, ams, diagnostic);
		this.validateEventOutcomeIndicatorRegex(document, ams, diagnostic);
		this.validateEventTypeCodeOpt(document, ams, diagnostic);
		this.validateEventTypeCodeValue(document, ams, diagnostic);
		this.validateEventOutcomeDescriptionOpt(document, ams, diagnostic);
		this.validateEventOutcomeDescriptionValue(document, ams, diagnostic);
		this.validateEventOutcomeDescriptionRegex(document, ams, diagnostic);
		this.validatePurposeOfUseOpt(document, ams, diagnostic);
		this.validatePurposeOfUseValue(document, ams, diagnostic);
	}
	
	protected void validateEventIDOpt(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getEvent() != null && ams.getEvent().getEventID() != null && ams.getEvent().getEventID().getOptionality() != null){

			try{
				String xpath = "/AuditMessage/EventIdentification/EventID";

				switch (ams.getEvent().getEventID().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, "Event/EventID is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, "Event/EventID has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "count(/AuditMessage/EventIdentification/EventID)=0";
					ValidatorUtil.handleXpath(xpath, "Event/EventID is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}

	protected void validateEventIDValue(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getEvent() != null && ams.getEvent().getEventID() != null && ams.getEvent().getEventID().getValue() != null){
			try{
				String val = ams.getEvent().getEventID().getValue();
				Pattern pat = Pattern.compile("^\\s*?EV\\s*?\\(\\s*?'(.*?)'\\s*?,\\s*?'(.*?)'\\s*?,\\s*?'(.*?)'\\s*?\\)\\s*?$");
				Matcher matcher = pat.matcher(val);
				if (matcher.find()){
					this.validateEventIDCodeValue(matcher.group(1), document , ams, diagnostic);
					this.validateEventIDCodeSystemNameValue(matcher.group(2), document , ams, diagnostic);
					this.validateEventIDDisplayNameValue(matcher.group(3), document , ams, diagnostic);
				}
			}
			catch(Exception e){

			}
		}
	}

	protected void validateEventIDCodeValue(String code, String document, AuditMessageSpecification ams, List<Notification> diagnostic) throws Exception{
		String xpath = "not (/AuditMessage/EventIdentification/EventID) or /AuditMessage/EventIdentification/EventID/@" + ValidatorUtil.getCodeIdentifier(ams) + "='" + code + "'";
		ValidatorUtil.handleXpath(xpath, "/AuditMessage/EventIdentification/EventID@" + ValidatorUtil.getCodeIdentifier(ams) + " must be '" + code + 
				"' if Event/EventID present", ERROR, document, diagnostic);
	}

	protected void validateEventIDCodeSystemNameValue(String codeSystemName, String document, AuditMessageSpecification ams, List<Notification> diagnostic) throws Exception{
		String xpath = "not (/AuditMessage/EventIdentification/EventID) or /AuditMessage/EventIdentification/EventID/@codeSystemName='" + codeSystemName + "'";
		ValidatorUtil.handleXpath(xpath, "/AuditMessage/EventIdentification/EventID@codeSystemName must be '" + codeSystemName + "' if Event/EventID present",
				ERROR, document, diagnostic);
	}

	protected void validateEventIDDisplayNameValue(String displayName, String document, AuditMessageSpecification ams, List<Notification> diagnostic) throws Exception{
		String xpath = "not (/AuditMessage/EventIdentification/EventID) or /AuditMessage/EventIdentification/EventID/@" + ValidatorUtil.getDisplayNameIdentifier(ams) + "='" + displayName + "'";
        ValidatorUtil.handleXpath(xpath, "/AuditMessage/EventIdentification/EventID@" + ValidatorUtil
                        .getDisplayNameIdentifier(ams) + " should be '" + displayName + "' if Event/EventID present",
                WARNING, document, diagnostic);
    }

	protected void validateEventActionCodeOpt(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getEvent() != null && ams.getEvent().getEventActionCode() != null && ams.getEvent().getEventActionCode().getOptionality() != null){

			try{
				String xpath = "/AuditMessage/EventIdentification/@EventActionCode";

				switch (ams.getEvent().getEventActionCode().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, "Event/@EventActionCode is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, "Event/@EventActionCode has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "count(/AuditMessage/EventIdentification/@EventActionCode)=0";
					ValidatorUtil.handleXpath(xpath, "Event/EventActionCode is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}

	protected void validateEventActionCodeValue(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getEvent() != null 
				&& ams.getEvent().getEventActionCode() != null 
				&& ams.getEvent().getEventActionCode().getValue() != null){
			String xpath = "not (/AuditMessage/EventIdentification/@EventActionCode) or /AuditMessage/EventIdentification/@EventActionCode='" + ams.getEvent().getEventActionCode().getValue() + "'";
			ValidatorUtil.handleXpath(xpath, 
					"/AuditMessage/EventIdentification/@EventActionCode must be '" + ams.getEvent().getEventActionCode().getValue() + "' if Event/EventActionCode present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateEventActionCodeRegex(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getEvent() != null 
				&& ams.getEvent().getEventActionCode() != null 
				&& ams.getEvent().getEventActionCode().getRegex() != null){
			String xpath = "not (/AuditMessage/EventIdentification/@EventActionCode) or matches(/AuditMessage/EventIdentification/@EventActionCode, '" + ams.getEvent().getEventActionCode().getRegex() + "')";
			ValidatorUtil.handleXpath(xpath, 
					"/AuditMessage/EventIdentification/@EventActionCode must have this form : '" + ams.getEvent().getEventActionCode().getRegex() + "' if Event/EventActionCode present" ,
					ERROR, document, diagnostic);
		}
	}

	protected void validateEventDateTimeOpt(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getEvent() != null && ams.getEvent().getEventDateTime() != null 
				&& ams.getEvent().getEventDateTime().getOptionality() != null){

			try{
				String xpath = "/AuditMessage/EventIdentification/@EventDateTime";

				switch (ams.getEvent().getEventDateTime().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, "Event/@EventDateTime is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, "Event/@EventDateTime has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "count(/AuditMessage/EventIdentification/@EventDateTime)=0";
					ValidatorUtil.handleXpath(xpath, "Event/@EventDateTime is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}

	protected void validateEventDateTimeValue(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getEvent() != null 
				&& ams.getEvent().getEventDateTime() != null 
				&& ams.getEvent().getEventDateTime().getValue() != null){
			String xpath = "not (/AuditMessage/EventIdentification/@EventDateTime) or /AuditMessage/EventIdentification/@EventDateTime='" + 
				ams.getEvent().getEventDateTime().getValue() + "'";
			ValidatorUtil.handleXpath(xpath, 
					"Event/@EventDateTime must be '" + ams.getEvent().getEventDateTime().getValue() + 
					"' if Event/EventActionCode present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateEventDateTimeRegex(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getEvent() != null 
				&& ams.getEvent().getEventDateTime() != null 
				&& ams.getEvent().getEventDateTime().getRegex() != null){
			String xpath = "not (/AuditMessage/EventIdentification/@EventDateTime) or matches(/AuditMessage/EventIdentification/@EventDateTime, '" + ams.getEvent().getEventDateTime().getRegex() + "')";
			ValidatorUtil.handleXpath(xpath, 
					"Event/@EventDateTime must have this form : '" + ams.getEvent().getEventDateTime().getRegex() + "' if Event/@EventDateTime present" ,
					ERROR, document, diagnostic);
		}
	}

	protected void validateEventOutcomeIndicatorOpt(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getEvent() != null && ams.getEvent().getEventOutcomeIndicator() != null 
				&& ams.getEvent().getEventOutcomeIndicator().getOptionality() != null){

			try{
				String xpath = "/AuditMessage/EventIdentification/@EventOutcomeIndicator";

				switch (ams.getEvent().getEventOutcomeIndicator().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, "Event/@EventOutcomeIndicator is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, "Event/@EventOutcomeIndicator has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "count(/AuditMessage/EventIdentification/@EventOutcomeIndicator)=0";
					ValidatorUtil.handleXpath(xpath, "Event/@EventOutcomeIndicator is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}

	protected void validateEventOutcomeIndicatorValue(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getEvent() != null 
				&& ams.getEvent().getEventOutcomeIndicator() != null 
				&& ams.getEvent().getEventOutcomeIndicator().getValue() != null){
			String xpath = "not(/AuditMessage/EventIdentification/@EventOutcomeIndicator) or /AuditMessage/EventIdentification/@EventOutcomeIndicator='" + 
				ams.getEvent().getEventOutcomeIndicator().getValue() + "'";
			ValidatorUtil.handleXpath(xpath, 
					"Event/@EventOutcomeIndicator must be '" + ams.getEvent().getEventOutcomeIndicator().getValue() + 
					"' if Event/@EventOutcomeIndicator present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateEventOutcomeIndicatorRegex(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getEvent() != null 
				&& ams.getEvent().getEventOutcomeIndicator() != null 
				&& ams.getEvent().getEventOutcomeIndicator().getRegex() != null){
			String xpath = "not (/AuditMessage/EventIdentification/@EventOutcomeIndicator) or matches(/AuditMessage/EventIdentification/@EventOutcomeIndicator, '" 
				+ ams.getEvent().getEventOutcomeIndicator().getRegex() + "')";
			ValidatorUtil.handleXpath(xpath, 
					"Event/@EventOutcomeIndicator must have this form : '" + 
							ams.getEvent().getEventOutcomeIndicator().getRegex() + "' if Event/@EventOutcomeIndicator present" ,
					ERROR, document, diagnostic);
		}
	}

	protected void validateEventTypeCodeOpt(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getEvent() != null && ams.getEvent().getEventTypeCode() != null 
				&& ams.getEvent().getEventTypeCode().getOptionality() != null){

			try{
				String xpath = "/AuditMessage/EventIdentification/EventTypeCode";

				switch (ams.getEvent().getEventTypeCode().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, "Event/EventTypeCode is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, "Event/EventTypeCode has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "count(/AuditMessage/EventIdentification/EventTypeCode)=0";
					ValidatorUtil.handleXpath(xpath, "Event/EventTypeCode is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}

	protected void validateEventTypeCodeValue(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getEvent() != null && ams.getEvent().getEventTypeCode() != null && ams.getEvent().getEventTypeCode().getValue() != null){
			try{
				String val = ams.getEvent().getEventTypeCode().getValue();
				Pattern pat = Pattern.compile("^\\s*EV\\s*\\(\\s*'(.*?)'\\s*,\\s*'(.*?)'\\s*,\\s*'(.*?)'\\s*\\)\\s*$");
				Matcher matcher = pat.matcher(val);
				if (matcher.find()){
					this.validateEventTypeCodeValue(matcher.group(1), document , ams, diagnostic);
					this.validateEventTypeCodeSystemNameValue(matcher.group(2), document , ams, diagnostic);
					this.validateEventTypeDisplayNameValue(matcher.group(3), document , ams, diagnostic);
				}
			}
			catch(Exception e){

			}
		}
	}
	
	protected void validateEventTypeCodeValue(String code, String document, AuditMessageSpecification ams, List<Notification> diagnostic) throws Exception{
		String xpath = "count(/AuditMessage/EventIdentification/EventTypeCode)=0 or (every $i in /AuditMessage/EventIdentification/EventTypeCode satisfies  $i/@" + ValidatorUtil.getCodeIdentifier(ams) + "='" + code + "')";
		ValidatorUtil.handleXpath(xpath, "Event/EventTypeCode@" + ValidatorUtil.getCodeIdentifier(ams) + " must be '" + code + 
				"' if Event/EventTypeCode present", ERROR, document, diagnostic);
	}

	protected void validateEventTypeCodeSystemNameValue(String codeSystemName, String document, AuditMessageSpecification ams, List<Notification> diagnostic) throws Exception{
		String xpath = "count(/AuditMessage/EventIdentification/EventTypeCode)=0 or (every $i in /AuditMessage/EventIdentification/EventTypeCode satisfies  $i/@codeSystemName='" + codeSystemName + "')";
		ValidatorUtil.handleXpath(xpath, "Event/EventTypeCode@codeSystemName must be '" + codeSystemName + "' if Event/EventTypeCode present",
				ERROR, document, diagnostic);
	}

	protected void validateEventTypeDisplayNameValue(String displayName, String document, AuditMessageSpecification ams, List<Notification> diagnostic) throws Exception{
		String xpath = "count(/AuditMessage/EventIdentification/EventTypeCode)=0 or (every $i in /AuditMessage/EventIdentification/EventTypeCode satisfies  $i/@" + ValidatorUtil.getDisplayNameIdentifier(ams) + "='" + displayName + "')";
        ValidatorUtil.handleXpath(xpath, "Event/EventTypeCode@" + ValidatorUtil
                        .getDisplayNameIdentifier(ams) + " should be '" + displayName + "' if Event/EventTypeCode present",
                WARNING, document, diagnostic);
    }
	
	protected void validateEventOutcomeDescriptionOpt(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getEvent() != null && ams.getEvent().getEventOutcomeDescription() != null && ams.getEvent().getEventOutcomeDescription().getOptionality() != null){

			try{
				String xpath = "/AuditMessage/EventIdentification/EventOutcomeDescription";

				switch (ams.getEvent().getEventOutcomeDescription().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, "Event/EventOutcomeDescription is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, "Event/EventOutcomeDescription has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "count(/AuditMessage/EventIdentification/EventOutcomeDescription)=0";
					ValidatorUtil.handleXpath(xpath, "Event/EventOutcomeDescription is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}

	protected void validateEventOutcomeDescriptionValue(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getEvent() != null 
				&& ams.getEvent().getEventOutcomeDescription() != null 
				&& ams.getEvent().getEventOutcomeDescription().getValue() != null){
			String xpath = "not (/AuditMessage/EventIdentification/EventOutcomeDescription) or /AuditMessage/EventIdentification/EventOutcomeDescription/text()='" + 
				ams.getEvent().getEventOutcomeDescription().getValue() + "'";
			ValidatorUtil.handleXpath(xpath, 
					"/AuditMessage/EventIdentification/EventOutcomeDescription must be '" + ams.getEvent().getEventOutcomeDescription().getValue() + "' if Event/EventOutcomeDescription present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validateEventOutcomeDescriptionRegex(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getEvent() != null 
				&& ams.getEvent().getEventOutcomeDescription() != null 
				&& ams.getEvent().getEventOutcomeDescription().getRegex() != null){
			String xpath = "not (/AuditMessage/EventIdentification/EventOutcomeDescription) or matches(/AuditMessage/EventIdentification/EventOutcomeDescription/text(), '" + 
				ams.getEvent().getEventOutcomeDescription().getRegex() + "')";
			ValidatorUtil.handleXpath(xpath, 
					"/AuditMessage/EventIdentification/EventOutcomeDescription must have this form : '" + ams.getEvent().getEventOutcomeDescription().getRegex() + "' if Event/EventOutcomeDescription present" ,
					ERROR, document, diagnostic);
		}
	}
	
	protected void validatePurposeOfUseOpt(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getEvent() != null && ams.getEvent().getPurposeOfUse() != null 
				&& ams.getEvent().getPurposeOfUse().getOptionality() != null){

			try{
				String xpath = "/AuditMessage/EventIdentification/PurposeOfUse";

				switch (ams.getEvent().getPurposeOfUse().getOptionality()) {

				case M:
					ValidatorUtil.handleXpath(xpath, "Event/PurposeOfUse is mandatory", ERROR, document, diagnostic);
					break;

				case C:
					ValidatorUtil.handleXpath(xpath, "Event/PurposeOfUse has optionality 'C'", WARNING, document, diagnostic);
					break;

				case NA:
					xpath = "count(/AuditMessage/EventIdentification/PurposeOfUse)=0";
					ValidatorUtil.handleXpath(xpath, "Event/PurposeOfUse is prohibited", ERROR, document, diagnostic);
					break;
				default:
					break;
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}

	protected void validatePurposeOfUseValue(String document, AuditMessageSpecification ams, List<Notification> diagnostic){
		if (ams != null && ams.getEvent() != null && ams.getEvent().getPurposeOfUse() != null && ams.getEvent().getPurposeOfUse().getValue() != null){
			try{
				String val = ams.getEvent().getPurposeOfUse().getValue();
				Pattern pat = Pattern.compile("^\\s*EV\\s*\\(\\s*'(.*?)'\\s*,\\s*'(.*?)'\\s*,\\s*'(.*?)'\\s*\\)\\s*$");
				Matcher matcher = pat.matcher(val);
				if (matcher.find()){
					this.validatePurposeOfUseValue(matcher.group(1), document , ams, diagnostic);
					this.validatePurposeOfUseSystemNameValue(matcher.group(2), document , ams, diagnostic);
					this.validatePurposeOfUseDisplayNameValue(matcher.group(3), document , ams, diagnostic);
				}
			}
			catch(Exception e){

			}
		}
	}
	
	protected void validatePurposeOfUseValue(String code, String document, AuditMessageSpecification ams, List<Notification> diagnostic) throws Exception{
		String xpath = "count(/AuditMessage/EventIdentification/PurposeOfUse)=0 or (every $i in /AuditMessage/EventIdentification/PurposeOfUse satisfies  $i/@" + ValidatorUtil.getCodeIdentifier(ams) + "='" + code + "')";
		ValidatorUtil.handleXpath(xpath, "Event/PurposeOfUse@" + ValidatorUtil.getCodeIdentifier(ams) + " must be '" + code + "' if Event/PurposeOfUse present", ERROR, document, diagnostic);
	}

	protected void validatePurposeOfUseSystemNameValue(String codeSystemName, String document, AuditMessageSpecification ams, List<Notification> diagnostic) throws Exception{
		String xpath = "count(/AuditMessage/EventIdentification/PurposeOfUse)=0 or (every $i in /AuditMessage/EventIdentification/PurposeOfUse satisfies  $i/@codeSystemName='" + codeSystemName + "')";
		ValidatorUtil.handleXpath(xpath, "Event/PurposeOfUse@codeSystemName must be '" + codeSystemName + "' if Event/PurposeOfUse present",
				ERROR, document, diagnostic);
	}

	protected void validatePurposeOfUseDisplayNameValue(String displayName, String document, AuditMessageSpecification ams, List<Notification> diagnostic) throws Exception{
		String xpath = "count(/AuditMessage/EventIdentification/PurposeOfUse)=0 or (every $i in /AuditMessage/EventIdentification/PurposeOfUse satisfies  $i/@" + ValidatorUtil.getDisplayNameIdentifier(ams) + "='" + displayName + "')";
        ValidatorUtil.handleXpath(xpath, "Event/PurposeOfUse@" + ValidatorUtil
                        .getDisplayNameIdentifier(ams) + " should be '" + displayName + "' if Event/PurposeOfUse present",
                WARNING, document, diagnostic);
    }

}
