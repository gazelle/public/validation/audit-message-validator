/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2012.12.25 at 01:02:08 PM CET 
//

package net.ihe.gazelle.audit.message.desc;

import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import java.io.Serializable;

/**
 * <p>
 * Java class for EventIdentificationDesc complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name=&quot;EventIdentificationDesc&quot;&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base=&quot;{http://www.w3.org/2001/XMLSchema}anyType&quot;&gt;
 *       &lt;sequence&gt;
 *         &lt;element name=&quot;EventID&quot; type=&quot;{}ElementDescription&quot;/&gt;
 *         &lt;element name=&quot;EventActionCode&quot; type=&quot;{}ElementDescription&quot;/&gt;
 *         &lt;element name=&quot;EventDateTime&quot; type=&quot;{}ElementDescription&quot;/&gt;
 *         &lt;element name=&quot;EventOutcomeIndicator&quot; type=&quot;{}ElementDescription&quot;/&gt;
 *         &lt;element name=&quot;EventTypeCode&quot; type=&quot;{}ElementDescription&quot;/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EventIdentificationDesc", propOrder = { "eventID",
		"eventActionCode", "eventDateTime", "eventOutcomeIndicator",
		"eventTypeCode", "eventOutcomeDescription",  "purposeOfUse"})
@Entity
@Name("eventIdentificationDesc")
@Table(name = "am_event_identification_desc", schema = "public")
@SequenceGenerator(name = "am_event_identification_desc_sequence", sequenceName = "am_event_identification_desc_id_seq", allocationSize = 1)
public class EventIdentificationDesc implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XmlTransient
	@Id
	@GeneratedValue(generator = "am_event_identification_desc_sequence", strategy = GenerationType.SEQUENCE)
	@NotNull
	@Column(name = "id")
	private Integer id;

	@XmlElement(name = "EventID", required = true)
	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "eventID_id")
	protected ElementDescription eventID;

	@XmlElement(name = "EventActionCode", required = true)
	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "eventActionCode_id")
	protected ElementDescription eventActionCode;

	@XmlElement(name = "EventDateTime", required = true)
	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "eventDateTime_id")
	protected ElementDescription eventDateTime;

	@XmlElement(name = "EventOutcomeIndicator", required = true)
	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "eventOutcomeIndicator_id")
	protected ElementDescription eventOutcomeIndicator;

	@XmlElement(name = "EventTypeCode", required = true)
	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "eventTypeCode_id")
	protected ElementDescription eventTypeCode;
	
	@XmlElement(name = "EventOutcomeDescription", required = true)
	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "eventOutcomeDescription_id")
	protected ElementDescription eventOutcomeDescription;
	
	@XmlElement(name = "PurposeOfUse", required = true)
	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "purposeOfUse_id")
	protected ElementDescription purposeOfUse;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the value of the eventID property.
	 * 
	 * @return possible object is {@link ElementDescription }
	 * 
	 */
	public ElementDescription getEventID() {
		return eventID;
	}

	/**
	 * Sets the value of the eventID property.
	 * 
	 * @param value
	 *            allowed object is {@link ElementDescription }
	 * 
	 */
	public void setEventID(ElementDescription value) {
		this.eventID = value;
	}

	/**
	 * Gets the value of the eventActionCode property.
	 * 
	 * @return possible object is {@link ElementDescription }
	 * 
	 */
	public ElementDescription getEventActionCode() {
		return eventActionCode;
	}

	/**
	 * Sets the value of the eventActionCode property.
	 * 
	 * @param value
	 *            allowed object is {@link ElementDescription }
	 * 
	 */
	public void setEventActionCode(ElementDescription value) {
		this.eventActionCode = value;
	}

	/**
	 * Gets the value of the eventDateTime property.
	 * 
	 * @return possible object is {@link ElementDescription }
	 * 
	 */
	public ElementDescription getEventDateTime() {
		return eventDateTime;
	}

	/**
	 * Sets the value of the eventDateTime property.
	 * 
	 * @param value
	 *            allowed object is {@link ElementDescription }
	 * 
	 */
	public void setEventDateTime(ElementDescription value) {
		this.eventDateTime = value;
	}

	/**
	 * Gets the value of the eventOutcomeIndicator property.
	 * 
	 * @return possible object is {@link ElementDescription }
	 * 
	 */
	public ElementDescription getEventOutcomeIndicator() {
		return eventOutcomeIndicator;
	}

	/**
	 * Sets the value of the eventOutcomeIndicator property.
	 * 
	 * @param value
	 *            allowed object is {@link ElementDescription }
	 * 
	 */
	public void setEventOutcomeIndicator(ElementDescription value) {
		this.eventOutcomeIndicator = value;
	}

	/**
	 * Gets the value of the eventTypeCode property.
	 * 
	 * @return possible object is {@link ElementDescription }
	 * 
	 */
	public ElementDescription getEventTypeCode() {
		return eventTypeCode;
	}

	/**
	 * Sets the value of the eventTypeCode property.
	 * 
	 * @param value
	 *            allowed object is {@link ElementDescription }
	 * 
	 */
	public void setEventTypeCode(ElementDescription value) {
		this.eventTypeCode = value;
	}

	public ElementDescription getEventOutcomeDescription() {
		return eventOutcomeDescription;
	}

	public void setEventOutcomeDescription(ElementDescription eventOutcomeDescription) {
		this.eventOutcomeDescription = eventOutcomeDescription;
	}

	public ElementDescription getPurposeOfUse() {
		return purposeOfUse;
	}

	public void setPurposeOfUse(ElementDescription purposeOfUse) {
		this.purposeOfUse = purposeOfUse;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((eventActionCode == null) ? 0 : eventActionCode.hashCode());
		result = prime * result + ((eventDateTime == null) ? 0 : eventDateTime.hashCode());
		result = prime * result + ((eventID == null) ? 0 : eventID.hashCode());
		result = prime * result + ((eventOutcomeDescription == null) ? 0 : eventOutcomeDescription.hashCode());
		result = prime * result + ((eventOutcomeIndicator == null) ? 0 : eventOutcomeIndicator.hashCode());
		result = prime * result + ((eventTypeCode == null) ? 0 : eventTypeCode.hashCode());
		result = prime * result + ((purposeOfUse == null) ? 0 : purposeOfUse.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventIdentificationDesc other = (EventIdentificationDesc) obj;
		if (eventActionCode == null) {
			if (other.eventActionCode != null)
				return false;
		} else if (!eventActionCode.equals(other.eventActionCode))
			return false;
		if (eventDateTime == null) {
			if (other.eventDateTime != null)
				return false;
		} else if (!eventDateTime.equals(other.eventDateTime))
			return false;
		if (eventID == null) {
			if (other.eventID != null)
				return false;
		} else if (!eventID.equals(other.eventID))
			return false;
		if (eventOutcomeDescription == null) {
			if (other.eventOutcomeDescription != null)
				return false;
		} else if (!eventOutcomeDescription.equals(other.eventOutcomeDescription))
			return false;
		if (eventOutcomeIndicator == null) {
			if (other.eventOutcomeIndicator != null)
				return false;
		} else if (!eventOutcomeIndicator.equals(other.eventOutcomeIndicator))
			return false;
		if (eventTypeCode == null) {
			if (other.eventTypeCode != null)
				return false;
		} else if (!eventTypeCode.equals(other.eventTypeCode))
			return false;
		if (purposeOfUse == null) {
			if (other.purposeOfUse != null)
				return false;
		} else if (!purposeOfUse.equals(other.purposeOfUse))
			return false;
		return true;
	}

}
