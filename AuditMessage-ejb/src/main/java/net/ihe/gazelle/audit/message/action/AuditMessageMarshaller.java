/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.audit.message.action;

import net.ihe.gazelle.audit.message.desc.AuditMessageSpecification;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

/**
 * Created by cel on 01/03/17.
 */
public class AuditMessageMarshaller {

    /**
     * Transform an XML Audit-Message specification into an {@link AuditMessageSpecification} object.
     *
     * @param auditSpecInputStream XML audit-message specification as ByteArrayInputStream
     * @return the specification as object
     * @throws JAXBException if a problem occurs while unmarshalling.
     */
    public static AuditMessageSpecification xmlToObject(ByteArrayInputStream auditSpecInputStream)
            throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(AuditMessageSpecification.class);
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        Reader specReader = new InputStreamReader(auditSpecInputStream, StandardCharsets.UTF_8);
        AuditMessageSpecification mimi = (AuditMessageSpecification) unmarshaller.unmarshal(specReader);
        return mimi;
    }

    /**
     * Transform an {@link AuditMessageSpecification} object into an XML Audit-Message specification.
     *
     * @param ams  the {@link AuditMessageSpecification} object to transform.
     * @param baos the output stream where to write the XML Audit-Message specification.
     * @throws JAXBException if a problem occurs while marshalling.
     */
    public static void objectToXml(AuditMessageSpecification ams, ByteArrayOutputStream baos) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(AuditMessageSpecification.class);
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        m.marshal(ams, baos);
    }

}
