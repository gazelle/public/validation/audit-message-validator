/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.audit.message.desc;

import net.ihe.gazelle.dates.TimestampNano;
import net.ihe.gazelle.users.UserService;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Date;

@MappedSuperclass
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class AuditedAuditMessageSpec {

    /**
     * Attribute lastChanged
     */
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(required = true)
    @Column(name = "last_changed", columnDefinition = "timestamp with time zone")
    protected Date lastChanged;

    /**
     * Attribute lastModifierId
     */
    @Column(name = "last_modifier_id")
    @XmlElement(required = true)
    protected String lastModifierId;

    @XmlTransient
    @Transient
    private boolean trackChanges = true;

    /**
     * Getter used for the lastChanged attribute.
     *
     * @return lastChanged : Timestamp corresponding to the last change.
     */
    public Date getLastChanged() {
        return lastChanged;
    }

    /**
     * Setter used for the lastChanged attribute.
     *
     * @param lastChanged : Timestamp corresponding to the last change.
     */
    public void setLastChanged(Date lastChanged) {
        this.lastChanged = lastChanged;
    }

    /**
     * Getter used for the lastModifierId attribute.
     *
     * @return lastModifierId : Id of the user who performed the last change.
     */
    public String getLastModifierId() {
        return lastModifierId;
    }

    /**
     * Setter used for the lastModifierId attribute.
     *
     * @param lastModifierId : Id of the user who performed the last change.
     */
    public void setLastModifierId(String lastModifierId) {
        this.lastModifierId = lastModifierId;
    }

    /**
     * Is track changes enabled or not
     *
     * @return true if enabled, false otherwise
     */
    public boolean isTrackChanges() {
        return trackChanges;
    }

    /**
     * Enable/disable track changes (enabled by default). Will be reset to enabled after each db reload.
     * Work only with entityManager.persist (merge is creating an object copy, so the field is reset)
     *
     * @param trackChanges true or false
     */
    public void setTrackChanges(boolean trackChanges) {
        this.trackChanges = trackChanges;
    }

    /**
     * Init Audit informations. Get the current time, find and set the user id logged in.
     */
    @PrePersist
    @PreUpdate
    public void recordChange() {
        if (isTrackChanges()) {
            this.setLastChanged(new TimestampNano());
            this.setLastModifierId(UserService.getUsername());
        }
    }

}
