/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.audit.message.utils;

import net.ihe.gazelle.preferences.PreferenceService;

/**
 * Created by cel on 09/03/17.
 */
public class AuditMessagePreferences {

    public static final String ATNA_MODE_ENABLED = "atna_mode_enabled";
    public static final String ROOT_OID = "audit_message_root_oid";
    public static final String OID_INDEX = "audit_message_index";
    public static final String DICOM_XSD = "dicom_xsd";
    public static final String RFC3881_XSD = "rfc3881_xsd";

    public static Boolean isAtnaModeEnabled() {
        return PreferenceService.getBoolean(ATNA_MODE_ENABLED);
    }

    public static String getAuditMessageRootOID() {
        String root = PreferenceService.getString(ROOT_OID);
        if (root != null) {
            return root.endsWith(".") ? root : root.concat(".");
        } else {
            return null;
        }
    }

    public static Integer getAuditMessageOIDIndex() {
        return PreferenceService.getInteger(OID_INDEX);
    }

    public static void setAuditMessageOIDIndex(Integer value) {
        PreferenceService.setInteger(OID_INDEX, value);
    }

    public static String getDicomXsd() {
        return PreferenceService.getString(DICOM_XSD);
    }

    public static String getRfc3881Xsd() {
        return PreferenceService.getString(RFC3881_XSD);
    }


}
