/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.audit.message.action;

import net.ihe.gazelle.audit.message.utils.AuditMessagePreferences;
import net.ihe.gazelle.audit.message.utils.ConfigurationException;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("auditMessageOIDProvider")
@Scope(ScopeType.STATELESS)
@GenerateInterface("AuditMessageOIDProviderLocal")
public class AuditMessageOIDProvider implements AuditMessageOIDProviderLocal {

    public String generateNextOid() throws ConfigurationException {
        String root = AuditMessagePreferences.getAuditMessageRootOID();
        if (root != null) {
            Integer index = AuditMessagePreferences.getAuditMessageOIDIndex();
            if (index != null) {
                AuditMessagePreferences.setAuditMessageOIDIndex(index + 1);
                return root + index;
            } else {
                throw new ConfigurationException(AuditMessagePreferences.OID_INDEX + " is not set, cannot generate OID");
            }
        } else {
            throw new ConfigurationException(AuditMessagePreferences.ROOT_OID + " is not set, cannot generate OID");
        }
    }

}
