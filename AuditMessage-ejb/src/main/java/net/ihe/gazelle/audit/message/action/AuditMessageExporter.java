/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.audit.message.action;

import net.ihe.gazelle.audit.message.desc.AuditMessageSpecification;
import net.ihe.gazelle.audit.message.utils.AuditMessagePreferences;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.io.IOUtils;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.LoggerFactory;

import javax.activation.FileTypeMap;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;

/**
 * @author Cédric Eoche-Duval
 */
public class AuditMessageExporter implements Serializable {

    private static final long serialVersionUID = -450973131509093760L;
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(AuditMessageExporter.class);
    private static final String ARCHIVE_PREFIX = "ams-";
    private static final String ARCHIVE_EXTENSION = "zip";

    public static void downloadArchive(List<AuditMessageSpecification> auditMessageSpecificationList)
            throws JAXBException, IOException {
        File zipFile = AuditMessageZipper.zip(auditMessageSpecificationList);
        String archiveName = ARCHIVE_PREFIX + AuditMessagePreferences.getAuditMessageRootOID() + ARCHIVE_EXTENSION;
        exportFile(new FileInputStream(zipFile), archiveName, true);
    }

    public static void downloadOne(AuditMessageSpecification auditMessageSpecification) throws JAXBException {
        exportOne(auditMessageSpecification, true);
    }

    public static void showOne(AuditMessageSpecification auditMessageSpecification) throws JAXBException {
        exportOne(auditMessageSpecification, false);
    }

    private static void exportOne(AuditMessageSpecification auditMessageSpecification, boolean download)
            throws JAXBException {
        if (auditMessageSpecification != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            AuditMessageMarshaller.objectToXml(auditMessageSpecification, baos);
            exportFile(baos.toByteArray(),
                    auditMessageSpecification.getOid() + ".xml", download);

        }
    }

    private static void exportFile(byte[] bytes, String filename, boolean download) {
        exportFile(new ByteArrayInputStream(bytes), filename, download);
    }

    private static void exportFile(InputStream inputStream, String filename, boolean download) {

        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();

        boolean hasException = false;
        while (!hasException) {
            try {
                response = (HttpServletResponse) PropertyUtils.getProperty(response, "response");
            } catch (Exception e1) {
                // nothing to do, we are at org.apache.catalina.connector.ResponseFacade
                hasException = true;
            }
        }

        response.reset();
        HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();

        exportFile(request, response, inputStream, filename, download);

        facesContext.responseComplete();
    }

    private static void exportFile(HttpServletRequest request, HttpServletResponse response, InputStream inputStream,
                                   String filename, boolean download) {
        try {
            if (inputStream != null) {

                String userAgent = request.getHeader("user-agent");
                boolean isInternetExplorer = (userAgent.indexOf("MSIE") > -1);

                int length = inputStream.available();

                if (filename != null) {

                    if (filename.toLowerCase().endsWith(".pdf")) {
                        response.setContentType("application/pdf");
                    } else if (filename.toLowerCase().endsWith(".xml")) {
                        response.setContentType("text/xml");
                    } else if (filename.toLowerCase().endsWith(".zip")) {
                        response.setContentType("application/zip");
                    } else {
                        String contentType = FileTypeMap.getDefaultFileTypeMap().getContentType(filename);
                        response.setContentType(contentType);
                    }

                    byte[] fileNameBytes = filename.getBytes((isInternetExplorer) ? ("windows-1250") : ("utf-8"));

                    StringBuilder dispositionFileName = new StringBuilder("");
                    for (byte b : fileNameBytes) {
                        dispositionFileName.append((char) (b & 0xff));
                    }

                    if (download) {
                        dispositionFileName.insert(0, "attachment; filename=\"");
                    } else {
                        dispositionFileName.insert(0, "inline; filename=\"");
                    }
                    dispositionFileName.append("\"");
                    String disposition = dispositionFileName.toString();

                    response.setHeader("Content-disposition", disposition);
                    response.setContentLength(length);

                    try (ServletOutputStream servletOutputStream = response.getOutputStream()) {
                        IOUtils.copy(inputStream, servletOutputStream);
                        servletOutputStream.flush();
                    }

                }
                inputStream.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            FacesMessages.instance()
                    .add(StatusMessage.Severity.ERROR, "Impossible to export file : " + e.getMessage());
        }
    }

}
