/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.audit.message.validator;

import net.ihe.gazelle.audit.message.desc.ActiveParticipantDescription;
import net.ihe.gazelle.audit.message.desc.ElementDescription;
import net.ihe.gazelle.validation.Notification;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ActiveParticipantValidator {

    private static final String WARNING = "warning";
    private static final String ERROR = "error";

    protected ActiveParticipantValidator() {
    }

    public static final ActiveParticipantValidator instance = new ActiveParticipantValidator();

    public void validate(String document,
                         List<Notification> diagnostic, ActiveParticipantDescription actpart, String activeParticipantDistinguisher,
                         boolean isDicomCompatible) {
        this.validateAlternativeUserIDOpt(document, diagnostic, actpart, activeParticipantDistinguisher);
        this.validateAlternativeUserIDRegex(document, diagnostic, actpart, activeParticipantDistinguisher);
        this.validateAlternativeUserIDValue(document, diagnostic, actpart, activeParticipantDistinguisher);
        this.validateNetworkAccessPointIDOpt(document, diagnostic, actpart, activeParticipantDistinguisher);
        this.validateNetworkAccessPointIDRegex(document, diagnostic, actpart, activeParticipantDistinguisher);
        this.validateNetworkAccessPointIDValue(document, diagnostic, actpart, activeParticipantDistinguisher);
        this.validateNetworkAccessPointTypeCodeOpt(document, diagnostic, actpart, activeParticipantDistinguisher);
        this.validateNetworkAccessPointTypeCodeRegex(document, diagnostic, actpart, activeParticipantDistinguisher);
        this.validateNetworkAccessPointTypeCodeValue(document, diagnostic, actpart, activeParticipantDistinguisher);
        this.validateRoleIDCode(document, diagnostic, actpart, activeParticipantDistinguisher, isDicomCompatible);
//        this.validateRoleIDCodeOpt(document, diagnostic, actpart, activeParticipantDistinguisher);
//        this.validateRoleIDCodeValue(document, diagnostic, actpart, activeParticipantDistinguisher, isDicomCompatible);
        this.validateUserIDOpt(document, diagnostic, actpart, activeParticipantDistinguisher);
        this.validateUserIDRegex(document, diagnostic, actpart, activeParticipantDistinguisher);
        this.validateUserIDValue(document, diagnostic, actpart, activeParticipantDistinguisher);
        this.validateUserIsRequestorOpt(document, diagnostic, actpart, activeParticipantDistinguisher);
        this.validateUserIsRequestorRegex(document, diagnostic, actpart, activeParticipantDistinguisher);
        this.validateUserIsRequestorValue(document, diagnostic, actpart, activeParticipantDistinguisher);
        this.validateUserNameOpt(document, diagnostic, actpart, activeParticipantDistinguisher);
        this.validateUserNameRegex(document, diagnostic, actpart, activeParticipantDistinguisher);
        this.validateUserNameValue(document, diagnostic, actpart, activeParticipantDistinguisher);
        this.validateMediaIdentifierOpt(document, diagnostic, actpart, activeParticipantDistinguisher);
        this.validateMediaIdentifierValue(document, diagnostic, actpart, activeParticipantDistinguisher, isDicomCompatible);
    }

    protected void validateUserIDOpt(String document, List<Notification> diagnostic, ActiveParticipantDescription actpart,
                                     String activeParticipantDistinguisher) {
        if (actpart != null && actpart.getUserID() != null && actpart.getUserID().getOptionality() != null) {
            try {
                //RoleIDCode[@code='110153']
                String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies $actpart/@UserID";

                switch (actpart.getUserID().getOptionality()) {

                    case M:
                        ValidatorUtil.handleXpath(xpath, actpart.getName() + "/UserID is mandatory", ERROR, document, diagnostic);
                        break;

                    case C:
                        ValidatorUtil.handleXpath(xpath, actpart.getName() + "/UserID has optionality 'C'", WARNING, document, diagnostic);
                        break;

                    case NA:
                        xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies not" +
                                "($actpart/@UserID)";
                        ValidatorUtil.handleXpath(xpath, actpart.getName() + "/UserID is prohibited", ERROR, document, diagnostic);
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected void validateUserIDValue(String document, List<Notification> diagnostic, ActiveParticipantDescription actpart,
                                       String activeParticipantDistinguisher) {
        if (actpart != null && actpart.getUserID() != null && actpart.getUserID().getValue() != null) {
            String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies not" +
                    "($actpart/@UserID) or $actpart/@UserID='" +
                    actpart.getUserID().getValue() + "'";
            ValidatorUtil.handleXpath(xpath,
                    actpart.getName() + "/@UserID must be '" + actpart.getUserID().getValue() +
                            "' if /" + actpart.getName() + "/@UserID present",
                    ERROR, document, diagnostic);
        }
    }

    protected void validateUserIDRegex(String document, List<Notification> diagnostic, ActiveParticipantDescription actpart,
                                       String activeParticipantDistinguisher) {
        if (actpart != null && actpart.getUserID() != null && actpart.getUserID().getRegex() != null) {
            String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies " +
                    "not($actpart/@UserID) or matches($actpart/@UserID,'" + actpart.getUserID().getRegex() + "')";
            ValidatorUtil.handleXpath(xpath,
                    actpart.getName() + "/@UserID must have this form '" + actpart.getUserID().getRegex() +
                            "' if " + actpart.getName() + "/@UserID present",
                    ERROR, document, diagnostic);
        }
    }

    protected void validateAlternativeUserIDOpt(String document, List<Notification> diagnostic, ActiveParticipantDescription actpart,
                                                String activeParticipantDistinguisher) {
        if (actpart != null && actpart.getAlternativeUserID() != null && actpart.getAlternativeUserID().getOptionality() != null) {
            try {
                String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies " +
                        "$actpart/@AlternativeUserID";

                switch (actpart.getAlternativeUserID().getOptionality()) {

                    case M:
                        ValidatorUtil.handleXpath(xpath, actpart.getName() + "/@AlternativeUserID is mandatory", ERROR, document, diagnostic);
                        break;

                    case C:
                        ValidatorUtil
                                .handleXpath(xpath, actpart.getName() + "/@AlternativeUserID has optionality 'C'", WARNING, document, diagnostic);
                        break;

                    case NA:
                        xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies not" +
                                "($actpart/@AlternativeUserID)";
                        ValidatorUtil.handleXpath(xpath, actpart.getName() + "/@AlternativeUserID is prohibited", ERROR, document, diagnostic);
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected void validateAlternativeUserIDValue(String document, List<Notification> diagnostic, ActiveParticipantDescription actpart,
                                                  String activeParticipantDistinguisher) {
        if (actpart != null && actpart.getAlternativeUserID() != null && actpart.getAlternativeUserID().getValue() != null) {
            String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] " +
                    "satisfies not($actpart/@AlternativeUserID) or $actpart/@AlternativeUserID='" +
                    actpart.getAlternativeUserID().getValue() + "'";
            ValidatorUtil.handleXpath(xpath,
                    actpart.getName() + "/@AlternativeUserID must be '" + actpart.getAlternativeUserID().getValue() +
                            "' if " + actpart.getName() + "/@AlternativeUserID present",
                    ERROR, document, diagnostic);
        }
    }

    protected void validateAlternativeUserIDRegex(String document, List<Notification> diagnostic, ActiveParticipantDescription actpart,
                                                  String activeParticipantDistinguisher) {
        if (actpart != null && actpart.getAlternativeUserID() != null && actpart.getAlternativeUserID().getRegex() != null) {
            String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies " +
                    "not($actpart/@AlternativeUserID) or matches($actpart/@AlternativeUserID,'" + actpart.getAlternativeUserID().getRegex() + "')";
            ValidatorUtil.handleXpath(xpath,
                    actpart.getName() + "/@AlternativeUserID must have this form '" + actpart.getAlternativeUserID().getRegex() +
                            "' if " + actpart.getName() + "/@AlternativeUserID present",
                    ERROR, document, diagnostic);
        }
    }

    protected void validateUserNameOpt(String document, List<Notification> diagnostic, ActiveParticipantDescription actpart,
                                       String activeParticipantDistinguisher) {
        if (actpart != null && actpart.getUserName() != null && actpart.getUserName().getOptionality() != null) {
            try {
                String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies " +
                        "$actpart/@UserName";

                switch (actpart.getUserName().getOptionality()) {

                    case M:
                        ValidatorUtil.handleXpath(xpath, actpart.getName() + "/@UserName is mandatory", ERROR, document, diagnostic);
                        break;

                    case C:
                        ValidatorUtil.handleXpath(xpath, actpart.getName() + "/@UserName has optionality 'C'", WARNING, document, diagnostic);
                        break;

                    case NA:
                        xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies not" +
                                "($actpart/@UserName)";
                        ValidatorUtil.handleXpath(xpath, actpart.getName() + "/@UserName is prohibited", ERROR, document, diagnostic);
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected void validateUserNameValue(String document, List<Notification> diagnostic, ActiveParticipantDescription actpart,
                                         String activeParticipantDistinguisher) {
        if (actpart != null && actpart.getUserName() != null && actpart.getUserName().getValue() != null) {
            String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] " +
                    "satisfies not($actpart/@UserName) or $actpart/@UserName='" +
                    actpart.getUserName().getValue() + "'";
            ValidatorUtil.handleXpath(xpath,
                    actpart.getName() + "/@UserName must be '" + actpart.getUserName().getValue() +
                            "' if " + actpart.getName() + "/@UserName present",
                    ERROR, document, diagnostic);
        }
    }

    protected void validateUserNameRegex(String document, List<Notification> diagnostic, ActiveParticipantDescription actpart,
                                         String activeParticipantDistinguisher) {
        if (actpart != null && actpart.getUserName() != null && actpart.getUserName().getRegex() != null) {
            String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies " +
                    "not($actpart/@UserName) or matches($actpart/@UserName,'" + actpart.getUserName().getRegex() + "')";
            ValidatorUtil.handleXpath(xpath,
                    actpart.getName() + "/@UserName must have this form '" + actpart.getUserName().getRegex() +
                            "' if " + actpart.getName() + "/@UserName present",
                    ERROR, document, diagnostic);
        }
    }

    protected void validateUserIsRequestorOpt(String document, List<Notification> diagnostic, ActiveParticipantDescription actpart,
                                              String activeParticipantDistinguisher) {
        if (actpart != null && actpart.getUserIsRequestor() != null && actpart.getUserIsRequestor().getOptionality() != null) {
            try {
                String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies " +
                        "$actpart/@UserIsRequestor";

                switch (actpart.getUserIsRequestor().getOptionality()) {

                    case M:
                        ValidatorUtil.handleXpath(xpath, actpart.getName() + "/@UserIsRequestor is mandatory", ERROR, document, diagnostic);
                        break;

                    case C:
                        ValidatorUtil.handleXpath(xpath, actpart.getName() + "/@UserIsRequestor has optionality 'C'", WARNING, document, diagnostic);
                        break;

                    case NA:
                        xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies not" +
                                "($actpart/@UserIsRequestor)";
                        ValidatorUtil.handleXpath(xpath, actpart.getName() + "/@UserIsRequestor is prohibited", ERROR, document, diagnostic);
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected void validateUserIsRequestorValue(String document, List<Notification> diagnostic, ActiveParticipantDescription actpart,
                                                String activeParticipantDistinguisher) {
        if (actpart != null && actpart.getUserIsRequestor() != null && actpart.getUserIsRequestor().getValue() != null) {
            String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] " +
                    "satisfies not($actpart/@UserIsRequestor) or $actpart/@UserIsRequestor='" +
                    actpart.getUserIsRequestor().getValue() + "'";
            ValidatorUtil.handleXpath(xpath,
                    actpart.getName() + "/@UserIsRequestor must be '" + actpart.getUserIsRequestor().getValue() +
                            "' if " + actpart.getName() + "/@UserIsRequestor present",
                    ERROR, document, diagnostic);
        }
    }

    protected void validateUserIsRequestorRegex(String document, List<Notification> diagnostic, ActiveParticipantDescription actpart,
                                                String activeParticipantDistinguisher) {
        if (actpart != null && actpart.getUserIsRequestor() != null && actpart.getUserIsRequestor().getRegex() != null) {
            String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies " +
                    "not($actpart/@UserIsRequestor) or matches($actpart/@UserIsRequestor,'" + actpart.getUserIsRequestor().getRegex() + "')";
            ValidatorUtil.handleXpath(xpath,
                    actpart.getName() + "/@UserIsRequestor must have this form '" + actpart.getUserIsRequestor().getRegex() +
                            "' if " + actpart.getName() + "/@UserIsRequestor present",
                    ERROR, document, diagnostic);
        }
    }

    protected void validateRoleIDCode(String document, List<Notification> diagnostic, ActiveParticipantDescription actpart,
                                      String activeParticipantDistinguisher, boolean isDicomCompatible) {

        if (actpart != null && actpart.getRoleIDCode() != null && actpart.getRoleIDCode().getOptionality() != null) {
            try {

                String xpath = "every $actpart in (/AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "]) satisfies count" +
                        "($actpart/RoleIDCode)>0";

                Matcher ridMatcher = getRoleIDCodeMatcher(actpart.getRoleIDCode());

                switch (actpart.getRoleIDCode().getOptionality()) {

                    case M:
                        ValidatorUtil.handleXpath(xpath, actpart.getName() + "/RoleIDCode is mandatory", ERROR, document, diagnostic);
                        if (ridMatcher != null) {
                            this.validateMandatoryRoleIDCode(ridMatcher.group(1), ridMatcher.group(2), ridMatcher.group(3), document, diagnostic,
                                    actpart,
                                    activeParticipantDistinguisher, isDicomCompatible);
                        }
                        break;
                    case U:
                    case C:
                        ValidatorUtil.handleXpath(xpath, actpart.getName() + "/RoleIDCode has optionality 'C'", WARNING, document, diagnostic);
                        if (ridMatcher != null) {
                            this.validateOptionalRoleIDCode(ridMatcher.group(1), ridMatcher.group(2), ridMatcher.group(3), document, diagnostic,
                                    actpart,
                                    activeParticipantDistinguisher, isDicomCompatible);
                        }
                        break;

                    case NA:
                        xpath = "every $actpart in (/AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "]) satisfies count" +
                                "($actpart/RoleIDCode)=0";
                        ValidatorUtil.handleXpath(xpath, actpart.getName() + "/RoleIDCode is prohibited", ERROR, document, diagnostic);
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected Matcher getRoleIDCodeMatcher(ElementDescription roleIdCode) {
        if (roleIdCode != null && roleIdCode.getValue() != null) {
            try {
                String val = roleIdCode.getValue();
                Pattern pat = Pattern.compile("^\\s*EV\\s*\\(\\s*'(.*?)'\\s*,\\s*'(.*?)'\\s*,\\s*'(.*?)'\\s*\\)\\s*$");
                Matcher matcher = pat.matcher(val);
                if (matcher.find()) {
                    return matcher;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    // If RoleIDCode is Mandatory
    protected void validateMandatoryRoleIDCode(String code, String codeSystemName, String displayName, String document, List<Notification> diagnostic,
                                               ActiveParticipantDescription actpart, String activeParticipantDistinguisher, Boolean isDicomCompatible)
            throws Exception {
        // Validate code and codeSystemName
        String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies " +
                "(some $i in $actpart/RoleIDCode satisfies  $i/@" + ValidatorUtil
                .getCodeIdentifier(isDicomCompatible) + "='" + code + "' and $i/@codeSystemName='" + codeSystemName + "')";
        ValidatorUtil.handleXpath(xpath,
                actpart.getName() + "/RoleIDCode must have @" + ValidatorUtil.getCodeIdentifier(isDicomCompatible) + " equals to '" + code +
                        "' and @codeSystemName equals to '" + codeSystemName + "'", ERROR, document, diagnostic);

        // validate displayName
        xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies " +
                "(some $i in $actpart/RoleIDCode satisfies $i/@" + ValidatorUtil
                .getDisplayNameIdentifier(isDicomCompatible) + "='" + displayName + "')";
        ValidatorUtil.handleXpath(xpath,
                actpart.getName() + "/RoleIDCode should have @" + ValidatorUtil
                        .getDisplayNameIdentifier(isDicomCompatible) + " equals to '" + displayName + "'", WARNING, document, diagnostic);
    }

    // If RoleIDCode is Optional
    protected void validateOptionalRoleIDCode(String code, String codeSystemName, String displayName, String document, List<Notification> diagnostic,
                                              ActiveParticipantDescription actpart, String activeParticipantDistinguisher, Boolean isDicomCompatible)
            throws Exception {
        // validate code and codeSystemName
        String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies " +
                "(every $i in $actpart/RoleIDCode[@" + ValidatorUtil.getCodeIdentifier(isDicomCompatible) + "='" + code +
                "' or @" + ValidatorUtil.getDisplayNameIdentifier(isDicomCompatible) + "='" + displayName + "'] satisfies $i/@" + ValidatorUtil
                .getCodeIdentifier(isDicomCompatible) + "='" + code + "' and $i/@codeSystemName='" + codeSystemName + "')";
        ValidatorUtil.handleXpath(xpath,
                actpart.getName() + "/RoleIDCode must have @" + ValidatorUtil.getCodeIdentifier(isDicomCompatible) + " equals to '" + code +
                        "' and @codeSystemName equals to '" + codeSystemName + "' if " + actpart
                        .getName() + "/RoleIDCode present", ERROR, document, diagnostic);

        // validate displayName
        xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies " +
                "(every $i in $actpart/RoleIDCode[@" + ValidatorUtil.getCodeIdentifier(isDicomCompatible) + "='" + code +
                "' or @" + ValidatorUtil.getDisplayNameIdentifier(isDicomCompatible) + "='" + displayName + "'] satisfies $i/@" + ValidatorUtil
                .getDisplayNameIdentifier(isDicomCompatible) + "='" + displayName + "')";
        ValidatorUtil.handleXpath(xpath,
                actpart.getName() + "/RoleIDCode should have @" + ValidatorUtil
                        .getDisplayNameIdentifier(isDicomCompatible) + " equals to '" + displayName + "' if " + actpart
                        .getName() + "/RoleIDCode present", WARNING, document, diagnostic);

    }

    protected void validateNetworkAccessPointTypeCodeOpt(String document, List<Notification> diagnostic, ActiveParticipantDescription actpart,
                                                         String activeParticipantDistinguisher) {
        if (actpart != null && actpart.getNetworkAccessPointTypeCode() != null && actpart.getNetworkAccessPointTypeCode().getOptionality() != null) {
            try {
                String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies " +
                        "$actpart/@NetworkAccessPointTypeCode";

                switch (actpart.getNetworkAccessPointTypeCode().getOptionality()) {

                    case M:
                        ValidatorUtil
                                .handleXpath(xpath, actpart.getName() + "/@NetworkAccessPointTypeCode is mandatory", ERROR, document, diagnostic);
                        break;

                    case C:
                        ValidatorUtil.handleXpath(xpath, actpart.getName() + "/@NetworkAccessPointTypeCode has optionality 'C'", WARNING, document,
                                diagnostic);
                        break;

                    case NA:
                        xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies not" +
                                "($actpart/@NetworkAccessPointTypeCode)";
                        ValidatorUtil
                                .handleXpath(xpath, actpart.getName() + "/@NetworkAccessPointTypeCode is prohibited", ERROR, document, diagnostic);
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected void validateNetworkAccessPointTypeCodeValue(String document, List<Notification> diagnostic, ActiveParticipantDescription actpart,
                                                           String activeParticipantDistinguisher) {
        if (actpart != null && actpart.getNetworkAccessPointTypeCode() != null && actpart.getNetworkAccessPointTypeCode().getValue() != null) {
            String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] " +
                    "satisfies not($actpart/@NetworkAccessPointTypeCode) or $actpart/@NetworkAccessPointTypeCode='" +
                    actpart.getNetworkAccessPointTypeCode().getValue() + "'";
            ValidatorUtil.handleXpath(xpath,
                    actpart.getName() + "/@NetworkAccessPointTypeCode must be '" + actpart.getNetworkAccessPointTypeCode().getValue() +
                            "' if " + actpart.getName() + "/@NetworkAccessPointTypeCode present",
                    ERROR, document, diagnostic);
        }
    }

    protected void validateNetworkAccessPointTypeCodeRegex(String document, List<Notification> diagnostic, ActiveParticipantDescription actpart,
                                                           String activeParticipantDistinguisher) {
        if (actpart != null && actpart.getNetworkAccessPointTypeCode() != null && actpart.getNetworkAccessPointTypeCode().getRegex() != null) {
            String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies " +
                    "not($actpart/@NetworkAccessPointTypeCode) or matches($actpart/@NetworkAccessPointTypeCode,'" + actpart
                    .getNetworkAccessPointTypeCode().getRegex() + "')";
            ValidatorUtil.handleXpath(xpath,
                    actpart.getName() + "/@NetworkAccessPointTypeCode must have this form '" + actpart.getNetworkAccessPointTypeCode().getRegex() +
                            "' if " + actpart.getName() + "/@NetworkAccessPointTypeCode present",
                    ERROR, document, diagnostic);
        }
    }

    protected void validateNetworkAccessPointIDOpt(String document, List<Notification> diagnostic, ActiveParticipantDescription actpart,
                                                   String activeParticipantDistinguisher) {
        if (actpart != null && actpart.getNetworkAccessPointID() != null && actpart.getNetworkAccessPointID().getOptionality() != null) {
            try {
                String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies " +
                        "$actpart/@NetworkAccessPointID";

                switch (actpart.getNetworkAccessPointID().getOptionality()) {

                    case M:
                        ValidatorUtil.handleXpath(xpath, actpart.getName() + "/@NetworkAccessPointID is mandatory", ERROR, document, diagnostic);
                        break;

                    case C:
                        ValidatorUtil
                                .handleXpath(xpath, actpart.getName() + "/@NetworkAccessPointID has optionality 'C'", WARNING, document, diagnostic);
                        break;

                    case NA:
                        xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies not" +
                                "($actpart/@NetworkAccessPointID)";
                        ValidatorUtil.handleXpath(xpath, actpart.getName() + "/@NetworkAccessPointID is prohibited", ERROR, document, diagnostic);
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected void validateNetworkAccessPointIDValue(String document, List<Notification> diagnostic, ActiveParticipantDescription actpart,
                                                     String activeParticipantDistinguisher) {
        if (actpart != null && actpart.getNetworkAccessPointID() != null && actpart.getNetworkAccessPointID().getValue() != null) {
            String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] " +
                    "satisfies not($actpart/@NetworkAccessPointID) or $actpart/@NetworkAccessPointID='" +
                    actpart.getNetworkAccessPointID().getValue() + "'";
            ValidatorUtil.handleXpath(xpath,
                    actpart.getName() + "/@NetworkAccessPointID must be '" + actpart.getNetworkAccessPointID().getValue() +
                            "' if " + actpart.getName() + "/@NetworkAccessPointID present",
                    ERROR, document, diagnostic);
        }
    }

    protected void validateNetworkAccessPointIDRegex(String document, List<Notification> diagnostic, ActiveParticipantDescription actpart,
                                                     String activeParticipantDistinguisher) {
        if (actpart != null && actpart.getNetworkAccessPointID() != null && actpart.getNetworkAccessPointID().getRegex() != null) {
            String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies " +
                    "not($actpart/@NetworkAccessPointID) or matches($actpart/@NetworkAccessPointID,'" + actpart.getNetworkAccessPointID()
                    .getRegex() + "')";
            ValidatorUtil.handleXpath(xpath,
                    actpart.getName() + "/@NetworkAccessPointID must have this form '" + actpart.getNetworkAccessPointID().getRegex() +
                            "' if " + actpart.getName() + "/@NetworkAccessPointID present",
                    ERROR, document, diagnostic);
        }
    }

    protected void validateMediaIdentifierOpt(String document, List<Notification> diagnostic, ActiveParticipantDescription actpart,
                                              String activeParticipantDistinguisher) {
        if (actpart != null && actpart.getMediaIdentifier() != null && actpart.getMediaIdentifier().getOptionality() != null) {
            try {
                String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies count" +
                        "($actpart/MediaIdentifier)>0";

                switch (actpart.getMediaIdentifier().getOptionality()) {

                    case M:
                        ValidatorUtil.handleXpath(xpath, actpart.getName() + "/MediaIdentifier is mandatory", ERROR, document, diagnostic);
                        break;

                    case C:
                        ValidatorUtil.handleXpath(xpath, actpart.getName() + "/MediaIdentifier has optionality 'C'", WARNING, document, diagnostic);
                        break;

                    case NA:
                        xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies count" +
                                "($actpart/MediaIdentifier)=0";
                        ValidatorUtil.handleXpath(xpath, actpart.getName() + "/MediaIdentifier is prohibited", ERROR, document, diagnostic);
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected void validateMediaIdentifierValue(String document, List<Notification> diagnostic, ActiveParticipantDescription actpart,
                                                String activeParticipantDistinguisher, boolean isDicomCompatible) {
        if (actpart != null && actpart.getMediaIdentifier() != null && actpart.getMediaIdentifier().getValue() != null) {
            try {
                String val = actpart.getMediaIdentifier().getValue();
                Pattern pat = Pattern.compile("^\\s*EV\\s*\\(\\s*'(.*?)'\\s*,\\s*'(.*?)'\\s*,\\s*'(.*?)'\\s*\\)\\s*$");
                Matcher matcher = pat.matcher(val);
                if (matcher.find()) {
                    this.validateMediaIdentifierCodeValue(matcher.group(1), document, diagnostic, actpart, activeParticipantDistinguisher,
                            isDicomCompatible);
                    this.validateMediaIdentifierCodeSystemNameValue(matcher.group(2), document, diagnostic, actpart, activeParticipantDistinguisher);
                    this.validateMediaIdentifierDisplayNameValue(matcher.group(3), document, diagnostic, actpart, activeParticipantDistinguisher,
                            isDicomCompatible);
                }
            } catch (Exception e) {

            }
        }
    }

    protected void validateMediaIdentifierCodeValue(String code, String document, List<Notification> diagnostic,
                                                    ActiveParticipantDescription actpart, String activeParticipantDistinguisher,
                                                    Boolean isDicomCompatible)
            throws Exception {
        String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies " +
                "(every $i in $actpart/MediaIdentifier/MediaType satisfies  $i/@" + ValidatorUtil
                .getCodeIdentifier(isDicomCompatible) + "='" + code + "')";
        ValidatorUtil.handleXpath(xpath, actpart.getName() + "/MediaIdentifier/MediaType@" + ValidatorUtil.getCodeIdentifier(isDicomCompatible) +
                " must be '" + code +
                "' if " + actpart.getName() + "/MediaIdentifier/MediaType present", ERROR, document, diagnostic);
    }

    protected void validateMediaIdentifierCodeSystemNameValue(String codeSystemName, String document,
                                                              List<Notification> diagnostic, ActiveParticipantDescription actpart,
                                                              String activeParticipantDistinguisher)
            throws Exception {
        String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies " +
                "(every $i in $actpart/MediaIdentifier/MediaType satisfies  $i/@codeSystemName='" + codeSystemName + "')";
        ValidatorUtil.handleXpath(xpath, actpart.getName() + "/MediaIdentifier/MediaType@codeSystemName must be '" + codeSystemName +
                "' if " + actpart.getName() + "/MediaIdentifier/MediaType present", ERROR, document, diagnostic);
    }

    protected void validateMediaIdentifierDisplayNameValue(String displayName, String document, List<Notification> diagnostic,
                                                           ActiveParticipantDescription actpart, String activeParticipantDistinguisher,
                                                           Boolean isDicomCompatible)
            throws Exception {
        String xpath = "every $actpart in /AuditMessage/ActiveParticipant[" + activeParticipantDistinguisher + "] satisfies " +
                "(every $i in $actpart/MediaIdentifier/MediaType satisfies  $i/@" + ValidatorUtil
                .getDisplayNameIdentifier(isDicomCompatible) + "='" + displayName + "')";
        ValidatorUtil.handleXpath(xpath,
                actpart.getName() + "/MediaIdentifier/MediaType@" + ValidatorUtil.getDisplayNameIdentifier(isDicomCompatible) + " must be '" +
                        displayName + "' if " + actpart.getName() + "/MediaIdentifier/MediaType present", ERROR, document, diagnostic);
    }


}
