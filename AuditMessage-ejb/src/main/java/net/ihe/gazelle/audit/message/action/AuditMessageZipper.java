/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.audit.message.action;

import net.ihe.gazelle.audit.message.desc.AuditMessageSpecification;
import org.apache.commons.io.IOUtils;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * Created by cel on 01/03/17.
 */
public class AuditMessageZipper {

    private static final String FILE_EXTENSION = ".xml";

    public static final File zip(List<AuditMessageSpecification> auditMessageSpecificationList)
            throws JAXBException, IOException {
        Map<String, byte[]> fileMap = buildFileMap(auditMessageSpecificationList);
        return zip(fileMap);
    }

    private static Map<String, byte[]> buildFileMap(List<AuditMessageSpecification> auditMessageSpecificationList)
            throws JAXBException {
        Map<String, byte[]> map = new HashMap<>();
        for (AuditMessageSpecification ams : auditMessageSpecificationList) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            AuditMessageMarshaller.objectToXml(ams, baos);
            map.put(ams.getOid(), baos.toByteArray());
        }
        return map;
    }

    private static final File zip(Map<String, byte[]> fileMap) throws IOException {
        File tempZipFile = File.createTempFile("ams_", ".zip");
        IOException ioe = null;
        ZipOutputStream zos = null;
        try {
            zos = new ZipOutputStream(new FileOutputStream(tempZipFile));
            for (Map.Entry<String, byte[]> fileEntry : fileMap.entrySet()) {
                ZipEntry entry = new ZipEntry(fileEntry.getKey() + FILE_EXTENSION);
                zos.putNextEntry(entry);
                zos.write(fileEntry.getValue());
                zos.flush();
            }
        } catch (IOException e) {
            ioe = e;
        } finally {
            IOUtils.closeQuietly(zos);
        }
        if (ioe != null) {
            throw ioe;
        }
        return tempZipFile;
    }


    public static final List<byte[]> unzip(File zip) throws IOException {

        List<byte[]> listZip = new ArrayList<>();
        IOException ioe = null;
        ZipFile archive = null;
        try {
            archive = new ZipFile(zip);
            Enumeration<? extends ZipEntry> entries = archive.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = (ZipEntry) entries.nextElement();

                InputStream in = archive.getInputStream(entry);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();

                byte[] buffer = new byte[8192];
                int read;
                while (-1 != (read = in.read(buffer))) {
                    baos.write(buffer, 0, read);
                }

                in.close();
                baos.close();
                listZip.add(baos.toByteArray());
            }
        } catch (IOException e) {
            ioe = e;
        } finally {
            IOUtils.closeQuietly(archive);
        }
        if (ioe != null) {
            throw ioe;
        }
        return listZip;
    }


}
