/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.audit.message.utils;

import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validation.DocumentWellFormed;
import net.ihe.gazelle.validation.XSDMessage;
import net.ihe.gazelle.xmltools.xsd.ValidationException;
import net.ihe.gazelle.xmltools.xsd.XSDValidator;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public final class XMLValidation {

    private static final String UNEXPECTED_ERROR = "Unexpected error";
    private static final String SEVERITY_ERROR = "error";
    private static final String SEVERITY_WARNING = "warning";
    private static final String STATUS_PASSED = "PASSED";
    private static final String STATUS_FAILED = "FAILED";

    private static volatile SAXParserFactory factoryBASIC;
    private static final Logger LOG = LoggerFactory.getLogger(XMLValidation.class);

    static {
        try {
            factoryBASIC = SAXParserFactory.newInstance();
            factoryBASIC.setValidating(false);
            factoryBASIC.setNamespaceAware(true);
        } catch (Exception e) {
        }
    }

    private XMLValidation() {
    }

    /**
     * Parses the file using SAX to check that it is a well-formed XML file
     *
     * @param document document to verify
     * @return a {@link DocumentWellFormed}
     */
    public static DocumentWellFormed isXMLWellFormed(String document) {
        return validateIfDocumentWellFormedXML(document, factoryBASIC);
    }

    /**
     * Checks that the XML document is valid regarding the XML Schema
     *
     * @param xmlDocument XML document to verify
     * @param schemaPath  XSD schema file path
     * @return a {@link DocumentValidXSD} or null.
     */
    public static DocumentValidXSD isCompliantWithXSD(String xmlDocument, String schemaPath) throws IOException {
        DocumentValidXSD xsdValidationReport = new DocumentValidXSD();
        return validateXMLToXSD(xmlDocument, schemaPath, xsdValidationReport);
    }

    private static DocumentWellFormed validateIfDocumentWellFormedXML(String xmlDocument, SAXParserFactory factory) {
        DocumentWellFormed xmlFormedReport = new DocumentWellFormed();
        return validateXMLToXSD(xmlDocument, "", factory, null, xmlFormedReport);
    }

    private static <T extends DocumentValidXSD> T validateXMLToXSD(String xmlDocument, String schemaPath, T xsdValidationReport) throws IOException {
        Schema schema;
        List<ValidationException> factoryExceptions = new ArrayList<ValidationException>();
        SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        parserFactory.setNamespaceAware(true);
        try {
            schema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(new URL(schemaPath));
            parserFactory.setSchema(schema);
        } catch (MalformedURLException e) {
            factoryExceptions.add(handleException(new IllegalArgumentException("XSD Schema path error: " + e.getMessage())));
        } catch (SAXException e) {
            factoryExceptions.add(handleException(new SAXException("Error while parsing XSD schema '" + schemaPath + "': " + e.getMessage())));
        }
        return validateXMLToXSD(xmlDocument, schemaPath, parserFactory, factoryExceptions, xsdValidationReport);
    }

    private static <T extends DocumentValidXSD> T validateXMLToXSD(String xmlDocument, String xsdPath, SAXParserFactory saxParserFactory,
                                                                   List<ValidationException> factoryExceptions, T validationReport) {

        List<ValidationException> exceptions = new ArrayList<ValidationException>();
        if (factoryExceptions != null && !factoryExceptions.isEmpty()) {
            for (ValidationException e : factoryExceptions) {
                LOG.warn(e.toString());
                exceptions.add(e);
            }
        }

        try {
            ByteArrayInputStream bais = new ByteArrayInputStream(xmlDocument.getBytes("UTF8"));
            XSDValidator xsdValidator = new XSDValidator();
            exceptions.addAll(xsdValidator.validateUsingFactoryAndSchema(bais, xsdPath, saxParserFactory));
        } catch (Exception e) {
            exceptions.add(handleException(e));
        }
        return extractValidationResult(exceptions, validationReport);
    }

    private static ValidationException handleException(Exception e) {

        ValidationException validationException = new ValidationException();
        validationException.setLineNumber("0");
        validationException.setColumnNumber("0");
        if (e != null && e.getMessage() != null) {
            validationException.setMessage(UNEXPECTED_ERROR + ": " + e.getMessage());
        } else if (e != null && e.getCause() != null && e.getCause().getMessage() != null) {
            validationException.setMessage(UNEXPECTED_ERROR + ": " + e.getCause().getMessage());
        } else if (e != null) {
            validationException.setMessage(UNEXPECTED_ERROR + ". The exception is of kind: " + e.getClass().getSimpleName());
        } else {
            validationException.setMessage(UNEXPECTED_ERROR);
        }
        validationException.setSeverity(SEVERITY_ERROR);
        return validationException;
    }

    private static <T extends DocumentValidXSD> T extractValidationResult(List<ValidationException> exceptions, T validationReport) {

        validationReport.setResult(STATUS_PASSED);

        if (exceptions == null || exceptions.size() == 0) {
            validationReport.setResult(STATUS_PASSED);
            return validationReport;
        } else {
            Integer nbOfErrors = 0;
            Integer nbOfWarnings = 0;
            for (ValidationException ve : exceptions) {
                if (ve.getSeverity() == null) {
                    ve.setSeverity(SEVERITY_ERROR);
                }
                if ((ve.getSeverity() != null) && (ve.getSeverity().equals(SEVERITY_WARNING))) {
                    nbOfWarnings++;
                } else {
                    nbOfErrors++;
                }
                XSDMessage xsd = new XSDMessage();
                xsd.setMessage(ve.getMessage());
                xsd.setSeverity(ve.getSeverity());

                if (StringUtils.isNumeric(ve.getLineNumber()) && StringUtils.isNumeric(ve.getColumnNumber())) {
                    xsd.setLineNumber(Integer.valueOf(ve.getLineNumber()));
                    xsd.setColumnNumber(Integer.valueOf(ve.getColumnNumber()));
                }
                validationReport.getXSDMessage().add(xsd);
            }
            validationReport.setNbOfErrors(nbOfErrors.toString());
            validationReport.setNbOfWarnings(nbOfWarnings.toString());
            if (nbOfErrors > 0) {
                validationReport.setResult(STATUS_FAILED);
            }
            return validationReport;
        }

    }

}
