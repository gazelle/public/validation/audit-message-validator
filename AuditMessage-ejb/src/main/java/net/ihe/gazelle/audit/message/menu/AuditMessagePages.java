/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.audit.message.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.common.pages.Page;

public enum AuditMessagePages implements Page {

	AM_VIEW("/audit-messages/view.xhtml", null, null, AuditMessageAuthorizations.ATNA),
	AM_LIST("/audit-messages/list.xhtml", "fa-book", "Audit message specifications", AuditMessageAuthorizations.ATNA),
    AM_EDIT("/audit-messages/edit.xhtml", null, null, AuditMessageAuthorizations.ATNA, AuditMessageAuthorizations.AM_EDITOR),
    AM_HOME("", "fa-history", "net.ihe.gazelle.audit.message.AuditTrail", AuditMessageAuthorizations.ATNA);

    private String link;

	private Authorization[] authorizations;

	private String label;

	private String icon;
	
	private AuditMessagePages(String link, String icon, String label, AuditMessageAuthorizations... authorizations) {
		this.link = link;
		this.authorizations = authorizations;
		this.label = label;
		this.icon = icon;
	}

	@Override
	public String getId() {
		return name();
	}

	@Override
	public String getLabel() {
		return label;
	}

	@Override
	public String getLink() {
		return link;
	}

	@Override
	public String getMenuLink() {
		return link.replace(".xhtml", ".seam");
	}

	@Override
	public String getIcon() {
		return icon;
	}

	@Override
	public Authorization[] getAuthorizations() {
		return authorizations;
	}

}
