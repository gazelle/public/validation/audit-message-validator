/*
 *   Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.audit.message.validator;

import net.ihe.gazelle.audit.message.action.AuditMessageMarshaller;
import net.ihe.gazelle.audit.message.desc.AuditMessageSpecification;
import net.ihe.gazelle.audit.message.desc.AuditMessageSpecificationQuery;
import net.ihe.gazelle.audit.message.utils.AuditMessagePreferences;
import net.ihe.gazelle.audit.message.utils.XMLValidation;
import net.ihe.gazelle.auditMessage.validator.api.AuditMessageValidatorProvider;
import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validation.DocumentWellFormed;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.MDAValidation;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.ValidationResultsOverview;
import org.kohsuke.MetaInfServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.soap.SOAPException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@MetaInfServices(AuditMessageValidatorProvider.class)
public class GlobalAuditMessageValidator implements AuditMessageValidatorProvider {

	private static final String PASSED = "PASSED";
	private static final String FAILED = "FAILED";
	private static Logger log = LoggerFactory.getLogger(GlobalAuditMessageValidator.class);
	public static GlobalPreferenceProvider globalPreferenceProvider;

	public static String validateDocument(String document, String validator) throws SOAPException {
		AuditMessageSpecification val = getAuditMessageSpecificationFromValue(validator);
		DetailedResult dr = validateMessageByValidator(document, val);
		String res = getDetailedResultAsString(dr);
		if (res.contains("?>")) {
			res = res.substring(res.indexOf("?>") + 2);
		}
		return res;
	}

	private static AuditMessageSpecification getAuditMessageSpecificationFromValue(String validator) {
		if (validator == null) {
			return null;
		}
		String validatorName = getValidatorName(validator);
		boolean isDicomCompatible = isDicomCapability(validator);

		AuditMessageSpecificationQuery quer = new AuditMessageSpecificationQuery();
		HQLRestriction name = quer.name().eqRestriction(validatorName);
		HQLRestriction oid = quer.oid().eqRestriction(validatorName);
		quer.addRestriction(HQLRestrictions.or(name, oid));
		AuditMessageSpecification ams = quer.getUniqueResult();
		AuditMessageSpecification amsnew = copyAuditMessage(ams);
		if (amsnew != null) {
			amsnew.setIsDicomCompatible(isDicomCompatible);
		}
		return amsnew;
	}

	private static AuditMessageSpecification copyAuditMessage(AuditMessageSpecification ams) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
            AuditMessageMarshaller.objectToXml(ams, baos);
        } catch (JAXBException e) {
			e.printStackTrace();
		}
		try {
            AuditMessageSpecification newams = AuditMessageMarshaller
                    .xmlToObject(new ByteArrayInputStream(baos.toString()
                            .getBytes()));
			newams.setId(null);
			return newams;
		} catch (JAXBException e) {
			log.info("jaxb exception : ", e);
		}
		return null;
	}

	private static boolean isDicomCapability(String validator) {
		if (validator != null) {
			return validator.contains(DicomExtensionDefinition.DICOM_COMPATIBLE.getValue());
		}
		return false;
	}

	private static String getValidatorName(String validator) {
		String[] ss = validator.split("\\s+\\[");
		if (ss != null && ss.length == 2) {
			return ss[0];
		}
		return validator;
	}

	public static DetailedResult validateMessageByValidator(String txdsString, AuditMessageSpecification val) {
		if (val == null) {
			return null;
		}
		DetailedResult dr = new DetailedResult();
		try {
			validateToSchema(dr, txdsString, getSchemaPath(val));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		List<Notification> diagnostic = new ArrayList<Notification>();
		try {
			AuditMessageValidator.instance.validate(txdsString, val, diagnostic);
		} catch (AuditMessageValidatorException e) {
			log.error("error on validating message with the validator : " + val.getName(), e);
			Error err = new Error();
			err.setDescription("error on validating message with the validator : " + val.getName() + ". "
					+ e.getMessage());
			err.setLocation("All the document");
			diagnostic.add(err);
		}
		dr.setMDAValidation(new MDAValidation());
		for (Notification notification : diagnostic) {
			notification.setDescription(notification.getDescription() + " ( " + val.getDocument() + " , "
					+ val.getSection() + " ) ");
			dr.getMDAValidation().getWarningOrErrorOrNote().add(notification);
		}
		updateMDAValidation(dr.getMDAValidation());
		summarizeDetailedResult(dr, val);
		return dr;
	}

	private static String getSchemaPath(AuditMessageSpecification val) {
		if (val.getIsDicomCompatibleAsBoolean()) {
            return AuditMessagePreferences.getDicomXsd();
        }
        return AuditMessagePreferences.getRfc3881Xsd();
    }

	static void summarizeDetailedResult(DetailedResult dr, AuditMessageSpecification val) {
		if (dr != null) {

			Date dd = new Date();
			DateFormat dateFormat = new SimpleDateFormat("yyyy, MM dd");
			DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
			DateFormat dF = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

			dr.setValidationResultsOverview(new ValidationResultsOverview());
			dr.getValidationResultsOverview().setValidationDate(dateFormat.format(dd));
			dr.getValidationResultsOverview().setValidationTime(timeFormat.format(dd));
			dr.getValidationResultsOverview().setValidationServiceName(
					"Gazelle AuditMessage Validation  : " + val.getName());
			dr.getValidationResultsOverview().setValidationTestResult(PASSED);
			if ((dr.getDocumentValidXSD() != null) && (dr.getDocumentValidXSD().getResult() != null)
					&& (dr.getDocumentValidXSD().getResult().equals(FAILED))) {
				dr.getValidationResultsOverview().setValidationTestResult(FAILED);
			}
			if ((dr.getDocumentWellFormed() != null) && (dr.getDocumentWellFormed().getResult() != null)
					&& (dr.getDocumentWellFormed().getResult().equals(FAILED))) {
				dr.getValidationResultsOverview().setValidationTestResult(FAILED);
			}
			if ((dr.getMDAValidation() != null) && (dr.getMDAValidation().getResult() != null)
					&& (dr.getMDAValidation().getResult().equals(FAILED))) {
				dr.getValidationResultsOverview().setValidationTestResult(FAILED);
			}
			dr.getValidationResultsOverview().setValidationServiceVersion(dF.format(val.getLastChanged()));
		}
	}

	private static void updateMDAValidation(MDAValidation mda) {
		mda.setResult(PASSED);
		for (Object notification : mda.getWarningOrErrorOrNote()) {
			if (notification instanceof net.ihe.gazelle.validation.Error) {
				mda.setResult(FAILED);
			}
		}
	}

	private static void validateToSchema(DetailedResult res, String sub, String schemapath) {
		DocumentWellFormed dd = XMLValidation.isXMLWellFormed(sub);
		res.setDocumentWellFormed(dd);
		try {
			res.setDocumentValidXSD(XMLValidation.isCompliantWithXSD(sub, schemapath));
		} catch (Exception e) {
			res.setDocumentValidXSD(new DocumentValidXSD());
			log.error(e.getMessage(), e);
		}
	}

	private static String getDetailedResultAsString(DetailedResult dr) {
		if (dr != null) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			try {
				save(baos, dr, null);
			} catch (JAXBException e) {
				log.error(e.getMessage(), e);
			}
			String res = baos.toString();
			return res;
		}
		return null;
	}

	public static void save(OutputStream os, DetailedResult txdw, String stylesheetUrl) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.validation");
		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		if (stylesheetUrl != null) {
			String xslDeclaration = "<?xml-stylesheet type=\"text/xsl\" href=\"" + stylesheetUrl + "\" ?>";
			m.setProperty("com.sun.xml.bind.xmlHeaders", xslDeclaration);
		}
		m.marshal(txdw, os);
	}

	@Override
	public String validateAuditMessageAndSaveFormattedReport(String document, String validator, OutputStream report,
			String stylesheetUrl) throws Exception {
		AuditMessageSpecification ams = getAuditMessageSpecificationFromValue(validator);
		if (ams != null) {
			ams.setIsDicomCompatible(true);
			DetailedResult result = validateMessageByValidator(document, ams);
			if (result != null) {
				save(report, result, stylesheetUrl);
				return result.getValidationResultsOverview().getValidationTestResult();
			} else {
				throw new Exception("Validation has encountered an error");
			}
		} else {
			throw new Exception("No validator found for value: " + validator);
		}
	}
}
