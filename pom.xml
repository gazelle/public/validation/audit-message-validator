<?xml version='1.0' encoding='UTF-8'?>
<!--
  ~   Copyright 2014-2023 IHE International
  ~
  ~    Licensed under the Apache License, Version 2.0 (the "License");
  ~    you may not use this file except in compliance with the License.
  ~    You may obtain a copy of the License at
  ~
  ~        http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~    Unless required by applicable law or agreed to in writing, software
  ~    distributed under the License is distributed on an "AS IS" BASIS,
  ~    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~    See the License for the specific language governing permissions and
  ~    limitations under the License.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>net.ihe.gazelle.maven</groupId>
        <artifactId>gazelle-tools</artifactId>
        <version>3.4.0</version>
    </parent>
    <groupId>net.ihe.gazelle</groupId>
    <artifactId>AuditMessage</artifactId>
    <version>3.0.1-SNAPSHOT</version>
    <packaging>pom</packaging>
    <issueManagement>
        <system>JIRA</system>
        <url>https://gazelle.ihe.net/jira/browse/TLS</url>
    </issueManagement>
    <scm>
        <connection>scm:git:${git.project.url}</connection>
        <developerConnection>scm:git:${git.project.url}</developerConnection>
        <url>https://gitlab.inria.fr/gazelle/public/validation/audit-message-validator</url>
        <tag>HEAD</tag>
    </scm>
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.7</maven.compiler.source>
        <maven.compiler.target>1.7</maven.compiler.target>
        <git.user.name>git</git.user.name>
        <git.user.token>git</git.user.token>
        <git.project.url>
            https://${git.user.name}:${git.user.token}@gitlab.inria.fr/gazelle/public/validation/audit-message-validator.git
        </git.project.url>

        <messages.mode>crowdin</messages.mode>
        <axiom.version>1.2.19</axiom.version> <!-- must be the same that used by axis2 in gazelle-axis2-client -->
        <sonar.maven.plugin>3.5.0.1254</sonar.maven.plugin>
    </properties>
    <modules>
        <module>AuditMessage-ejb</module>
        <module>AuditMessage-war</module>
    </modules>
    <repositories>
        <repository>
            <id>IHE</id>
            <name>IHE Public Maven Repository Group</name>
            <url>https://gazelle.ihe.net/nexus/content/groups/public/</url>
            <layout>default</layout>
            <releases>
                <enabled>true</enabled>
                <updatePolicy>never</updatePolicy>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>
    <pluginRepositories>
        <pluginRepository>
            <id>IHE-plugins</id>
            <name>IHE Plugins Public Maven Repository Group</name>
            <url>https://gazelle.ihe.net/nexus/content/groups/public/</url>
            <layout>default</layout>
            <releases>
                <enabled>true</enabled>
                <updatePolicy>never</updatePolicy>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </pluginRepository>
    </pluginRepositories>

    <build>
        <plugins>
            <plugin>
                <groupId>org.sonarsource.scanner.maven</groupId>
                <artifactId>sonar-maven-plugin</artifactId>
                <version>${sonar.maven.plugin}</version>
            </plugin>
        </plugins>
    </build>
</project>
